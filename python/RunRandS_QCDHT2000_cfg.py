# For testing do:
# cmsRun QCDSmearingClosure_OnMC_fromMiniAOD_cfg.py

## --- Read parameters --------------------------------------------------
from TreeMaker.Utils.CommandLineParams import CommandLineParams
parameters = CommandLineParams()

runOnMC = True#parameters.value("is_mc",True)
dataSetName = parameters.value("dataset_name","Spring16.QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_ext1")
testFileName = parameters.value("test_filename","root://cmsxrootd.fnal.gov//store/mc/RunIISpring16MiniAODv1/QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/MINIAODSIM/PUSpring16_80X_mcRun2_asymptotic_2016_v3_ext1-v2/20000/023897CA-6CFB-E511-849F-0CC47A1DF7E4.root")
#testFileName = parameters.value("test_filename",'file:5A879F39-AC97-E611-A060-008CFA197D10.root')
#testFileName = parameters.value("test_filename","file:/nfs/dust/cms/user/beinsam/2017A2B2-4F1E-E611-9305-0CC47A78A3E8.root")

#globalTag = parameters.value("global_tag","80X_mcRun2_asymptotic_2016_v3")
globalTag = parameters.value("global_tag","80X_mcRun2_asymptotic_2016_miniAODv2")
if runOnMC: tagName = parameters.value("tag_name","PAT")
else: tagName = parameters.value("tag_name","RECO")
jecFile = parameters.value("jec_file","Spring16_25nsV6_MC")
jerFile = parameters.value("jer_file","Spring16_25nsV6_MC")
#jerFile = parameters.value("jer_file","")
lumi = parameters.value("lumi",10000.0)
doPUReweighting = parameters.value("pu_reweighting",False)
InputJetTag = parameters.value("jet_tag","GoodJets")
InputLeptonTag = parameters.value("lepton_tag","GoodLeptons")
OutputFileName = parameters.value("out_name","RebalanceAndSmearMC.root")
mtcut = parameters.value("mTcut",100.0)
fastsim=False
signal=False

print "*** JOB SETUP ****************************************************"
print "  is_mc          : "+str(runOnMC)
print "  global_tag     : "+globalTag
print "  jecFile        : "+jecFile
print "  jerFile        : "+jerFile
print "  lumi           : "+str(lumi)
print "  pu_reweighting : "+str(doPUReweighting)
print "  jet_tag        : "+InputJetTag
print "  lepton_tag     : "+InputLeptonTag
print "  out_name       : "+OutputFileName
print "******************************************************************"

## --- The process needs to be defined AFTER reading sys.argv, ---------
## --- otherwise edmConfigHash fails -----------------------------------
import FWCore.ParameterSet.Config as cms
import sys,os
process = cms.Process("RA2QCDSmearingClosure")

## configure geometry & conditions
process.load("Configuration.StandardSequences.GeometryRecoDB_cff")
process.load("Configuration.StandardSequences.MagneticField_AutoFromDBCurrent_cff")
#process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff")
process.GlobalTag.globaltag = globalTag

## --- Log output ------------------------------------------------------
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr = cms.untracked.PSet(placeholder = cms.untracked.bool(True))
process.MessageLogger.cout = cms.untracked.PSet(INFO = cms.untracked.PSet(reportEvery = cms.untracked.int32(100)))
process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(True),  
    allowUnscheduled = cms.untracked.bool(True))


## --- Input Source ----------------------------------------------------
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(1000))
process.source = cms.Source("PoolSource",fileNames = cms.untracked.vstring(testFileName))
process.source.duplicateCheckMode = cms.untracked.string('noDuplicateCheck')

process.TFileService = cms.Service("TFileService",fileName = cms.string(OutputFileName) )
process.load("rs.TruthNoiseFilter.truthnoisefilter_cfi")
process.TruthNoiseFilter.jetCollection = InputJetTag


VarsInt = cms.vstring()
VectorInt = cms.vstring()
VarsDouble = cms.vstring()
VectorDouble = cms.vstring()
VectorRecoCand = cms.vstring()
VarsBool = cms.vstring()


# baseline producers
process.Baseline = cms.Sequence()

print "*** NVertex producer **************************************************"
from TreeMaker.Utils.primaryvertices_cfi import primaryvertices
process.NVtx = primaryvertices.clone(
                                      VertexCollection  = cms.InputTag('offlineSlimmedPrimaryVertices'),
                                      )
process.Baseline += process.NVtx
VarsInt.extend(['NVtx'])

## --- isotrack producer -----------------------------------------------
from TreeMaker.Utils.trackIsolationMaker_cfi import trackIsolationFilter

process.IsolatedElectronTracksVeto = trackIsolationFilter.clone(
    doTrkIsoVeto= True,
    vertexInputTag = cms.InputTag("offlineSlimmedPrimaryVertices"),
    pfCandidatesTag = cms.InputTag("packedPFCandidates"),
    dR_ConeSize         = cms.double(0.3),
    dz_CutValue         = cms.double(0.1),
    minPt_PFCandidate   = cms.double(5.0),
    isoCut              = cms.double(0.2),
    pdgId               = cms.int32(11),
    mTCut=mtcut,
    )

process.IsolatedMuonTracksVeto = trackIsolationFilter.clone(
    doTrkIsoVeto= True,
    vertexInputTag = cms.InputTag("offlineSlimmedPrimaryVertices"),
    pfCandidatesTag = cms.InputTag("packedPFCandidates"),
    dR_ConeSize         = cms.double(0.3),
    dz_CutValue         = cms.double(0.1),
    minPt_PFCandidate   = cms.double(5.0),
    isoCut              = cms.double(0.2),
    pdgId               = cms.int32(13),
    mTCut=mtcut,
    )

process.IsolatedPionTracksVeto = trackIsolationFilter.clone(
    doTrkIsoVeto= True,
    vertexInputTag = cms.InputTag("offlineSlimmedPrimaryVertices"),
    pfCandidatesTag = cms.InputTag("packedPFCandidates"),
    dR_ConeSize         = cms.double(0.3),
    dz_CutValue         = cms.double(0.1),
    minPt_PFCandidate   = cms.double(10.0),
    isoCut              = cms.double(0.1),
    pdgId               = cms.int32(211),
    mTCut=mtcut,
    )

process.Baseline += process.IsolatedElectronTracksVeto
process.Baseline += process.IsolatedMuonTracksVeto
process.Baseline += process.IsolatedPionTracksVeto

## --- good leptons producer -------------------------------------------
from TreeMaker.Utils.leptonproducer_cfi import leptonproducer
process.GoodLeptons = leptonproducer.clone(
    MuonTag          = cms.InputTag('slimmedMuons'),
    ElectronTag      = cms.InputTag('slimmedElectrons'),
    PrimaryVertex    = cms.InputTag('offlineSlimmedPrimaryVertices'),
    minElecPt        = cms.double(10),
    maxElecEta       = cms.double(2.5),
    minMuPt          = cms.double(10),
    maxMuEta         = cms.double(2.4),
    UseMiniIsolation = cms.bool(True),
    muIsoValue       = cms.double(0.2),
    elecIsoValue     = cms.double(0.1), # only has an effect when used with miniIsolation
    METTag           = cms.InputTag('slimmedMETs'),
    )
process.Baseline += process.GoodLeptons
VarsInt.extend(['GoodLeptons'])

## --- good photon producer -----------------------------------------------
process.GoodPhotons = cms.EDProducer("PhotonIDisoProducer",
    photonCollection = cms.untracked.InputTag("slimmedPhotons"),
    electronCollection = cms.untracked.InputTag("slimmedElectrons"),
    conversionCollection = cms.untracked.InputTag("reducedEgamma","reducedConversions",tagName),
    beamspotCollection = cms.untracked.InputTag("offlineBeamSpot","","RECO"),
    ecalRecHitsInputTag_EE = cms.InputTag("reducedEgamma","reducedEERecHits"),
    ecalRecHitsInputTag_EB = cms.InputTag("reducedEgamma","reducedEBRecHits"),
    rhoCollection = cms.untracked.InputTag("fixedGridRhoFastjetAll"),
    genParCollection = cms.untracked.InputTag("prunedGenParticles"), 
    debug = cms.untracked.bool(False)
    )
process.Baseline += process.GoodPhotons

## ----------------------------------------------------------------------------------------------
## JECs
## ----------------------------------------------------------------------------------------------

if len(jecFile)>0:
   JECPatch = cms.string('sqlite_file:data/jec/'+jecFile+'.db')
   JECera = jecFile.split('/')[-1]
   print 'JECera====', JECera
   process.load("CondCore.DBCommon.CondDBCommon_cfi")
   from CondCore.DBCommon.CondDBSetup_cfi import CondDBSetup
   process.jec = cms.ESSource("PoolDBESSource",CondDBSetup,
                              connect = JECPatch,
                              toGet   = cms.VPSet(
                                  cms.PSet(
                                      record = cms.string("JetCorrectionsRecord"),
                                      tag    = cms.string("JetCorrectorParametersCollection_"+JECera+"_AK4PFchs"),
                                      label  = cms.untracked.string("AK4PFchs")
                                      ),
                                      cms.PSet(
                                          record = cms.string("JetCorrectionsRecord"),
                                          tag    = cms.string("JetCorrectorParametersCollection_"+JECera+"_AK4PF"),
                                          label  = cms.untracked.string("AK4PF")
                                          )
                                  )
       ) 
   process.es_prefer_jec = cms.ESPrefer("PoolDBESSource","jec")


if len(jerFile)>0:
    JERPatch = cms.string('sqlite_file:data/jer/'+jerFile+'.db')
    JERera = jerFile.split('/')[-1] #without directory
    print 'JERPatch', JERPatch
    process.jer = cms.ESSource("PoolDBESSource",CondDBSetup,
        connect = JERPatch, toGet = cms.VPSet( 
            cms.PSet(
                record = cms.string('JetResolutionRcd'),
                tag    = cms.string('JR_'+JERera+'_PtResolution_AK4PFchs'),
                label  = cms.untracked.string('AK4PFchs_pt')
                ),
            cms.PSet(
              record = cms.string('JetResolutionScaleFactorRcd'),
              tag    = cms.string('JR_'+JERera+'_SF_AK4PFchs'),
              label  = cms.untracked.string('AK4PFchs')
              ),
           ),
        )
    process.es_prefer_jer = cms.ESPrefer('PoolDBESSource', 'jer')
    GoodJetsTag = cms.InputTag('updatedPatJetsUpdatedJECJER')
else: GoodJetsTag = cms.InputTag('updatedPatJetsUpdatedJEC')    


        
'''
if True:
   from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cff import updatedPatJetCorrFactors
   process.patJetCorrFactorsReapplyJEC = updatedPatJetCorrFactors.clone(
       src     = cms.InputTag("slimmedJets"),
       levels  = ['L1FastJet', 'L2Relative', 'L3Absolute'],
       payload = 'AK4PFchs' # Make sure to choose the appropriate levels and payload here!
       )
   #   if residual: process.patJetCorrFactorsReapplyJEC.levels.append('L2L3Residual')

   from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cff import updatedPatJets
   process.patJetsReapplyJEC = updatedPatJets.clone(
       jetSource = cms.InputTag("slimmedJets"),
       jetCorrFactorsSource = cms.VInputTag(cms.InputTag("patJetCorrFactorsReapplyJEC"))
       )
'''

from PhysicsTools.PatAlgos.tools.jetTools import *   

#whoa, horsey!
updateJetCollection(
    process,
    jetSource = cms.InputTag('slimmedJets'),
    postfix = 'UpdatedJEC',
    jetCorrections = ('AK4PFchs', cms.vstring(['L1FastJet', 'L2Relative', 'L3Absolute']), 'None'),
#btagDiscriminators = ['pfCombinedInclusiveSecondaryVertexV2BJetTags']
    )
#whoa, horsey!

process.jecUnc = cms.EDProducer("JetUncertaintyProducer",
    JetTag = cms.InputTag("updatedPatJetsUpdatedJEC"),
    JetType = cms.string('AK4PFchs'),
    jecUncDir = cms.int32(0)
)   

if len(jerFile)>0:
    process.jerFactor = cms.EDProducer("SmearedPATJetProducer",
            algo = cms.string('AK4PFchs'),
            algopt = cms.string('AK4PFchs_pt'),
            dPtMaxFactor = cms.double(3),
            dRMax = cms.double(0.2),
            enabled = cms.bool(True),
            genJets = cms.InputTag("slimmedGenJets"),
            rho = cms.InputTag("fixedGridRhoFastjetAll"),
            seed = cms.uint32(37428479),
            skipGenMatching = cms.bool(False),
            src = cms.InputTag("updatedPatJetsUpdatedJEC"),
            store_factor = cms.bool(True),
            variation = cms.int32(0)
            )
    process.updatedPatJetsUpdatedJECJER = cms.EDProducer("SmearedPATJetProducer",
            algo = cms.string('AK4PFchs'),
            algopt = cms.string('AK4PFchs_pt'),
            dPtMaxFactor = cms.double(3),
            dRMax = cms.double(0.2),
            enabled = cms.bool(True),
            genJets = cms.InputTag("slimmedGenJets"),
            rho = cms.InputTag("fixedGridRhoFastjetAll"),
            seed = cms.uint32(37428479),
            skipGenMatching = cms.bool(False),
            src = cms.InputTag("updatedPatJetsUpdatedJEC"),
            store_factor = cms.bool(False),
            variation = cms.int32(0)
            )

from TreeMaker.Utils.goodjetsproducer_cfi import GoodJetsProducer
GoodJets = cms.EDFilter("GoodJetsProducer",
    ExcludeLepIsoTrackPhotons = cms.bool(True),
    JetConeSize = cms.double(0.4),
    JetTag = GoodJetsTag,
    SaveAllJetsId = cms.bool(True),
    SaveAllJetsPt = cms.bool(False),
    SkipTag = cms.VInputTag(cms.InputTag("GoodLeptons","IdIsoMuon"), cms.InputTag("GoodLeptons","IdIsoElectron"), cms.InputTag("IsolatedElectronTracksVeto"), cms.InputTag("IsolatedMuonTracksVeto"), cms.InputTag("IsolatedPionTracksVeto")),
    TagMode = cms.bool(True),
    invertJetPtFilter = cms.bool(False),
    jetPtFilter = cms.double(30),
    maxChargedEMFraction = cms.double(0.99),
    maxJetEta = cms.double(5.0),
    maxNeutralFraction = cms.double(0.99),
    maxPhotonFraction = cms.double(0.99),
    maxPhotonFractionHF = cms.double(0.9),
    minChargedFraction = cms.double(0),
    minNcharged = cms.int32(0),
    minNconstituents = cms.int32(1),
    minNneutrals = cms.int32(10),
    minNneutralsHE = cms.int32(2),
    minNneutralsHF = cms.int32(10)
)
process.GoodJets = GoodJets
process.Baseline += process.GoodJets
VarsBool.extend(['GoodJets:JetID(JetID)'])

'''
from TreeMaker.Utils.smearedpatjet_cfi import SmearedPATJetProducer
jerFactor = SmearedPATJetProducer.clone(
    src = cms.InputTag("updatedPatJets"),#'selectedUpdatedPatJets',
    variation = cms.int32(0),
    store_factor = cms.bool(True)
)
'''


'''
## --- good jets producer -----------------------------------------------
from TreeMaker.Utils.goodjetsproducer_cfi import GoodJetsProducer
GoodJets = GoodJetsProducer.clone(
    TagMode = cms.bool(True),
#JetTag= cms.InputTag('slimmedJets'),
#JetTag= cms.InputTag('patJetsReapplyJEC'),#shphink original from kevin
    JetTag= cms.InputTag('updatedPatJetsUpdatedJECAuxiliaryJERAuxiliary'),#as Kevin in the new code
    maxJetEta                 = cms.double(5.0),
    minNconstituents          = cms.int32(1),
    minNneutrals              = cms.int32(10),
    minNcharged               = cms.int32(0),
    maxNeutralFraction        = cms.double(0.99),
    maxPhotonFraction         = cms.double(0.99),
    maxPhotonFractionHF       = cms.double(0.90),
    minChargedFraction        = cms.double(0),
    maxChargedEMFraction      = cms.double(0.99),
    jetPtFilter               = cms.double(30),
    ExcludeLepIsoTrackPhotons = cms.bool(True),
    JetConeSize               = cms.double(0.4),
    SaveAllJetsId             = True,
    )

process.GoodJets = GoodJets
process.Baseline += process.GoodJets
VarsBool.extend(['GoodJets:JetID(JetID)'])
'''

## --- HT jets producer ------------------------------------------------
print "*** HT jets producer **************************************************"
from TreeMaker.Utils.subJetSelection_cfi import SubJetSelection
process.HTJets = SubJetSelection.clone(
    JetTag   = cms.InputTag('GoodJets'),
    MinPt    = cms.double(30),
    MaxEta   = cms.double(2.4),
)
process.Baseline += process.HTJets

## --- HT producer -----------------------------------------------------
print "*** HT producer **************************************************"
from TreeMaker.Utils.htdouble_cfi import htdouble
process.HT = htdouble.clone(
    JetTag  = cms.InputTag('HTJets'),
)
process.Baseline += process.HT
VarsDouble.extend(['HT'])

## --- NJets producer --------------------------------------------------
print "*** NJets producer **************************************************"
from TreeMaker.Utils.njetint_cfi import njetint
process.NJets = njetint.clone(
    JetTag  = cms.InputTag('HTJets'),
)
process.Baseline += process.NJets
VarsInt.extend(['NJets'])

## --- NBJets producer -------------------------------------------------
print "*** NBJets producer **************************************************"
from TreeMaker.Utils.btagint_cfi import btagint
process.BTags = btagint.clone(
    JetTag         = cms.InputTag('HTJets'),
    BTagInputTag = cms.string('pfCombinedInclusiveSecondaryVertexV2BJetTags'),
    BTagCutValue = cms.double(0.80)
)
process.Baseline += process.BTags
VarsInt.extend(['BTags'])

## --- MHT jets producer -----------------------------------------------
print "*** MHT jets producer **************************************************"
from TreeMaker.Utils.subJetSelection_cfi import SubJetSelection
process.MHTJets = SubJetSelection.clone(
    JetTag  = cms.InputTag('GoodJets'),
    MinPt   = cms.double(30),
    MaxEta  = cms.double(5.0),
    )
process.Baseline += process.MHTJets

## --- MET producer ----------------------------------------------------
print "*** MET producer **************************************************"
from TreeMaker.Utils.metdouble_cfi import metdouble
process.MET = metdouble.clone(
    JetTag  = cms.InputTag('MHTJets'),
    METTag  = cms.InputTag("slimmedMETs"),
    GenMETTag  = cms.InputTag("slimmedMETs"),
    )
process.Baseline += process.MET
VarsDouble.extend(['MET:Pt(MET)','MET:Phi(METPhi)','MET:CaloPt(CaloMET)','MET:CaloPhi(CaloMETPhi)','MET:PFCaloPtRatio(PFCaloMETRatio)'])

## --- MHT producer ----------------------------------------------------
print "*** MHT producer **************************************************"
from TreeMaker.Utils.mhtdouble_cfi import mhtdouble
process.MHT = mhtdouble.clone(
    JetTag  = cms.InputTag('MHTJets'),
    )
process.Baseline += process.MHT
VarsDouble.extend(['MHT:Pt(MHT)','MHT:Phi(MHT)'])

## --- DeltaPhi producer -----------------------------------------------
print "*** DeltaPhi producer **************************************************"
from TreeMaker.Utils.deltaphidouble_cfi import deltaphidouble
process.DeltaPhi = deltaphidouble.clone(
    DeltaPhiJets  = cms.InputTag('HTJets'),
    MHTJets       = cms.InputTag('MHTJets'),
    )
process.Baseline += process.DeltaPhi
VarsDouble.extend(['DeltaPhi:Jet1Pt','DeltaPhi:Jet2Pt','DeltaPhi:Jet3Pt','DeltaPhi:Jet4Pt'])
VarsDouble.extend(['DeltaPhi:Jet1Eta','DeltaPhi:Jet2Eta','DeltaPhi:Jet3Eta','DeltaPhi:Jet4Eta'])
VarsDouble.extend(['DeltaPhi:DeltaPhi1','DeltaPhi:DeltaPhi2','DeltaPhi:DeltaPhi3','DeltaPhi:DeltaPhi4'])

if runOnMC:
        from TreeMaker.WeightProducer.getWeightProducer_cff import getWeightProducer
        process.WeightProducer = getWeightProducer(process.source.fileNames[0],fastsim and signal)
        process.WeightProducer.Lumi                       = cms.double(1) #default: 1 pb-1 (unit value)
        process.WeightProducer.FileNamePUDataDistribution = cms.string('')
        VarsDouble.extend(['WeightProducer:weight(Weight)','WeightProducer:xsec(CrossSection)','WeightProducer:nevents(NumEvents)'])
        process.Baseline += process.WeightProducer
else:
    print "*** PrescaleWeightProducer setup **************************************************"
    from TreeMaker.Utils.prescaleweightproducer_cfi import prescaleweightProducer
    process.PrescaleWeightProducer = prescaleweightProducer.clone()
    process.Baseline += process.PrescaleWeightProducer
    VarsDouble.extend(['PrescaleWeightProducer:weight(PrescaleWeightHt)'])
    VarsDouble.extend(['PrescaleWeightProducer:ht(OnlineHt)'])
    VarsDouble.extend(['PrescaleWeightProducer:mht(OnlineMht)'])



#################################################
## Update PAT jets
################################################# 

Ntries_global = 10
NSmearsPerTry_global = 1


process.load("rs.QCDBkgRS.qcdbkgrs_cfi")
###############################################################################
# Rebalancing and Smearing configuration
###############################################################################
print "*** R+S Configuration **************************************************"
process.QCDfromSmearing.useEnergyTemplateBinning = False		# remember to use proper templates!!!False
process.QCDfromSmearing.runOnMC = cms.bool(runOnMC) # remember to use proper templates!!!False
process.QCDfromSmearing.useEtaPhiSmearing = False
process.QCDfromSmearing.useSamsRebalancing = cms.bool(True)		# True -> uses Bayes minimization, False uses TKinFitter code
process.QCDfromSmearing.absoluteTailScaling = True    
process.QCDfromSmearing.PrintJets = False
#process.QCDfromSmearing.SmearingFile = 'data/megatemplateNoSF.root'
process.QCDfromSmearing.RebalancingFile = 'data/TemplatesSFNom80x.root'
process.QCDfromSmearing.SmearingFile = 'data/TemplatesCoreDown80x.root'
#process.QCDfromSmearing.RebalancingFile = 'data/TemplatesSFNone80x.root'

process.QCDfromSmearing.EtaBinEdges = cms.vdouble(0, 0.5, 1,   1.5, 2,   2.5, 3,   3.5, 4,   4.5, 5.0)    ## Sam's amazing binning
process.QCDfromSmearing.PtBinEdges = cms.vdouble(0,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,44,48,52,56,60,66,72,78,84,90,100,110,120,130,140,150,160,170,180,190,200,220,240,260,280,300,330,360,390,420,450,500,550,600,650,700,750,800,900,1000,1100,1200,1300,1400,1500,1600,1800,2000,2200,2500,3000,3500,4000,5000,6000)    ## Sam's amazing binning

process.QCDfromSmearing.jetCollection = cms.InputTag('updatedPatJets')#'RandSJets:Rebalanced
process.QCDfromSmearing.FitSucceed = cms.InputTag('RandSJets','FitSucceed')###
process.QCDfromSmearing.leptonTag = InputLeptonTag
process.QCDfromSmearing.uncertaintyName = ''
process.QCDfromSmearing.InputHistoPt_HF = 'h_b_JetAll_ResponsePt'
process.QCDfromSmearing.InputHistoEta_HF = 'h_b_JetAll_ResponseEta'
process.QCDfromSmearing.InputHistoPhi_HF = 'h_b_JetAll_ResponsePhi'
process.QCDfromSmearing.InputHistoPt_NoHF = 'h_nob_JetAll_ResponsePt'
process.QCDfromSmearing.InputHistoEta_NoHF = 'h_nob_JetAll_ResponseEta'
process.QCDfromSmearing.InputHistoPhi_NoHF = 'h_nob_JetAll_ResponsePhi'
process.QCDfromSmearing.RebalanceCorrectionFile = 'data/RebalanceCorrectionFactors_13TeV_8012_energyBinning_MHTallpt10.root'
process.QCDfromSmearing.BTagEfficiencyFile = 'data/B_Mis_TagEfficiencies_Spring15MadGraph_DeadECALTP.root' 
process.QCDfromSmearing.NRebin = 1
process.QCDfromSmearing.SmearCollection = 'Reco'
#process.QCDfromSmearing.SmearCollection = 'Gen'
process.QCDfromSmearing.PtBinEdges_scaling = cms.vdouble(0., 7000.)
process.QCDfromSmearing.EtaBinEdges_scaling = cms.vdouble(0.0, 5.0)
process.QCDfromSmearing.AdditionalSmearing = cms.vdouble(1.0)
process.QCDfromSmearing.LowerTailScaling = cms.vdouble(1.0)
process.QCDfromSmearing.UpperTailScaling = cms.vdouble(1.0)
process.QCDfromSmearing.SmearedJetPt = 0.
process.QCDfromSmearing.RebalanceJetPt = 15.
process.QCDfromSmearing.RebalanceMode = 'MHTall'
if runOnMC: process.QCDfromSmearing.treemakerWeightName = cms.InputTag('WeightProducer','weight')
else: process.QCDfromSmearing.treemakerWeightName = cms.InputTag('PrescaleWeightProducer','weight')
process.QCDfromSmearing.smearingWeightName = cms.InputTag('RandSJets','SmearingWeight')
process.QCDfromSmearing.ControlPlots = False
process.QCDfromSmearing.HTSeedMin = 0.
process.QCDfromSmearing.NJetsSeedMin = 2
process.QCDfromSmearing.NSmearsPerTry = cms.int32(NSmearsPerTry_global) #temporary fix
process.QCDfromSmearing.Ntries = cms.int32(Ntries_global)
process.QCDfromSmearing.NJetsSave = 2
process.QCDfromSmearing.HTSave = cms.double(200)
process.QCDfromSmearing.MHTSave = cms.double(200)
process.QCDfromSmearing.cleverPrescaleTreating = True
process.QCDfromSmearing.useRebalanceCorrectionFactors = False
process.QCDfromSmearing.useBTagEfficiencyFactors = False
process.QCDfromSmearing.useCleverRebalanceCorrectionFactors = False
process.QCDfromSmearing.MHTcut_low = cms.double(200.)
process.QCDfromSmearing.MHTcut_medium = cms.double(350.)
process.QCDfromSmearing.MHTcut_high = cms.double(500.)
process.QCDfromSmearing.HTcut_low = cms.double(500.)
process.QCDfromSmearing.HTcut_medium = cms.double(800.)
process.QCDfromSmearing.HTcut_high = cms.double(1000.)
process.QCDfromSmearing.HTcut_veryhigh = cms.double(1200.)
process.QCDfromSmearing.HTcut_extremehigh = cms.double(1400.)
###############################################################################

### Binning and Results from 11.Jan.2012 (with residual JEC)
process.QCDfromSmearing.PtBinEdges_scaling = cms.vdouble(0, 5000)
process.QCDfromSmearing.EtaBinEdges_scaling = cms.vdouble(0, 0.5, 0.8, 1.1, 1.3, 1.7, 1.9, 2.1, 2.3, 2.5, 2.8, 3.0, 3.2, 5.0)
process.QCDfromSmearing.LowerTailScaling = cms.vdouble(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 )
process.QCDfromSmearing.UpperTailScaling = cms.vdouble(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 )
process.QCDfromSmearing.AdditionalSmearing = cms.vdouble(1.122, 1.167, 1.168, 1.029, 1.115, 1.041, 1.167, 1.094, 1.168, 1.266, 1.595, 0.998, 1.226)
process.QCDfromSmearing.PtBinEdges_scaling = cms.vdouble(0, 7000)
process.QCDfromSmearing.EtaBinEdges_scaling = cms.vdouble(0, 0.5, 0.8, 1.1, 1.3, 1.7, 1.9, 2.1, 2.3, 2.5, 2.8, 3.0, 3.2, 5.0)
process.QCDfromSmearing.LowerTailScaling = cms.vdouble(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 )
process.QCDfromSmearing.UpperTailScaling = cms.vdouble(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 )
process.QCDfromSmearing.AdditionalSmearing = cms.vdouble(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 )
### Very simple tail and core scalings for uncertainty checks
#process.QCDfromSmearingCoreUP = process.QCDfromSmearing.clone()
#process.QCDfromSmearingCoreUP.AdditionalSmearing = cms.vdouble(1.222, 1.267, 1.268, 1.129, 1.215, 1.141, 1.267, 1.194, 1.268, 1.366, 1.695, 1.098, 1.326)
#process.QCDfromSmearingCoreUP.uncertaintyName = 'CoreUP'
process.QCDfromSmearingCoreDN = process.QCDfromSmearing.clone()
process.QCDfromSmearingCoreDN.AdditionalSmearing = cms.vdouble(1.022, 1.067, 1.068, 0.929, 1.015, 0.941, 1.067, 0.994, 1.068, 1.166, 1.495, 0.898, 1.126)
process.QCDfromSmearingCoreDN.uncertaintyName = 'CoreDN'
process.QCDfromSmearingTailUP = process.QCDfromSmearing.clone()
process.QCDfromSmearingTailUP.LowerTailScaling = cms.vdouble(1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5 )
process.QCDfromSmearingTailUP.UpperTailScaling = cms.vdouble(1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5 )
process.QCDfromSmearingTailUP.uncertaintyName = 'TailUP'
#process.QCDfromSmearingTailDN = process.QCDfromSmearing.clone()
#process.QCDfromSmearingTailDN.LowerTailScaling = cms.vdouble(0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5 )
#process.QCDfromSmearingTailDN.UpperTailScaling = cms.vdouble(0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5 )
#process.QCDfromSmearingTailDN.uncertaintyName = 'TailDN'



## --- Rebalanced Jets producer ----------------------------------------------------
print "*** R+S Jets producer **************************************************"
from  TreeMaker.Utils.randsjetsproducer_cfi import randsjetsProducer
process.RandSJets = randsjetsProducer.clone(
    jetCollection  = cms.InputTag('GoodJets'),
    genjetCollection = cms.InputTag('slimmedGenJets'),
    SmearingFile = process.QCDfromSmearing.SmearingFile,
    NTries  = process.QCDfromSmearing.Ntries,#cms.int32(Ntries_global),#
    NSmearsPerTry  = process.QCDfromSmearing.NSmearsPerTry,#cms.int32(NSmearsPerTry_global),#
    treemakerWeightName = process.QCDfromSmearing.treemakerWeightName,
    PrintJets = process.QCDfromSmearing.PrintJets,#cms.bool(True)#
    doGenSmear = cms.bool(False)#
    )
#setattr(process,"RandSJets",RandSJets)
process.Baseline += process.RandSJets
VectorRecoCand.extend(['RandSJets:RebalancedJets'])
VarsBool.extend(['RandSJets:FitSucceed'])


print 'process.QCDfromSmearing.Ntries is ', process.QCDfromSmearing.Ntries
itry = 0
while  itry< Ntries_global:
    iextra = 0
    print 'itry', itry
    while iextra<NSmearsPerTry_global:
        print 'iextra', iextra
        updateJetCollection(
            process,
            jetSource = cms.InputTag('RandSJets','RandSJetsTry'+str(itry)+'Smear'+str(iextra)),
            #jetSource = cms.InputTag('GoodJets'),
            labelName = 'Try'+str(itry)+'Smear'+str(iextra),
            #postfix = 'Try'+str(itry)+'Smear'+str(iextra),
            jetCorrections = ('AK4PFchs', cms.vstring(['L1FastJet', 'L2Relative', 'L3Absolute']), 'None'),
            #jetCorrections = ('AK4PFchs', cms.vstring([]), 'None'),
            btagDiscriminators = ['pfCombinedInclusiveSecondaryVertexV2BJetTags']
            )
        iextra+=1
    itry+=1


print "*** Treemaker setup **************************************************"
from TreeMaker.TreeMaker.treeMaker import TreeMaker
process.RA2TreeMaker = TreeMaker.clone(
    TreeName       = cms.string("PreSelection"),
    VectorRecoCand   = VectorRecoCand,
    VarsDouble     = VarsDouble,
    VectorDouble   = VectorDouble,
    VarsInt        = VarsInt,
    VectorInt      = VectorInt,
    VarsBool     = VarsBool,
)


###############################################################################

###############################################################################
process.dump   = cms.EDAnalyzer("EventContentAnalyzer")
###############################################################################


## --- MET Filters -----------------------------------------------
print "*** MET Filters **************************************************"

# Dead ECAL cell primitive filter
from RecoMET.METFilters.metFilters_cff import EcalDeadCellTriggerPrimitiveFilter
process.EcalDeadCellTriggerPrimitiveFilter = EcalDeadCellTriggerPrimitiveFilter.clone()
# HBHE noise filter
#from CommonTools.RecoAlgos import HBHENoiseFilterResultProducer
#from CommonTools.RecoAlgos import HBHENoiseFilter
#process.HBHENoiseFilter = HBHENoiseFilter.clone()
#process.HBHENoiseFilterResultProducer = HBHENoiseFilterResultProducer.clone()
from CommonTools.RecoAlgos.HBHENoiseFilterResultProducer_cfi import *
process.HBHENoiseFilterResultProducer = HBHENoiseFilterResultProducer.clone()
from CommonTools.RecoAlgos.HBHENoiseFilter_cfi import *
process.HBHENoiseFilter = HBHENoiseFilter.clone()

process.metFilters = cms.Sequence(
   process.HBHENoiseFilterResultProducer *
   process.HBHENoiseFilter *
   #primaryVertexFilter*
   #HBHENoiseIsoFilter*
   #CSCTightHaloFilter *
   #hcalLaserEventFilter *
   process.EcalDeadCellTriggerPrimitiveFilter
   #*goodVertices * trackingFailureFilter *
   #eeBadScFilter
   #ecalLaserCorrFilter *
   #trkPOGFilters
)

from TreeMaker.Utils.filterdecisionproducer_cfi import filterDecisionProducer

process.globalTightHalo2016Filter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_globalTightHalo2016Filter"),
)
process.Baseline += process.globalTightHalo2016Filter
VarsInt.extend(['globalTightHalo2016Filter'])

process.HBHENoiseFilter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_HBHENoiseFilter"),
)
process.Baseline += process.HBHENoiseFilter
VarsInt.extend(['HBHENoiseFilter'])

process.HBHEIsoNoiseFilter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_HBHENoiseIsoFilter"),
)
process.Baseline += process.HBHEIsoNoiseFilter
VarsInt.extend(['HBHEIsoNoiseFilter'])

process.EcalDeadCellTriggerPrimitiveFilter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_EcalDeadCellTriggerPrimitiveFilter"),
)
process.Baseline += process.EcalDeadCellTriggerPrimitiveFilter
VarsInt.extend(['EcalDeadCellTriggerPrimitiveFilter'])

process.eeBadScFilter = filterDecisionProducer.clone(
    trigTagArg1  = cms.string('TriggerResults'),
    trigTagArg2  = cms.string(''),
    trigTagArg3  = cms.string(tagName),
    filterName  =   cms.string("Flag_eeBadScFilter"),
    )
process.Baseline += process.eeBadScFilter
VarsInt.extend(['eeBadScFilter'])

# some filters need to be rerun
process.load('RecoMET.METFilters.BadChargedCandidateFilter_cfi')
process.BadChargedCandidateFilter.muons = cms.InputTag("slimmedMuons")
process.BadChargedCandidateFilter.PFCandidates = cms.InputTag("packedPFCandidates")
process.BadChargedCandidateFilter.taggingMode = True
process.Baseline += process.BadChargedCandidateFilter
VarsBool.extend(['BadChargedCandidateFilter'])

process.load('RecoMET.METFilters.BadPFMuonFilter_cfi')
process.BadPFMuonFilter.muons = cms.InputTag("slimmedMuons")
process.BadPFMuonFilter.PFCandidates = cms.InputTag("packedPFCandidates")
process.BadPFMuonFilter.taggingMode = True
process.Baseline += process.BadPFMuonFilter
VarsBool.extend(['BadPFMuonFilter'])

## --- NoMuonJet producer -----------------------------------------------
from TreeMaker.Utils.nomuonjet_cfi import nomuonjet
process.NoMuonInt = nomuonjet.clone(
                                        JetTag     = cms.InputTag('HTJets'),
                                        MHTPhiTag  = cms.InputTag('MET:Phi'),#changed from MHT
)
process.Baseline += process.NoMuonInt
VarsInt.extend(['NoMuonInt:NoMuonInt'])



process.prediction = cms.Path(
    process.Baseline 
    * process.QCDfromSmearing
    )

process.mc = cms.Path(
    process.Baseline
    * process.RA2TreeMaker
    )


open('dumppy.py','w').write(process.dumpPython())

