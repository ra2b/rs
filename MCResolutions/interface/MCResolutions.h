#ifndef MCResolutions_H
#define MCResolutions_H

// system include files
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <iostream>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/EDMException.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"

#include "DataFormats/Math/interface/LorentzVector.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Math/interface/deltaPhi.h"
#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/JetReco/interface/CaloJet.h"
#include "DataFormats/JetReco/interface/GenJet.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TFile.h"

// b-Tagging
#include "DataFormats/BTauReco/interface/JetTag.h"

//
// class declaration
//

class MCResolutions: public edm::EDAnalyzer {
   public:
   explicit MCResolutions(const edm::ParameterSet&);
   ~MCResolutions();

   private:
   virtual void beginJob();
   virtual void analyze(const edm::Event&, const edm::EventSetup&);
   virtual void endJob();

   // ----------member data ---------------------------
   edm::InputTag _jetTag;
   edm::InputTag _leptonTag;
   std::string _btagTag;
   edm::InputTag _genJetTag;
   edm::InputTag _weightName;

   edm::InputTag _genParticlesTag;


   
	edm::InputTag NoMuonFilter_;
	edm::InputTag HBHENoiseFilter_;
   edm::InputTag HBHEIsoNoiseFilter_;
   edm::InputTag eeBadScFilter_;
   edm::InputTag ecalDeadCellFilter_;
   edm::InputTag globalTightHaloFilter_;
   edm::InputTag badChargedCandidateFilter_;
   edm::InputTag badMuonFilter_;
	edm::InputTag jetIDFilter_;
   
   edm::InputTag PFCaloRatio_;
	edm::InputTag NVtxFilter_;


   edm::EDGetTokenT<int> nleptons_;
   edm::EDGetTokenT<double> weight_;
   edm::EDGetTokenT<edm::View<reco::GenJet> > genJet_;
   edm::EDGetTokenT<edm::View<pat::Jet> > jet_;
   edm::EDGetTokenT<edm::View<pat::PackedGenParticle> > genParticles_;

//   edm::EDGetTokenT<edm::Association<std::vector<pat::PackedGenParticle> > > genParticles_;
//
	edm::EDGetTokenT<int> NoMuonToken_;
	edm::EDGetTokenT<int> HBHENoiseToken_;
	edm::EDGetTokenT<int> HBHEIsoNoiseToken_;
	edm::EDGetTokenT<int> eeBadScToken_;
	edm::EDGetTokenT<int> ecalDeadCellToken_;
	edm::EDGetTokenT<int> globalTightHaloToken_;
	edm::EDGetTokenT<bool> badChargedCandidateToken_;
	edm::EDGetTokenT<bool> badMuonToken_;
	edm::EDGetTokenT<bool> jetIDToken_;

   edm::EDGetTokenT<double> PFCaloRatioToken_;
   edm::EDGetTokenT<int> NVtxToken_;


   double _btagCut;
   double _deltaRMatch;
   double _deltaRMatchVeto;
   double _absPtVeto;
   double _relPtVeto;
   double _GenJetPtCut;
   std::string _fileName;

   bool _genBinning;
   bool _neutrinosInBinning;
   bool _useFilters;
   bool _energyBinning;

   double weight;

   // JetResponse in Pt and eta bins
   int PtBin(const double& pt);
   int EtaBin(const double& eta);

   // Resize histo vectors
   void ResizeHistoVector(std::vector<std::vector<TH1F*> > &histoVector);

   // total
   std::vector<std::vector<TH1F*> > h_tot_JetAll_JetResPt_Pt;
   std::vector<std::vector<TH1F*> > h_tot_JetAll_JetResPhi_Pt;
   std::vector<std::vector<TH1F*> > h_tot_JetAll_JetResEta_Pt;
   // with btag
   std::vector<std::vector<TH1F*> > h_b_JetAll_JetResPt_Pt;
   std::vector<std::vector<TH1F*> > h_b_JetAll_JetResPhi_Pt;
   std::vector<std::vector<TH1F*> > h_b_JetAll_JetResEta_Pt;
   // without btag
   std::vector<std::vector<TH1F*> > h_nob_JetAll_JetResPt_Pt;
   std::vector<std::vector<TH1F*> > h_nob_JetAll_JetResPhi_Pt;
   std::vector<std::vector<TH1F*> > h_nob_JetAll_JetResEta_Pt;
   //
   // Btag efficiencies
   std::vector<TH1F*> h_trueb_RecoPt;
   std::vector<TH1F*> h_no_trueb_RecoPt;
   std::vector<TH1F*> h_trueb_btag_RecoPt;
   std::vector<TH1F*> h_no_trueb_btag_RecoPt;
   std::vector<TH1F*> h_no_trueb_no_btag_RecoPt;
   std::vector<TH1F*> h_trueb_no_btag_RecoPt;
   std::vector<TH1F*> h_btag_RecoPt;
   std::vector<TH1F*> h_no_btag_RecoPt;

   std::vector<double> PtBinEdges;
   std::vector<double> EtaBinEdges;

   TFile* hfile;

};

#endif
