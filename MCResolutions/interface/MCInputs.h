#ifndef MCInputs_H
#define MCInputs_H

// system include files
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <iostream>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/EDMException.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"

#include "DataFormats/Math/interface/LorentzVector.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Math/interface/deltaPhi.h"
#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/JetReco/interface/CaloJet.h"
#include "DataFormats/JetReco/interface/GenJet.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "rs/QCDBkgRS/interface/UsefulJet.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TFile.h"

// b-Tagging
#include "DataFormats/BTauReco/interface/JetTag.h"

//
// class declaration
//

class MCInputs: public edm::EDAnalyzer {
 public:
  explicit MCInputs(const edm::ParameterSet&);
  ~MCInputs();

 private:
  virtual void beginJob();
  virtual void analyze(const edm::Event&, const edm::EventSetup&);
  virtual void endJob();


  // ----------member data ---------------------------
  edm::InputTag _jetTag;
  edm::InputTag _leptonTag;
  std::string _btagTag;
  edm::InputTag _genJetTag;
  edm::InputTag _weightName;

  edm::InputTag _genParticlesTag;


   
  edm::InputTag NoMuonFilter_;
  edm::InputTag HBHENoiseFilter_;
  edm::InputTag HBHEIsoNoiseFilter_;
  edm::InputTag eeBadScFilter_;
  edm::InputTag ecalDeadCellFilter_;
  edm::InputTag globalTightHaloFilter_;
  edm::InputTag badChargedCandidateFilter_;
  edm::InputTag badMuonFilter_;
  edm::InputTag jetIDFilter_;
   
  edm::InputTag PFCaloRatio_;
  edm::InputTag NVtxFilter_;


  edm::EDGetTokenT<int> nleptons_;
  edm::EDGetTokenT<double> weight_;
  edm::EDGetTokenT<edm::View<reco::GenJet> > genJet_;
  edm::EDGetTokenT<edm::View<pat::Jet> > jet_;
  edm::EDGetTokenT<edm::View<pat::PackedGenParticle> > genParticles_;

  //   edm::EDGetTokenT<edm::Association<std::vector<pat::PackedGenParticle> > > genParticles_;
  //
  edm::EDGetTokenT<int> NoMuonToken_;
  edm::EDGetTokenT<int> HBHENoiseToken_;
  edm::EDGetTokenT<int> HBHEIsoNoiseToken_;
  edm::EDGetTokenT<int> eeBadScToken_;
  edm::EDGetTokenT<int> ecalDeadCellToken_;
  edm::EDGetTokenT<int> globalTightHaloToken_;
  edm::EDGetTokenT<bool> badChargedCandidateToken_;
  edm::EDGetTokenT<bool> badMuonToken_;
  edm::EDGetTokenT<bool> jetIDToken_;

  edm::EDGetTokenT<double> PFCaloRatioToken_;
  edm::EDGetTokenT<int> NVtxToken_;


  double _btagCut;
  double _deltaRMatch;
  double _deltaRMatchVeto;
  double _absPtVeto;
  double _relPtVeto;
  double _GenJetPtCut;
  std::string _fileName;

  bool _genBinning;
  bool _neutrinosInBinning;
  bool _useFilters;
  bool _energyBinning;

  double weight;

  // with btag
  std::vector<std::vector<TH1F*> > vvHeavyJetResponse;
  // without btag
  std::vector<std::vector<TH1F*> > vvLightJetResponse;
  //

  std::vector<double> EnBinEdges;
  std::vector<double> EtaBinEdges;
  std::vector<double> HtBinEdges;
  std::vector<double> MhtBinEdges;
  std::vector<double> MhtPhiBinEdges;

  TH1F* hNull;
  TH1F* hEtaTemplate;
  TAxis* templateEtaAxis;
  TH1F* hEnTemplate;
  TAxis* templateEnAxis;
  TH1F* hHtTemplate;
  TAxis* templateHtAxis;
  TH1F* hHt;
  TH1F* hHtWeighted;

  std::vector<TH1F*> hMhtPtTemplatesB0;
  std::vector<TH1F*> hMhtPtTemplatesB1;
  std::vector<TH1F*> hMhtPtTemplatesB2;
  std::vector<TH1F*> hMhtPtTemplatesB3;
  std::vector<TH1F*> hMhtPhiTemplatesB0;
  std::vector<TH1F*> hMhtPhiTemplatesB1;
  std::vector<TH1F*> hMhtPhiTemplatesB2;
  std::vector<TH1F*> hMhtPhiTemplatesB3;

  TFile* hfile;

};

#endif
