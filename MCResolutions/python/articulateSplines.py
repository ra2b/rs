#this module should work just like hadd
from ROOT import *
import glob, os, sys
import numpy as np
from utils import *


try: newfile = sys.argv[1]
except: newfile = 'newtemplates.root'
try: bunchofiles = sys.argv[2]
except: bunchofiles = 'xx.root'

    #'''
#os.system('rm rootyard/*.root')
print 'python python/ahadd.py -f '+newfile+' '+bunchofiles
os.system('python python/ahadd.py -f '+newfile+' '+bunchofiles)
#'''
    
'''
refugees = glob.glob(bunchofiles)
for refugee in refugees:
    iszombie = False
    ftemp = TFile(refugee)
    if ftemp.IsZombie(): iszombie = True
    ftemp.Close()
    if not iszombie: 
        print 'moving', refugee, 'into place.'
        os.system('cp '+refugee+' rootyard')


os.system('rm '+newfile)
os.system('hadd '+newfile+' rootyard/*.root')
'''

f = TFile(newfile,'update')
keys = f.GetListOfKeys()
print 'the first key is', keys[0]
dir_ = f.mkdir('splines')
dir_.cd()
nsmooth = 5
for key in keys:
    name = key.GetName()
    if not (('hRTemplate(gEn' in name) or ('hGenMht' in name)): continue
    h = f.Get(name)
    print 'name=',name
    h.SetBinContent(1,0)
    try: h.Scale(1.0/h.Integral(-1,9999),'width')
    except: pass
    h.Smooth(nsmooth)
    g = TGraph(h)
    s3 = TSpline3("s3",g.GetX(),g.GetY(),g.GetN())
    lowerbound = 0
    upperbound = 3.5
    #if 'hRTemplate(gPt' in name and int(name[name.find('gPt')+3:name.find('.')])<17: upperbound = 4.0
    if 'hRTemplate(gEn' in name and int(name[name.find('gEn')+3:name.find('.')])<30: upperbound = 4.5        
    if 'GenMhtPhi' in name: 
        upperbound = 3.142
        lowerbound = -3.142
    if 'GenMhtPt' in name: 
        upperbound = 800.0#1.0
    def splineFunc(x,p):
        return s3.Eval(x[0])
    func = TF1('func',splineFunc,lowerbound,upperbound,1)
    func.SetNpx(10000)
    if 'En' in name:
        a = 1
        #h.Draw('hist')
        #c1.SetLogy()
        #func.Draw('same')
        #c1.Update()
        #pause()
    print 'writing', name, '_graph'
    g.Write(name+'_graph')
    #func.Write(name+'_spline')
f.Close()
    
