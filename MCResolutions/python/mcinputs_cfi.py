import FWCore.ParameterSet.Config as cms

MCInputs = cms.EDAnalyzer('MCInputs',
   leptonTag				= cms.InputTag('GoodLeptons'),
	jetTag					= cms.InputTag('GoodJets'),
	packedGenParticles	= cms.InputTag('packedGenParticles'),
   btagTag					= cms.string('pfCombinedInclusiveSecondaryVertexV2BJetTags'),
   btagCut					= cms.double(0.800),
	genJetTag				= cms.InputTag('slimmedGenJets'),
	weightName				= cms.InputTag('WeightProducer','weight','ResponseTemplates'),
	deltaRMatch				= cms.double(0.5),
	deltaRMatchVeto		= cms.double(0.7),
	absPtVeto				= cms.double(20.),
	relPtVeto				= cms.double(0.01),
	GenJetPtCut				= cms.double(0.),
   fileName					= cms.string('MCEventInputs.root'),
   genBinning			 	= cms.bool(True),
   neutrinosInBinning	= cms.bool(True),
   useFilters				= cms.bool(True),
   energyBinning			= cms.bool(True),

   PFCaloRatio				= cms.InputTag('MET:PFCaloPtRatio'),
   NVtx						= cms.InputTag('NVtx'),

   NoMuon					= cms.InputTag('NoMuonInt:NoMuonInt'),
   HBHENoise				= cms.InputTag('HBHENoiseFilter'),
   HBHEIsoNoise			= cms.InputTag('HBHEIsoNoiseFilter'),
   eeBadSc					= cms.InputTag('eeBadScFilter'),
   ecalDeadCell			= cms.InputTag('EcalDeadCellTriggerPrimitiveFilter'),
   globalTightHalo		= cms.InputTag('globalTightHalo2016Filter'),
   badChargedCandidate	= cms.InputTag('BadChargedCandidateFilter'),
   badMuon					= cms.InputTag('BadPFMuonFilter'),
   jetID						= cms.InputTag('GoodJets:JetID')

   

)
