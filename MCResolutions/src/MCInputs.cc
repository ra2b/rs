// -*- C++ -*-
//
// Package:    MCInputs
// Class:      MCInputs
//
/**\class MCInputs MCInputs.cc JetResolutionFromMC/MCInputs/src/MCInputs.cc
 
   Description: [one line class summary]
 
   Implementation:
   [Notes on implementation]
*/
//
// Original Author:  Sam bein
// template taken from MCResolutions.cc by Christian Sander,
//         Created:  Wed Oct  6 18:22:21 CEST 2010
// $Id: MCInputs.cc,v 1.1 2012/08/01 13:31:08 kheine Exp $
//
//
#include "rs/MCResolutions/interface/MCInputs.h"
#include <iostream>

using namespace std;

//
// constructors and destructor
//
MCInputs::MCInputs(const edm::ParameterSet& iConfig) {
  //now do what ever initialization is needed
  _leptonTag = iConfig.getParameter<edm::InputTag> ("leptonTag");
  _jetTag = iConfig.getParameter<edm::InputTag> ("jetTag");
  _btagTag = iConfig.getParameter<std::string> ("btagTag");
  _btagCut = iConfig.getParameter<double> ("btagCut");
  _genJetTag = iConfig.getParameter<edm::InputTag> ("genJetTag");
  _weightName = iConfig.getParameter<edm::InputTag> ("weightName");
  _deltaRMatch = iConfig.getParameter<double> ("deltaRMatch");
  _deltaRMatchVeto = iConfig.getParameter<double> ("deltaRMatchVeto");
  _absPtVeto = iConfig.getParameter<double> ("absPtVeto");
  _relPtVeto = iConfig.getParameter<double> ("relPtVeto");
  _GenJetPtCut = iConfig.getParameter<double> ("GenJetPtCut");
  _fileName = iConfig.getParameter<std::string> ("fileName");
  _neutrinosInBinning = iConfig.getParameter<bool> ("neutrinosInBinning");
  _useFilters = iConfig.getParameter<bool> ("useFilters");
  _energyBinning = iConfig.getParameter<bool> ("energyBinning");

  //   _genParticlesTag = iConfig.getParameter<edm::InputTag> ("packedGenParticles");
  _genParticlesTag = iConfig.getParameter<edm::InputTag>("packedGenParticles");


  NoMuonFilter_              = iConfig.getParameter<edm::InputTag> ("NoMuon");
  HBHENoiseFilter_           = iConfig.getParameter<edm::InputTag> ("HBHENoise");
  HBHEIsoNoiseFilter_        = iConfig.getParameter<edm::InputTag> ("HBHEIsoNoise");
  eeBadScFilter_             = iConfig.getParameter<edm::InputTag> ("eeBadSc");
  ecalDeadCellFilter_        = iConfig.getParameter<edm::InputTag> ("ecalDeadCell");
  globalTightHaloFilter_     = iConfig.getParameter<edm::InputTag> ("globalTightHalo");
  badChargedCandidateFilter_ = iConfig.getParameter<edm::InputTag> ("badChargedCandidate");
  badMuonFilter_             = iConfig.getParameter<edm::InputTag> ("badMuon");
  jetIDFilter_               = iConfig.getParameter<edm::InputTag> ("jetID");

  PFCaloRatio_ = iConfig.getParameter<edm::InputTag> ("PFCaloRatio");
  NVtxFilter_ = iConfig.getParameter<edm::InputTag> ("NVtx");
   
  NoMuonToken_ = consumes<int>(NoMuonFilter_);
  HBHENoiseToken_ = consumes<int>(HBHENoiseFilter_);
  HBHEIsoNoiseToken_ = consumes<int>(HBHEIsoNoiseFilter_);
  eeBadScToken_ = consumes<int>(eeBadScFilter_);
  ecalDeadCellToken_ = consumes<int>(ecalDeadCellFilter_);
  globalTightHaloToken_ = consumes<int>(globalTightHaloFilter_);
  badChargedCandidateToken_ = consumes<bool>(badChargedCandidateFilter_);
  badMuonToken_ = consumes<bool>(badMuonFilter_);
  jetIDToken_ = consumes<bool>(jetIDFilter_);
  PFCaloRatioToken_ = consumes<double>(PFCaloRatio_);
  NVtxToken_ = consumes<int>(NVtxFilter_);
  nleptons_ = consumes<int>(_leptonTag);
  weight_ = consumes<double>(_weightName);
  genJet_ = consumes<edm::View<reco::GenJet> >(_genJetTag); 
  jet_ = consumes<edm::View<pat::Jet> >(_jetTag); 
  genParticles_ = consumes<edm::View<pat::PackedGenParticle> >(_genParticlesTag); 


  hfile = new TFile(_fileName.c_str(), "RECREATE", "Jet response in pT and eta bins");

}

MCInputs::~MCInputs() {
   
  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
   
}

//
// member functions
//

// ------------ method called to for each event  ------------
void MCInputs::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace std;
   
  edm::Handle<int> HBHENoiseHandle_;
  edm::Handle<int> HBHEIsoNoiseHandle_;
  edm::Handle<int> eeBadScHandle_;
  edm::Handle<int> ecalDeadCellHandle_;
  edm::Handle<int> globalTightHaloHandle_;
  edm::Handle<int> NVtxHandle_;
  edm::Handle<int> NoMuonHandle_;
  edm::Handle<bool> badChargedCandidateHandle_;
  edm::Handle<bool> badMuonHandle_;
  edm::Handle<bool> jetIDHandle_;

	
  iEvent.getByToken(NoMuonToken_,NoMuonHandle_);
  iEvent.getByToken(HBHENoiseToken_,HBHENoiseHandle_);
  iEvent.getByToken(HBHEIsoNoiseToken_,HBHEIsoNoiseHandle_);
  iEvent.getByToken(eeBadScToken_,eeBadScHandle_);
  iEvent.getByToken(ecalDeadCellToken_,ecalDeadCellHandle_);
  iEvent.getByToken(globalTightHaloToken_,globalTightHaloHandle_);
  iEvent.getByToken(jetIDToken_,jetIDHandle_);
  iEvent.getByToken(NVtxToken_,NVtxHandle_);


  //LeptonVeto
  edm::Handle<int> NLeptons;
  iEvent.getByToken(nleptons_, NLeptons);
  edm::Handle<double> FilterPFCaloRatio;
  iEvent.getByToken(PFCaloRatioToken_, FilterPFCaloRatio);

  if ((*NoMuonHandle_) == 0 ||
      (*HBHENoiseHandle_) == 0 ||
      (*HBHEIsoNoiseHandle_) == 0 ||
      (*eeBadScHandle_) == 0 ||
      (*ecalDeadCellHandle_) == 0 ||
      !(*jetIDHandle_) ||
      (*NVtxHandle_) == 0 ||
      (*FilterPFCaloRatio) > 5)
    return;
   
  //Weight
  edm::Handle<double> event_weight;
  bool findWeight = iEvent.getByToken(weight_, event_weight);
  weight = (event_weight.isValid() ? (*event_weight) : 1.0);
  if (!findWeight) {
    cout << "Weight not found!" << endl;
  }


  //GenJets
  edm::Handle<edm::View<reco::GenJet> > Jets_gen;
  iEvent.getByToken(genJet_, Jets_gen);
   
  //RecoJets
  edm::Handle<edm::View<pat::Jet> > Jets_rec;
  iEvent.getByToken(jet_, Jets_rec);

  std::vector<UsefulJet> recjets;
  int irec = -1;
  for (edm::View<pat::Jet>::const_iterator jt = Jets_rec-> begin(); jt != Jets_rec->end(); ++jt)
    {
      irec+=1;
      if (! (abs(jt->eta())<5.0) ) continue;
      TLorentzVector recjetTlv; 
      recjetTlv.SetPtEtaPhiE(jt->pt(),jt->eta(),jt->phi(),jt->energy());
      UsefulJet recjet(recjetTlv, jt->bDiscriminator(_btagTag), irec);
      recjets.push_back(recjet);
    }
  double htReco = getHT(recjets, 30);
  hHt->Fill(htReco);
  //std::cout << "weight is " << weight << std::endl;
  hHtWeighted->Fill(htReco, weight);
   
  edm::Handle<edm::View<pat::PackedGenParticle> > genParticles;
  iEvent.getByLabel("packedGenParticles", genParticles);

  std::vector<UsefulJet> genjets;
  for (edm::View<reco::GenJet>::const_iterator it = Jets_gen->begin(); it != Jets_gen->end(); ++it) {
    if (! (abs(it->eta())<5.0) ) continue;
    TLorentzVector genjetTlv; 
    genjetTlv.SetPtEtaPhiE(it->pt(),it->eta(),it->phi(),it->energy());
    UsefulJet genjet(genjetTlv, 0, 0);
    genjets.push_back(genjet);
      
    //// First look if there is no significant GenJet near the tested GenJet
    double dRgenjet = 999.; double PtdRmin = 0;
    for (edm::View<reco::GenJet>::const_iterator kt = Jets_gen-> begin(); kt != Jets_gen->end(); ++kt) {
      if (&(*it) == &(*kt))
	continue;
      double dR = deltaR(*it, *kt);
      if (dR < dRgenjet) {
	dRgenjet = dR;
	PtdRmin = kt->pt();
	if (dRgenjet < _deltaRMatchVeto && (PtdRmin / it->pt()) > _relPtVeto) break;//sammer
      }
    }
    //cout << "Gen pT: " << it->pt() << " closest genJet pT: " << PtdRmin << " dR: " << dRgenjet << endl;
    if (dRgenjet < _deltaRMatchVeto && (PtdRmin / it->pt()) > _relPtVeto)
      continue;
    
    const pat::Jet* matchedJet = 0;
    double dRnearest = 999.;
    
    //	here you do the matching
    int nMatchedReco = 0;
    for (edm::View<pat::Jet>::const_iterator jt = Jets_rec-> begin(); jt != Jets_rec->end(); ++jt) {
      double dR = deltaR(*it, *jt);
      if (dR < dRnearest) {
	dRnearest = dR;
	matchedJet = &(*jt);
      }
      if(dR<_deltaRMatchVeto) nMatchedReco+=1;
    }
    if (nMatchedReco>1) continue;
    double res = 0;
    if(dRnearest<_deltaRMatch) res = matchedJet->pt() / (it->p4()).pt();
    //// Use b-tag for heavy flavour ID
    bool bTag = false;
    int ieta = TMath::Min(templateEtaAxis->FindBin(abs(it->eta())), templateEtaAxis->GetNbins());
    int ienergy = TMath::Min(templateEnAxis->FindBin(abs(it->energy())),  templateEnAxis->GetNbins());
    if ( matchedJet!=0 && matchedJet->bDiscriminator(_btagTag) > _btagCut) bTag = true; 
    if (bTag) vvHeavyJetResponse[ieta][ienergy]->Fill(res);
    else vvLightJetResponse[ieta][ienergy]->Fill(res);
  }


  EndowGenJets(recjets, genjets);
  TLorentzVector gmht = getMHT(genjets, lhdMhtThresh);
  double ght = getHT(genjets, lhdMhtThresh);
  int iht = TMath::Min(templateHtAxis->FindBin(ght), templateHtAxis->GetNbins());
  int nb = countBTags(genjets, lhdMhtThresh);
  if(nb==0)
    {
      hMhtPtTemplatesB0[iht]->Fill(gmht.Pt(), weight);
      hMhtPhiTemplatesB0[iht]->Fill(gmht.DeltaPhi(genjets[0].tlv), weight);
    }
  else
    {
      int ib; UsefulJet leadingbjet;
      getLeadingBJet_CC(genjets, ib, nb, leadingbjet);
      if(nb==1)
	{
	  hMhtPtTemplatesB1[iht]->Fill(gmht.Pt(), weight);
	  hMhtPhiTemplatesB1[iht]->Fill(gmht.DeltaPhi(leadingbjet.tlv), weight);
	}
      else if(nb==2)
	{
	  hMhtPtTemplatesB2[iht]->Fill(gmht.Pt(), weight);
	  hMhtPhiTemplatesB2[iht]->Fill(gmht.DeltaPhi(leadingbjet.tlv), weight);
	}
      else
	{
	  hMhtPtTemplatesB3[iht]->Fill(gmht.Pt(), weight);
	  hMhtPhiTemplatesB3[iht]->Fill(gmht.DeltaPhi(leadingbjet.tlv), weight);
	}
    }

}


// ------------ method called once each job just before starting event loop  ------------
void MCInputs::beginJob() {

  hNull = new TH1F("hNull","hNull",1,0,1);
  hHt = new TH1F("hHt","hHt",120,0,2500);
  hHt->Sumw2();
  hHtWeighted = new TH1F("hHtWeighted","hHtWeighted",120,0,2500);
  hHtWeighted->Sumw2();

  //sorry this is my morbid sense of humor:
  double EnBinArray[]={0,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,44,48,52,56,60,66,72,78,84,90,100,110,120,130,140,150,160,170,180,190,200,220,240,260,280,300,330,360,390,420,450,500,550,600,650,700,750,800,900,1000,1100,1200,1300,1400,1500,1600,1800,2000,10000};for(unsigned int i=0;i<(sizeof(EnBinArray)/sizeof(EnBinArray[0]));i++)EnBinEdges.push_back(EnBinArray[i]); double EtaBinArray[]={0.0,0.5,0.8,1.1,1.3,1.7,1.9,2.1,2.3,2.5,2.8,3.0,3.2,4.7,5.1};for(unsigned int i=0;i<(sizeof(EtaBinArray)/sizeof(EtaBinArray[0]));i++)EtaBinEdges.push_back(EtaBinArray[i]); double HtBinArray[]={0,200,300,500,700,1000,1500,2000,10000}; for(unsigned int i=0;i<(sizeof(HtBinArray)/sizeof(HtBinArray[0]));i++)HtBinEdges.push_back(HtBinArray[i]);double MhtBinArray[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,155,160,165,170,175,180,185,190,195,200,220,240,260,280,300,400,500,800};for(unsigned int i=0;i<(sizeof(MhtBinArray)/sizeof(MhtBinArray[0]));i++)MhtBinEdges.push_back(MhtBinArray[i]);double MhtPhiBinArray[] = {-3.2, -3.136, -3.072, -3.008, -2.944, -2.88, -2.816, -2.752, -2.688, -2.624, -2.56, -2.496, -2.432, -2.368, -2.304, -2.24, -2.176, -2.112, -2.048, -1.984, -1.92, -1.856, -1.792, -1.728, -1.664, -1.6, -1.536, -1.472, -1.408, -1.344, -1.28, -1.216, -1.152, -1.088, -1.024, -0.96, -0.896, -0.832, -0.768, -0.704, -0.64, -0.576, -0.512, -0.448, -0.384, -0.32, -0.256, -0.192, -0.128, -0.064, 0.0, 0.064, 0.128, 0.192, 0.256, 0.32, 0.384, 0.448, 0.512, 0.576, 0.64, 0.704, 0.768, 0.832, 0.896, 0.96, 1.024, 1.088, 1.152, 1.216, 1.28, 1.344, 1.408, 1.472, 1.536, 1.6, 1.664, 1.728, 1.792, 1.856, 1.92, 1.984, 2.048, 2.112, 2.176, 2.24, 2.304, 2.368, 2.432, 2.496, 2.56, 2.624, 2.688, 2.752, 2.816, 2.88, 2.944, 3.008, 3.072, 3.136, 3.2};for(unsigned int i=0;i<(sizeof(MhtPhiBinArray)/sizeof(MhtPhiBinArray[0]));i++)MhtPhiBinEdges.push_back(MhtPhiBinArray[i]);


  int nBinEta = EtaBinEdges.size()-1;
  int nBinEn = EnBinEdges.size()-1;
  int nBinHt = HtBinEdges.size()-1;
  int nBinMht = MhtBinEdges.size()-1;
  int nBinMhtPhi = MhtPhiBinEdges.size()-1;
  
  hEtaTemplate = new TH1F("hEtaTemplate","hEtaTemplate",nBinEta,EtaBinArray);
  templateEtaAxis = (TAxis*)hEtaTemplate->GetXaxis();
  hEnTemplate = new TH1F("hEnTemplate","hEnTemplate",nBinEn,EnBinArray);
  templateEnAxis = (TAxis*)hEnTemplate->GetXaxis();
  hHtTemplate = new TH1F("hHtTemplate","hHtTemplate",nBinHt,HtBinArray);
  templateHtAxis = (TAxis*)hHtTemplate->GetXaxis();
  
  std::vector<TH1F*> vNullHisto; vNullHisto.push_back(hNull);
  vvHeavyJetResponse.push_back(vNullHisto);
  vvLightJetResponse.push_back(vNullHisto);

  for (unsigned int i_eta = 1; i_eta < EtaBinEdges.size() ; ++i_eta) {
    vvHeavyJetResponse.push_back(vNullHisto);
    vvLightJetResponse.push_back(vNullHisto);
    char etarangename[100];
    sprintf(etarangename,"%2.1f-%2.1f",templateEtaAxis->GetBinLowEdge(i_eta),templateEtaAxis->GetBinUpEdge(i_eta));
    for (unsigned int i_en = 1; i_en < EnBinEdges.size(); ++i_en) {
      //// Book histograms (all jet multiplicities)
      char enrangename[100];
      sprintf(enrangename,"%2.1f-%2.1f",templateEnAxis->GetBinLowEdge(i_en),templateEnAxis->GetBinUpEdge(i_en));
      char hname[100];
      sprintf(hname, "hRTemplate(gEn%s, gEta%s)", enrangename, etarangename);
      TH1F* h0 = new TH1F(hname, "en(gen)", 648, 0., 4); h0->Sumw2();
      vvLightJetResponse.back().push_back(h0);

      sprintf(hname, "hRTemplate(gEn%s, gEta%s)B", enrangename, etarangename);
      TH1F* hB = new TH1F(hname, "en(gen)", 400, 0., 4); hB->Sumw2();
      vvHeavyJetResponse.back().push_back(hB);
      if(!(vvHeavyJetResponse.back().back()==vvHeavyJetResponse[i_eta][i_en]))
	std::cout << "problem with the histogram indexing" << std::endl;
    }
  }
  hMhtPtTemplatesB0.push_back(hNull);
  hMhtPtTemplatesB1.push_back(hNull);
  hMhtPtTemplatesB2.push_back(hNull);
  hMhtPtTemplatesB3.push_back(hNull);
  hMhtPhiTemplatesB0.push_back(hNull);
  hMhtPhiTemplatesB1.push_back(hNull);
  hMhtPhiTemplatesB2.push_back(hNull);
  hMhtPhiTemplatesB3.push_back(hNull);
  for (unsigned int i_ht = 1; i_ht < HtBinEdges.size(); ++i_ht) {//can this only go up to 1? Or do we need it to go to 2 for the sake of interpolation? better leave it at 2 for the moment.
    char htrangename[100];
    sprintf(htrangename,"%2.1f-%2.1f",templateHtAxis->GetBinLowEdge(i_ht),templateHtAxis->GetBinUpEdge(i_ht));
    char hname[100];
    sprintf(hname, "hGenMhtPtB0(ght%s)", htrangename);
    TH1F* h0pt = new TH1F(hname, hname, nBinMht, MhtBinArray); h0pt->Sumw2();
    hMhtPtTemplatesB0.push_back(h0pt);
    sprintf(hname, "hGenMhtPtB1(ght%s)", htrangename);
    TH1F* h1pt = new TH1F(hname, hname, nBinMht, MhtBinArray); h1pt->Sumw2();
    hMhtPtTemplatesB1.push_back(h1pt);
    sprintf(hname, "hGenMhtPtB2(ght%s)", htrangename);
    TH1F* h2pt = new TH1F(hname, hname, nBinMht, MhtBinArray); h2pt->Sumw2();
    hMhtPtTemplatesB2.push_back(h2pt);
    sprintf(hname, "hGenMhtPtB3(ght%s)", htrangename);
    TH1F* h3pt = new TH1F(hname, hname, nBinMht, MhtBinArray); h3pt->Sumw2();
    hMhtPtTemplatesB3.push_back(h3pt);

    sprintf(hname, "hGenMhtPhiB0(ght%s)", htrangename);
    TH1F* h0phi = new TH1F(hname, hname, nBinMhtPhi, MhtPhiBinArray); h0phi->Sumw2();
    hMhtPhiTemplatesB0.push_back(h0phi);
    sprintf(hname, "hGenMhtPhiB1(ght%s)", htrangename);
    TH1F* h1phi = new TH1F(hname, hname, nBinMhtPhi, MhtPhiBinArray); h1phi->Sumw2();
    hMhtPhiTemplatesB1.push_back(h1phi);
    sprintf(hname, "hGenMhtPhiB2(ght%s)", htrangename);
    TH1F* h2phi = new TH1F(hname, hname, nBinMhtPhi, MhtPhiBinArray); h2phi->Sumw2();
    hMhtPhiTemplatesB2.push_back(h2phi);
    sprintf(hname, "hGenMhtPhiB3(ght%s)", htrangename);
    TH1F* h3phi = new TH1F(hname, hname, nBinMhtPhi, MhtPhiBinArray); h3phi->Sumw2();
    hMhtPhiTemplatesB3.push_back(h3phi);
  }
}

// ------------ method called once each job just after ending the event loop  ------------
void MCInputs::endJob() {

  hfile->cd();
  //hfile->cd(_dirName.c_str());
  // Save all objects in this file
  hHt->Write();
  hHtWeighted->Write();
  for (unsigned int i_eta = 1; i_eta < EtaBinEdges.size() ; ++i_eta) {
    for (unsigned int i_en = 1; i_en < EnBinEdges.size() ; ++i_en) {
      // with btag
      std::cout << "writing " <<  vvHeavyJetResponse[i_eta][i_en]->GetName() << "to file" << std::endl;
      hfile->WriteTObject(vvHeavyJetResponse[i_eta][i_en]);
      // without btag
      hfile->WriteTObject(vvLightJetResponse[i_eta][i_en]);
    }
  }
   
  hfile->cd();

  //for more elegant future, can use writing convention
  //hfile->WriteObject(&MhtPtTemplatesB0, "MhtPtTemplatesB0");

  for (unsigned int i=1; i< hMhtPtTemplatesB0.size(); i++)
    {
      hMhtPtTemplatesB0[i]->Write();
      hMhtPtTemplatesB1[i]->Write();
      hMhtPtTemplatesB2[i]->Write();
      hMhtPtTemplatesB3[i]->Write();
      hMhtPhiTemplatesB0[i]->Write();
      hMhtPhiTemplatesB1[i]->Write();
      hMhtPhiTemplatesB2[i]->Write();
      hMhtPhiTemplatesB3[i]->Write();
    }

  hEtaTemplate->Write();
  hEnTemplate->Write();
  hHtTemplate->Write();
  hfile->ls();

   
  // Close the file.
  hfile->Close();

}


//define this as a plug-in
DEFINE_FWK_MODULE( MCInputs);
