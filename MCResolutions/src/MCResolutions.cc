// -*- C++ -*-
//
// Package:    MCResolutions
// Class:      MCResolutions
//
/**\class MCResolutions MCResolutions.cc JetResolutionFromMC/MCResolutions/src/MCResolutions.cc
 
 Description: [one line class summary]
 
 Implementation:
 [Notes on implementation]
 */
//
// Original Author:  Christian Sander,,,
//         Created:  Wed Oct  6 18:22:21 CEST 2010
// $Id: MCResolutions.cc,v 1.1 2012/08/01 13:31:08 kheine Exp $
//
//
#include "rs/MCResolutions/interface/MCResolutions.h"
#include <iostream>

using namespace std;

//
// constructors and destructor
//
MCResolutions::MCResolutions(const edm::ParameterSet& iConfig) {
   //now do what ever initialization is needed
   _leptonTag = iConfig.getParameter<edm::InputTag> ("leptonTag");
   _jetTag = iConfig.getParameter<edm::InputTag> ("jetTag");
   _btagTag = iConfig.getParameter<std::string> ("btagTag");
   _btagCut = iConfig.getParameter<double> ("btagCut");
   _genJetTag = iConfig.getParameter<edm::InputTag> ("genJetTag");
   _weightName = iConfig.getParameter<edm::InputTag> ("weightName");
   _deltaRMatch = iConfig.getParameter<double> ("deltaRMatch");
   _deltaRMatchVeto = iConfig.getParameter<double> ("deltaRMatchVeto");
   _absPtVeto = iConfig.getParameter<double> ("absPtVeto");
   _relPtVeto = iConfig.getParameter<double> ("relPtVeto");
   _GenJetPtCut = iConfig.getParameter<double> ("GenJetPtCut");
   _fileName = iConfig.getParameter<std::string> ("fileName");
   _genBinning = iConfig.getParameter<bool> ("genBinning");
   _neutrinosInBinning = iConfig.getParameter<bool> ("neutrinosInBinning");
   _useFilters = iConfig.getParameter<bool> ("useFilters");
   _energyBinning = iConfig.getParameter<bool> ("energyBinning");

//   _genParticlesTag = iConfig.getParameter<edm::InputTag> ("packedGenParticles");
   _genParticlesTag = iConfig.getParameter<edm::InputTag>("packedGenParticles");


	NoMuonFilter_              = iConfig.getParameter<edm::InputTag> ("NoMuon");
	HBHENoiseFilter_           = iConfig.getParameter<edm::InputTag> ("HBHENoise");
	HBHEIsoNoiseFilter_        = iConfig.getParameter<edm::InputTag> ("HBHEIsoNoise");
	eeBadScFilter_             = iConfig.getParameter<edm::InputTag> ("eeBadSc");
	ecalDeadCellFilter_        = iConfig.getParameter<edm::InputTag> ("ecalDeadCell");
	globalTightHaloFilter_     = iConfig.getParameter<edm::InputTag> ("globalTightHalo");
	badChargedCandidateFilter_ = iConfig.getParameter<edm::InputTag> ("badChargedCandidate");
	badMuonFilter_             = iConfig.getParameter<edm::InputTag> ("badMuon");
	jetIDFilter_               = iConfig.getParameter<edm::InputTag> ("jetID");

	PFCaloRatio_ = iConfig.getParameter<edm::InputTag> ("PFCaloRatio");
	NVtxFilter_ = iConfig.getParameter<edm::InputTag> ("NVtx");
   
	NoMuonToken_ = consumes<int>(NoMuonFilter_);
	HBHENoiseToken_ = consumes<int>(HBHENoiseFilter_);
	HBHEIsoNoiseToken_ = consumes<int>(HBHEIsoNoiseFilter_);
	eeBadScToken_ = consumes<int>(eeBadScFilter_);
	ecalDeadCellToken_ = consumes<int>(ecalDeadCellFilter_);
	globalTightHaloToken_ = consumes<int>(globalTightHaloFilter_);
	badChargedCandidateToken_ = consumes<bool>(badChargedCandidateFilter_);
	badMuonToken_ = consumes<bool>(badMuonFilter_);
	jetIDToken_ = consumes<bool>(jetIDFilter_);

	PFCaloRatioToken_ = consumes<double>(PFCaloRatio_);
	NVtxToken_ = consumes<int>(NVtxFilter_);


   nleptons_ = consumes<int>(_leptonTag);
   weight_ = consumes<double>(_weightName);
   genJet_ = consumes<edm::View<reco::GenJet> >(_genJetTag); 
   jet_ = consumes<edm::View<pat::Jet> >(_jetTag); 
	genParticles_ = consumes<edm::View<pat::PackedGenParticle> >(_genParticlesTag); 


   hfile = new TFile(_fileName.c_str(), "RECREATE", "Jet response in pT and eta bins");
   //hfile->mkdir(_dirName.c_str(), _dirName.c_str());
   
	//Binning we used before
	/*
   PtBinEdges.push_back(0);
   PtBinEdges.push_back(20);
   PtBinEdges.push_back(30);
   PtBinEdges.push_back(50);
   PtBinEdges.push_back(80);
   PtBinEdges.push_back(120);
   PtBinEdges.push_back(170);
   PtBinEdges.push_back(230);
   PtBinEdges.push_back(300);
   PtBinEdges.push_back(380);
   PtBinEdges.push_back(470);
   PtBinEdges.push_back(570);
   PtBinEdges.push_back(680);
   PtBinEdges.push_back(800);
   PtBinEdges.push_back(1000);
   PtBinEdges.push_back(1300);
   PtBinEdges.push_back(1700);
   PtBinEdges.push_back(2200);
   PtBinEdges.push_back(2800);
   PtBinEdges.push_back(3500);
   PtBinEdges.push_back(4300);
   PtBinEdges.push_back(5200);
   PtBinEdges.push_back(6500);
   */
	double PtBinArray[] = {0,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,44,48,52,56,60,66,72,78,84,90,100,110,120,130,140,150,160,170,180,190,200,220,240,260,280,300,330,360,390,420,450,500,550,600,650,700,750,800,900,1000,1100,1200,1300,1400,1500,1600,1800,2000,2200,2500,3000,3500,4000,5000,6000}; //     Sam's amazing binning
	for( unsigned int i=0; i<(sizeof(PtBinArray)/sizeof(PtBinArray[0])); i++ ){
   	PtBinEdges.push_back(PtBinArray[i]);
		std::cout << "pt bin edge = " << PtBinArray[i] << std::endl;
	}

   EtaBinEdges.push_back(0.0);
   EtaBinEdges.push_back(0.3);
   EtaBinEdges.push_back(0.5);
   EtaBinEdges.push_back(0.8);
   EtaBinEdges.push_back(1.1);
   EtaBinEdges.push_back(1.4);
   EtaBinEdges.push_back(1.7);
   EtaBinEdges.push_back(2.0);
   EtaBinEdges.push_back(2.3);
   EtaBinEdges.push_back(2.8);
   EtaBinEdges.push_back(3.2);
   EtaBinEdges.push_back(4.1);
   EtaBinEdges.push_back(5.0);
   
   
   /*
    EtaBinEdges.push_back(0.0);
    EtaBinEdges.push_back(0.5);
    EtaBinEdges.push_back(1.1);
    EtaBinEdges.push_back(1.7);
    EtaBinEdges.push_back(2.3);
    EtaBinEdges.push_back(3.2);
    EtaBinEdges.push_back(5.0);
    */
   
   //// Array of histograms for jet resolutions (all jet multiplicities)
   ResizeHistoVector(h_tot_JetAll_JetResPt_Pt);
   ResizeHistoVector(h_b_JetAll_JetResPt_Pt);
   ResizeHistoVector(h_nob_JetAll_JetResPt_Pt);

   ResizeHistoVector(h_tot_JetAll_JetResPhi_Pt);
   ResizeHistoVector(h_b_JetAll_JetResPhi_Pt);
   ResizeHistoVector(h_nob_JetAll_JetResPhi_Pt);
   
   ResizeHistoVector(h_tot_JetAll_JetResEta_Pt);
   ResizeHistoVector(h_b_JetAll_JetResEta_Pt);
   ResizeHistoVector(h_nob_JetAll_JetResEta_Pt);

   // Btag efficiencies
   h_trueb_RecoPt.resize(EtaBinEdges.size() - 1);
   h_no_trueb_RecoPt.resize(EtaBinEdges.size() - 1);
   h_trueb_btag_RecoPt.resize(EtaBinEdges.size() - 1);
   h_no_trueb_btag_RecoPt.resize(EtaBinEdges.size() - 1);
   h_no_trueb_no_btag_RecoPt.resize(EtaBinEdges.size() - 1);
   h_trueb_no_btag_RecoPt.resize(EtaBinEdges.size() - 1);
   h_btag_RecoPt.resize(EtaBinEdges.size() - 1);
   h_no_btag_RecoPt.resize(EtaBinEdges.size() - 1);
   
}

MCResolutions::~MCResolutions() {
   
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)
   
}

//
// member functions
//

// ------------ method called to for each event  ------------
void MCResolutions::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {
   using namespace std;
   
	edm::Handle<int> HBHENoiseHandle_;
	edm::Handle<int> HBHEIsoNoiseHandle_;
	edm::Handle<int> eeBadScHandle_;
	edm::Handle<int> ecalDeadCellHandle_;
	edm::Handle<int> globalTightHaloHandle_;
	edm::Handle<int> NVtxHandle_;
	edm::Handle<int> NoMuonHandle_;
	edm::Handle<bool> badChargedCandidateHandle_;
	edm::Handle<bool> badMuonHandle_;
	edm::Handle<bool> jetIDHandle_;
	
	iEvent.getByToken(NoMuonToken_,NoMuonHandle_);
	iEvent.getByToken(HBHENoiseToken_,HBHENoiseHandle_);
	iEvent.getByToken(HBHEIsoNoiseToken_,HBHEIsoNoiseHandle_);
	iEvent.getByToken(eeBadScToken_,eeBadScHandle_);
	iEvent.getByToken(ecalDeadCellToken_,ecalDeadCellHandle_);
	iEvent.getByToken(globalTightHaloToken_,globalTightHaloHandle_);
//	iEvent.getByToken(badChargedCandidateToken_,badChargedCandidateHandle_);
//	iEvent.getByToken(badMuonToken_,badMuonHandle_);
	iEvent.getByToken(jetIDToken_,jetIDHandle_);
	iEvent.getByToken(NVtxToken_,NVtxHandle_);











   //LeptonVeto
   edm::Handle<int> NLeptons;
	iEvent.getByToken(nleptons_, NLeptons);

   if ( _useFilters &&
	((*NLeptons) != 0 ||
	 (*NoMuonHandle_) == 0 ||
	 (*HBHENoiseHandle_) == 0 ||
	 (*HBHEIsoNoiseHandle_) == 0 ||
	 (*eeBadScHandle_) == 0 ||
	 (*ecalDeadCellHandle_) == 0 ||
	 !(*jetIDHandle_) ||
	 (*NVtxHandle_) == 0) 	){
//		cout << "reject event!" << endl;
//		std::cout << "                                                NoMuonJet filetr value " << (*NoMuonHandle_) << std::endl;
//		std::cout << "                                                HBHENoise filetr value " << (*HBHENoiseHandle_) << std::endl;
//		std::cout << "                                                HBHEIsoNoise filetr value " << (*HBHEIsoNoiseHandle_) << std::endl;
//		std::cout << "                                                eeBadSc filetr value " << (*eeBadScHandle_) << std::endl;
//		std::cout << "                                                ecalDeadCell filetr value " << (*ecalDeadCellHandle_) << std::endl;
//		std::cout << "                                                globalTightHalo filetr value " << (*globalTightHaloHandle_) << std::endl;
//		std::cout << "                                                jetID filetr value " << ( (*jetIDHandle_) ? 1 : 0 )  << std::endl;
//		std::cout << "                                                NVtx value " << (*NVtxHandle_) << std::endl;
//		std::cout << "                                                # of leptons " << (*NLeptons) << std::endl;
////	std::cout << "                                                badChargedCandidate filetr value " << ( (*badChargedCandidateHandle_) ? 1 : 0 ) << std::endl;
////	std::cout << "                                                badMuon filetr value " << ( (*badMuonHandle_) ? 1 : 0 )  << std::endl;
//		std::cout << std::endl;
      return;
	}
   
   //Weight
   edm::Handle<double> event_weight;
   bool findWeight = iEvent.getByToken(weight_, event_weight);
   weight = (event_weight.isValid() ? (*event_weight) : 1.0);
   if (!findWeight) {
      cout << "Weight not found!" << endl;
   }
   
   //GenJets
   edm::Handle<edm::View<reco::GenJet> > Jets_gen;
   iEvent.getByToken(genJet_, Jets_gen);
   
   //RecoJets
   edm::Handle<edm::View<pat::Jet> > Jets_rec;
   iEvent.getByToken(jet_, Jets_rec);
   
   edm::Handle<edm::View<pat::PackedGenParticle> > genParticles;
//   iEvent.getByToken(genParticles_, genParticles);
   iEvent.getByLabel("packedGenParticles", genParticles);
   for (edm::View<reco::GenJet>::const_iterator it = Jets_gen->begin(); it != Jets_gen->end(); ++it) {
      
      if (it->pt() < _GenJetPtCut) continue;
      
      //// First look if there is no significant GenJet near the tested GenJet
      double dRgenjet = 999.;
      double PtdRmin = 0;
      for (edm::View<reco::GenJet>::const_iterator kt = Jets_gen-> begin(); kt != Jets_gen->end(); ++kt) {
         if (&(*it) == &(*kt))
            continue;
         double dR = deltaR(*it, *kt);
         if (dR < dRgenjet) {
            dRgenjet = dR;
            PtdRmin = kt->pt();
         }
      }
      //cout << "Gen pT: " << it->pt() << " closest genJet pT: " << PtdRmin << " dR: " << dRgenjet << endl;
      if (dRgenjet < _deltaRMatchVeto && (PtdRmin / it->pt()) > _relPtVeto)
         continue;
      
      const pat::Jet* matchedJet = 0;
      const pat::Jet* nearestJet = 0;
      math::PtEtaPhiMLorentzVector allJetsInCone(0., 0., 0., 0.);
      double dRmatched = 999.;
      double dRnearest = 999.;

		//	here you do the matching

      for (edm::View<pat::Jet>::const_iterator jt = Jets_rec-> begin(); jt != Jets_rec->end(); ++jt) {
         //cout << "RECO: " << jt->pt() << ", " << jt->eta() << endl;
			//	it -> GenJets, jt -> RecoJets.
         double dR = deltaR(*it, *jt);
         if (dR < 0.35) {
            allJetsInCone+=jt->p4();
         }
         if (dR < dRmatched) {
            nearestJet = matchedJet;
            dRnearest = dRmatched;
            matchedJet = &(*jt);
            dRmatched = dR;
         } else if (dR < dRnearest) {
            nearestJet = &(*jt);
            dRnearest = dR;
         }
      }
      
      //// look if there is no further significant CaloJet near the genJet
      if (dRmatched < _deltaRMatch && (nearestJet == 0 || dRnearest > _deltaRMatchVeto || (nearestJet->pt() < _absPtVeto && nearestJet->pt() / matchedJet->pt() < _relPtVeto))) {
      //if (dRmatched < _deltaRMatch) {
         //// Find additional neutrinos and add them back since these are not included in the genJets
         math::PtEtaPhiMLorentzVector neutrinos(0., 0., 0., 0.);
			if (_neutrinosInBinning){
            for(edm::View<pat::PackedGenParticle>::const_iterator cand = genParticles->begin(); cand!=genParticles->end(); ++cand)
            {
               if ( cand->status()==1 && (abs(cand->pdgId())==12 || abs(cand->pdgId())==14 || abs(cand->pdgId())==16)){
                  double dR = deltaR(*it, *cand);
                  if (dR < 0.4) neutrinos += cand->p4();
               }
            }
//      		cout << "neutrions in binning!" << endl;
		   }

         
         double res = matchedJet->pt() / (it->p4()+neutrinos).pt();
         double resPhi = matchedJet->phi() - it->phi();
         double resEta = matchedJet->eta() - it->eta();
         //double res = allJetsInCone.pt() / it->pt 


// ==============================================================================================================
    // Note: Currently, templates are used for b-jets and non-b-jets, respectively;
    // these all-inclusive templates are NOT used!

    // For GenSmearing:
    // Store resolution binned in genparticle pt and eta, including neutrinopt:
         // h_tot_JetAll_JetResPt_Pt.at(EtaBin(it->eta())).at(PtBin((it->p4()+neutrinos).pt()))->Fill(res, weight);
         // h_tot_JetAll_JetResPhi_Pt.at(EtaBin(it->eta())).at(PtBin(it->pt()))->Fill(resPhi, weight);
         // h_tot_JetAll_JetResEta_Pt.at(EtaBin(it->eta())).at(PtBin(it->pt()))->Fill(resEta, weight);


    // For R+S:
    // Store resolution binned in recoparticle pt and eta:
			if (_energyBinning){
//      		cout << "using energy binning!" << endl;
				h_tot_JetAll_JetResPt_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(res, weight);
         	h_tot_JetAll_JetResPhi_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(resPhi, weight);
         	h_tot_JetAll_JetResEta_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(resEta, weight);
			}
			else{
//      		cout << "using P_T binning!" << endl;
				h_tot_JetAll_JetResPt_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(res, weight);
         	h_tot_JetAll_JetResPhi_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(resPhi, weight);
         	h_tot_JetAll_JetResEta_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(resEta, weight);
			}
// ==============================================================================================================


         // "Determine btag/btrue information"
         
         //// Use algorithmic matching for heavy flavour ID
         bool bTrue = false;
         //if (fabs(matchedJet->partonFlavour()) == 4 || fabs(matchedJet->partonFlavour()) == 5) {
         if (fabs(matchedJet->partonFlavour()) == 5) {
            bTrue = true;
         }

         //// Use b-tag for heavy flavour ID
         bool bTag = false;
         // "Store btag/btrue information"
         if ( matchedJet->bDiscriminator(_btagTag) > _btagCut) {
            bTag = true;
            // "btag"
            h_btag_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
         } else {
            // "no btag"
            h_no_btag_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
         }

         if(bTrue){
             // "true b"
             h_trueb_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
             if(bTag){
                // "true b and btag"
                h_trueb_btag_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
             } else {
                // "true b and no btag"
                h_trueb_no_btag_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
             }
         } else {
             // "no true b"
             h_no_trueb_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
             if(bTag){
                // "no true b and btag"
                h_no_trueb_btag_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
             } else {
                // "no true b and no btag"
                h_no_trueb_no_btag_RecoPt.at(EtaBin(it->eta()))->Fill(it->pt(), weight);
             }
         }

    // For GenSmearing:
    // Store resolution binned in genparticle pt and eta:
         // Use templates based on whether it is a true b or not (not based on btags):
			if ( _genBinning ) {
				if (bTrue) {
					if (_energyBinning){
      				cout << "using energy binning!" << endl;
						h_b_JetAll_JetResPt_Pt.at(EtaBin(it->eta())).at(PtBin((it->p4()+neutrinos).energy()))->Fill(res, weight);
						h_b_JetAll_JetResPhi_Pt.at(EtaBin(it->eta())).at(PtBin(it->energy()))->Fill(resPhi, weight);
						h_b_JetAll_JetResEta_Pt.at(EtaBin(it->eta())).at(PtBin(it->energy()))->Fill(resEta, weight);
					}
					else{
      				cout << "using P_T binning!" << endl;
						h_b_JetAll_JetResPt_Pt.at(EtaBin(it->eta())).at(PtBin((it->p4()+neutrinos).pt()))->Fill(res, weight);
						h_b_JetAll_JetResPhi_Pt.at(EtaBin(it->eta())).at(PtBin(it->pt()))->Fill(resPhi, weight);
						h_b_JetAll_JetResEta_Pt.at(EtaBin(it->eta())).at(PtBin(it->pt()))->Fill(resEta, weight);
					}
				} else {
					if (_energyBinning){
		      		cout << "using energy binning!" << endl;
						h_nob_JetAll_JetResPt_Pt.at(EtaBin(it->eta())).at(PtBin((it->p4()+neutrinos).energy()))->Fill(res, weight);
						h_nob_JetAll_JetResPhi_Pt.at(EtaBin(it->eta())).at(PtBin(it->energy()))->Fill(resPhi, weight);
						h_nob_JetAll_JetResEta_Pt.at(EtaBin(it->eta())).at(PtBin(it->energy()))->Fill(resEta, weight);
					}
					else{
      				cout << "using P_T binning!" << endl;
						h_nob_JetAll_JetResPt_Pt.at(EtaBin(it->eta())).at(PtBin((it->p4()+neutrinos).pt()))->Fill(res, weight);
						h_nob_JetAll_JetResPhi_Pt.at(EtaBin(it->eta())).at(PtBin(it->pt()))->Fill(resPhi, weight);
						h_nob_JetAll_JetResEta_Pt.at(EtaBin(it->eta())).at(PtBin(it->pt()))->Fill(resEta, weight);
					}
				}
  //    		cout << "gen binning!" << endl;
			} else {
    // For R+S:
    // Store resolution binned in recoparticle pt and eta:
// Use templates based on whether it is a true b or not (not based on btags):
				if (bTrue) {
					if (_energyBinning){
//   	 		  		cout << "using energy binning!" << endl;
						h_b_JetAll_JetResPt_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(res, weight);
						h_b_JetAll_JetResPhi_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(resPhi, weight);
						h_b_JetAll_JetResEta_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(resEta, weight);
					}
					else{
//      				cout << "using P_T binning!" << endl;
						h_b_JetAll_JetResPt_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(res, weight);
						h_b_JetAll_JetResPhi_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(resPhi, weight);
						h_b_JetAll_JetResEta_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(it->pt()))->Fill(resEta, weight); // why genjet here?
					}
				} else {
					if (_energyBinning){
//			    		cout << "using energy binning!" << endl;
						h_nob_JetAll_JetResPt_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(res, weight);
						h_nob_JetAll_JetResPhi_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(resPhi, weight);
						h_nob_JetAll_JetResEta_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->energy()))->Fill(resEta, weight);
					}
					else{
//      				cout << "using P_T binning!" << endl;
						h_nob_JetAll_JetResPt_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(res, weight);
						h_nob_JetAll_JetResPhi_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(resPhi, weight);
						h_nob_JetAll_JetResEta_Pt.at(EtaBin(matchedJet->eta())).at(PtBin(matchedJet->pt()))->Fill(resEta, weight);
					}
				}
//				cout << "reco binning!" << endl;
			}
      }
   }
}

// ------------ method called once each job just before starting event loop  ------------
void MCResolutions::beginJob() {
   
   
   for (unsigned int i_eta = 0; i_eta < EtaBinEdges.size() - 1; ++i_eta) {
      char hname[100];
      cout << "Book histograms" << endl;
      // Book histograms b-tag efficiencies
      sprintf(hname, "h_trueb_RecoPt_Eta%i", i_eta);
      h_trueb_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_trueb_RecoPt.at(i_eta)->Sumw2();
      sprintf(hname, "h_no_trueb_RecoPt_Eta%i", i_eta);
      h_no_trueb_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_no_trueb_RecoPt.at(i_eta)->Sumw2();
      sprintf(hname, "h_trueb_btag_RecoPt_Eta%i", i_eta);
      h_trueb_btag_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_trueb_btag_RecoPt.at(i_eta)->Sumw2();
      sprintf(hname, "h_trueb_no_btag_RecoPt_Eta%i", i_eta);
      h_trueb_no_btag_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_trueb_no_btag_RecoPt.at(i_eta)->Sumw2();
      sprintf(hname, "h_no_trueb_no_btag_RecoPt_Eta%i", i_eta);
      h_no_trueb_no_btag_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_no_trueb_no_btag_RecoPt.at(i_eta)->Sumw2();
      sprintf(hname, "h_no_trueb_btag_RecoPt_Eta%i", i_eta);
      h_no_trueb_btag_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_no_trueb_btag_RecoPt.at(i_eta)->Sumw2();
      sprintf(hname, "h_btag_RecoPt_Eta%i", i_eta);
      h_btag_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_btag_RecoPt.at(i_eta)->Sumw2();
      sprintf(hname, "h_no_btag_RecoPt_Eta%i", i_eta);
      h_no_btag_RecoPt.at(i_eta) = new TH1F(hname, hname, 200, 0., 1000.);
      h_no_btag_RecoPt.at(i_eta)->Sumw2();

      for (unsigned int i_pt = 0; i_pt < PtBinEdges.size() - 1; ++i_pt) {
         //// Book histograms (all jet multiplicities)
         sprintf(hname, "h_tot_JetAll_ResponsePt_Pt%i_Eta%i", i_pt, i_eta);
         h_tot_JetAll_JetResPt_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 150, 0., 3.);
         h_tot_JetAll_JetResPt_Pt.at(i_eta).at(i_pt)->Sumw2();
         sprintf(hname, "h_b_JetAll_ResponsePt_Pt%i_Eta%i", i_pt, i_eta);
         h_b_JetAll_JetResPt_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 150, 0., 3.);
         h_b_JetAll_JetResPt_Pt.at(i_eta).at(i_pt)->Sumw2();
         sprintf(hname, "h_nob_JetAll_ResponsePt_Pt%i_Eta%i", i_pt, i_eta);
         h_nob_JetAll_JetResPt_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 150, 0., 3.);
         h_nob_JetAll_JetResPt_Pt.at(i_eta).at(i_pt)->Sumw2();

         //// Book histograms Phi resolution (all jet multiplicities)
         sprintf(hname, "h_tot_JetAll_ResponsePhi_Pt%i_Eta%i", i_pt, i_eta);
         h_tot_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 60, -0.3, 0.3);
         h_tot_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt)->Sumw2();
         sprintf(hname, "h_b_JetAll_ResponsePhi_Pt%i_Eta%i", i_pt, i_eta);
         h_b_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 60, -0.3, 0.3);
         h_b_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt)->Sumw2();
         sprintf(hname, "h_nob_JetAll_ResponsePhi_Pt%i_Eta%i", i_pt, i_eta);
         h_nob_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 60, -0.3, 0.3);
         h_nob_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt)->Sumw2();

         //// Book histograms Eta resolution (all jet multiplicities)
         sprintf(hname, "h_tot_JetAll_ResponseEta_Pt%i_Eta%i", i_pt, i_eta);
         h_tot_JetAll_JetResEta_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 60, -0.3, 0.3);
         h_tot_JetAll_JetResEta_Pt.at(i_eta).at(i_pt)->Sumw2();
         sprintf(hname, "h_b_JetAll_ResponseEta_Pt%i_Eta%i", i_pt, i_eta);
         h_b_JetAll_JetResEta_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 60, -0.3, 0.3);
         h_b_JetAll_JetResEta_Pt.at(i_eta).at(i_pt)->Sumw2();
         sprintf(hname, "h_nob_JetAll_ResponseEta_Pt%i_Eta%i", i_pt, i_eta);
         h_nob_JetAll_JetResEta_Pt.at(i_eta).at(i_pt) = new TH1F(hname, hname, 60, -0.3, 0.3);
         h_nob_JetAll_JetResEta_Pt.at(i_eta).at(i_pt)->Sumw2();
      }
   }
}

// ------------ method called once each job just after ending the event loop  ------------
void MCResolutions::endJob() {
   
	hfile->cd();
	//hfile->cd(_dirName.c_str());
	// Save all objects in this file
	for (unsigned int i_eta = 0; i_eta < EtaBinEdges.size() - 1; ++i_eta) {
		// btag efficiencies
		cout << "Write histograms" << endl;
		hfile->WriteTObject(h_trueb_RecoPt.at(i_eta));
		hfile->WriteTObject(h_no_trueb_RecoPt.at(i_eta));
		hfile->WriteTObject(h_no_trueb_btag_RecoPt.at(i_eta));
		hfile->WriteTObject(h_no_trueb_no_btag_RecoPt.at(i_eta));
		hfile->WriteTObject(h_trueb_no_btag_RecoPt.at(i_eta));
		hfile->WriteTObject(h_trueb_btag_RecoPt.at(i_eta));
		hfile->WriteTObject(h_btag_RecoPt.at(i_eta));
		hfile->WriteTObject(h_no_btag_RecoPt.at(i_eta));
		
		for (unsigned int i_pt = 0; i_pt < PtBinEdges.size() - 1; ++i_pt) {
			// total
			hfile->WriteTObject(h_tot_JetAll_JetResPt_Pt.at(i_eta).at(i_pt));
			hfile->WriteTObject(h_tot_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt));
			hfile->WriteTObject(h_tot_JetAll_JetResEta_Pt.at(i_eta).at(i_pt));
			// with btag
			hfile->WriteTObject(h_b_JetAll_JetResPt_Pt.at(i_eta).at(i_pt));
			hfile->WriteTObject(h_b_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt));
			hfile->WriteTObject(h_b_JetAll_JetResEta_Pt.at(i_eta).at(i_pt));
			// without btag
			hfile->WriteTObject(h_nob_JetAll_JetResPt_Pt.at(i_eta).at(i_pt));
			hfile->WriteTObject(h_nob_JetAll_JetResPhi_Pt.at(i_eta).at(i_pt));
			hfile->WriteTObject(h_nob_JetAll_JetResEta_Pt.at(i_eta).at(i_pt));
		}
	}
   
   hfile->cd();
   hfile->WriteObject(&PtBinEdges, "PtBinEdges");
   hfile->WriteObject(&EtaBinEdges, "EtaBinEdges");
   //hfile->ls();
   
   // Close the file.
   hfile->Close();
   
}

int MCResolutions::PtBin(const double& pt) {
   int i_pt = -1;
   for (std::vector<double>::const_iterator it = PtBinEdges.begin(); it != PtBinEdges.end(); ++it) {
      if ((*it) > pt)
         break;
      ++i_pt;
   }
   if (i_pt < 0)
      i_pt = 0;
   if (i_pt > (int) PtBinEdges.size() - 2)
      i_pt = (int) PtBinEdges.size() - 2;
   
   return i_pt;
}

int MCResolutions::EtaBin(const double& eta) {
   int i_eta = -1;
   for (std::vector<double>::const_iterator it = EtaBinEdges.begin(); it != EtaBinEdges.end(); ++it) {
      if ((*it) > fabs(eta))
         break;
      ++i_eta;
   }
   if (i_eta < 0)
      i_eta = 0;
   if (i_eta > (int) EtaBinEdges.size() - 2)
      i_eta = (int) EtaBinEdges.size() - 2;
   return i_eta;
}


void MCResolutions::ResizeHistoVector(std::vector<std::vector<TH1F*> > &histoVector) {
   
   histoVector.resize(EtaBinEdges.size() - 1);
   for (std::vector<std::vector<TH1F*> >::iterator it = histoVector.begin(); it != histoVector.end(); ++it) {
      it->resize(PtBinEdges.size() - 1);
   }
}

//define this as a plug-in
DEFINE_FWK_MODULE( MCResolutions);
