# Expects a file name as argument e.g.
# cmsRun mcresolutions_cfg.py data_set=/QCD_HT-500To1000_13TeV-madgraph/Phys14DR-PU20bx25_PHYS14_25_V1-v1/MINIAODSIM, globaltag=PHYS14_25_V1

## --- Read parameters --------------------------------------------------
from TreeMaker.Utils.CommandLineParams import CommandLineParams
parameters = CommandLineParams()

runOnMC = parameters.value("is_mc",True)
dataSetName = parameters.value("dataset_name","/QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/RunIISpring16MiniAODv1-PUSpring16_80X_mcRun2_asymptotic_2016_v3-v1/MINIAODSIM")
testFileName = parameters.value("test_filename","/store/mc/RunIISpring16MiniAODv1/QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/MINIAODSIM/PUSpring16_80X_mcRun2_asymptotic_2016_v3-v1/00000/00281DF0-8EFE-E511-8C0F-00259073E3B6.root")
globalTag = parameters.value("global_tag","80X_mcRun2_asymptotic_2016_v3")
tagName = parameters.value("tag_name","PAT")
jecFile = parameters.value("jec_file","Spring16_25nsV6_MC")
lumi = parameters.value("lumi",10000.0)
doPUReweighting = parameters.value("pu_reweighting",False)
mcResoJetTag = parameters.value("jet_tag","GoodJets")
mcResoPackedGenTag = parameters.value("packed_tag","packedGenParticles")
mcResoLeptonTag = parameters.value("lepton_tag","GoodLeptons")
mcResoFileName = parameters.value("out_name","MCResoDefault.root")
mtcut = parameters.value("mTcut",100.0)
mcResoGenBinning = parameters.value("GenBinning",False)
mcResoNeutrinosInBinning = parameters.value("AddNeutrinos",False)
flagUseFilters = parameters.value("UseFilters",False)
flagEnergyBinning = parameters.value("EnergyBinning",True)

print "*** JOB SETUP ****************************************************"
print "  is_mc          : "+str(runOnMC)
print "  data_set       : "+dataSetName
print "  global_tag     : "+globalTag
print "  jecFile        : "+jecFile
print "  lumi           : "+str(lumi)
print "  pu_reweighting : "+str(doPUReweighting)
print "  jet_tag        : "+mcResoJetTag
print "  lepton_tag     : "+mcResoLeptonTag
print "  out_name       : "+mcResoFileName
print "  energy binning : "+str(flagEnergyBinning)
print "  neutrinos incl : "+str(mcResoNeutrinosInBinning)
print "  gen binning    : "+str(mcResoGenBinning)
print "******************************************************************"

## --- The process needs to be defined AFTER reading sys.argv, ---------
## --- otherwise edmConfigHash fails -----------------------------------
import FWCore.ParameterSet.Config as cms
import sys,os
process = cms.Process("ResponseTemplates")

## configure geometry & conditions
process.load("Configuration.StandardSequences.GeometryRecoDB_cff")
process.load("Configuration.StandardSequences.MagneticField_AutoFromDBCurrent_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.GlobalTag.globaltag = globalTag

## --- Log output ------------------------------------------------------
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr = cms.untracked.PSet(placeholder = cms.untracked.bool(True))
process.MessageLogger.cout = cms.untracked.PSet(INFO = cms.untracked.PSet(reportEvery = cms.untracked.int32(1000)))
process.options = cms.untracked.PSet(wantSummary = cms.untracked.bool(True))

## --- Input Source ----------------------------------------------------
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(-1))
process.source = cms.Source("PoolSource",fileNames = cms.untracked.vstring(testFileName))
process.source.duplicateCheckMode = cms.untracked.string('noDuplicateCheck')

## --- Setup WeightProducer --------------------------------------------
from TreeMaker.WeightProducer.getWeightProducer_cff import getWeightProducer
process.WeightProducer = getWeightProducer(dataSetName)
process.WeightProducer.LumiScale = cms.double(lumi)
process.WeightProducer.PU = cms.int32(0) # PU: 3 for S10, 2 for S7
process.WeightProducer.FileNamePUDataDistribution = cms.string("NONE")

## --- isotrack producer -----------------------------------------------
from TreeMaker.Utils.trackIsolationMaker_cfi import trackIsolationFilter

process.IsolatedElectronTracksVeto = trackIsolationFilter.clone(
                                                                doTrkIsoVeto= True,
                                                                vertexInputTag = cms.InputTag("offlineSlimmedPrimaryVertices"),
                                                                pfCandidatesTag = cms.InputTag("packedPFCandidates"),
                                                                dR_ConeSize         = cms.double(0.3),
                                                                dz_CutValue         = cms.double(0.1),
                                                                minPt_PFCandidate   = cms.double(5.0),
                                                                isoCut              = cms.double(0.2),
                                                                pdgId               = cms.int32(11),
                                                                mTCut=mtcut,
                                                                )
   
process.IsolatedMuonTracksVeto = trackIsolationFilter.clone(
                                                            doTrkIsoVeto= True,
                                                            vertexInputTag = cms.InputTag("offlineSlimmedPrimaryVertices"),
                                                            pfCandidatesTag = cms.InputTag("packedPFCandidates"),
                                                            dR_ConeSize         = cms.double(0.3),
                                                            dz_CutValue         = cms.double(0.1),
                                                            minPt_PFCandidate   = cms.double(5.0),
                                                            isoCut              = cms.double(0.2),
                                                            pdgId               = cms.int32(13),
                                                            mTCut=mtcut,
                                                            )

process.IsolatedPionTracksVeto = trackIsolationFilter.clone(
                                                            doTrkIsoVeto= True,
                                                            vertexInputTag = cms.InputTag("offlineSlimmedPrimaryVertices"),
                                                            pfCandidatesTag = cms.InputTag("packedPFCandidates"),
                                                            dR_ConeSize         = cms.double(0.3),
                                                            dz_CutValue         = cms.double(0.1),
                                                            minPt_PFCandidate   = cms.double(10.0),
                                                            isoCut              = cms.double(0.1),
                                                            pdgId               = cms.int32(211),
                                                            mTCut=mtcut,
                                                            )


## --- good leptons producer -------------------------------------------
from TreeMaker.Utils.leptonproducer_cfi import leptonproducer
process.GoodLeptons = leptonproducer.clone(
                                           MuonTag          = cms.InputTag('slimmedMuons'),
                                           ElectronTag      = cms.InputTag('slimmedElectrons'),
                                           PrimaryVertex    = cms.InputTag('offlineSlimmedPrimaryVertices'),
                                           minElecPt        = cms.double(10),
                                           maxElecEta       = cms.double(2.5),
                                           minMuPt          = cms.double(10),
                                           maxMuEta         = cms.double(2.4),
                                           UseMiniIsolation = cms.bool(True),
                                           muIsoValue       = cms.double(0.2),
                                           elecIsoValue     = cms.double(0.1), # only has an effect when used with miniIsolation
                                           METTag           = cms.InputTag('slimmedMETs'),
                                           )

## --- good photon producer -----------------------------------------------
process.GoodPhotons = cms.EDProducer("PhotonIDisoProducer",
                                     photonCollection = cms.untracked.InputTag("slimmedPhotons"),
                                     electronCollection = cms.untracked.InputTag("slimmedElectrons"),
                                     conversionCollection = cms.untracked.InputTag("reducedEgamma","reducedConversions",tagName),
                                     beamspotCollection = cms.untracked.InputTag("offlineBeamSpot","","RECO"),
                                     ecalRecHitsInputTag_EE = cms.InputTag("reducedEgamma","reducedEERecHits"),
                                     ecalRecHitsInputTag_EB = cms.InputTag("reducedEgamma","reducedEBRecHits"),
                                     rhoCollection = cms.untracked.InputTag("fixedGridRhoFastjetAll"),
                                     genParCollection = cms.untracked.InputTag("genParCollection"),
                                     debug = cms.untracked.bool(False)
                                     )

## ----------------------------------------------------------------------------------------------
## JECs
## ----------------------------------------------------------------------------------------------

# get the JECs (disabled by default)
# this requires the user to download the .db file from this twiki
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECDataMC
if len(jecFile)>0:
#   JECPatch = cms.string('sqlite_file:'+jecFile+'.db')
#   if os.getenv('GC_CONF'):
#      JECPatch = cms.string('sqlite_file:/nfs/dust/cms/user/niedziem/RnS_input_files/'+jecFile+'.db')

   JECPatch = cms.string('sqlite_file:/nfs/dust/cms/user/niedziem/RnS_input_files/'+jecFile+'.db')

   process.load("CondCore.DBCommon.CondDBCommon_cfi")
   from CondCore.DBCommon.CondDBSetup_cfi import CondDBSetup
   process.jec = cms.ESSource("PoolDBESSource",CondDBSetup,
                              connect = JECPatch,
                              toGet   = cms.VPSet(
                                                  cms.PSet(
                                                           record = cms.string("JetCorrectionsRecord"),
                                                           tag    = cms.string("JetCorrectorParametersCollection_"+jecFile+"_AK4PFchs"),
                                                           label  = cms.untracked.string("AK4PFchs")
                                                           ),
                                                  cms.PSet(
                                                           record = cms.string("JetCorrectionsRecord"),
                                                           tag    = cms.string("JetCorrectorParametersCollection_"+jecFile+"_AK4PF"),
                                                           label  = cms.untracked.string("AK4PF")
                                                           )
                                                  )
                              )
   process.es_prefer_jec = cms.ESPrefer("PoolDBESSource","jec")
                           
   from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cff import updatedPatJetCorrFactors
   process.patJetCorrFactorsReapplyJEC = updatedPatJetCorrFactors.clone(
                                                                        src     = cms.InputTag("slimmedJets"),
                                                                        levels  = ['L1FastJet',
                                                                                   'L2Relative',
                                                                                   'L3Absolute'],
                                                                        payload = 'AK4PFchs' # Make sure to choose the appropriate levels and payload here!
                                                                        )
#   if residual: process.patJetCorrFactorsReapplyJEC.levels.append('L2L3Residual')
         
   from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cff import updatedPatJets
   process.patJetsReapplyJEC = updatedPatJets.clone(
                                                    jetSource = cms.InputTag("slimmedJets"),
                                                    jetCorrFactorsSource = cms.VInputTag(cms.InputTag("patJetCorrFactorsReapplyJEC"))
   )

## --- good jets producer -----------------------------------------------
from TreeMaker.Utils.goodjetsproducer_cfi import GoodJetsProducer
process.GoodJets = GoodJetsProducer.clone(
                                          TagMode = cms.bool(False),
                                          #JetTag= cms.InputTag('slimmedJets'),
                                          JetTag= cms.InputTag('patJetsReapplyJEC'),
                                          maxJetEta = cms.double(5.0),
                                          maxMuFraction = cms.double(2),
                                          minNConstituents = cms.double(2),
                                          maxNeutralFraction = cms.double(0.90),
                                          maxPhotonFraction = cms.double(0.95),
                                          minChargedMultiplicity = cms.double(0),
                                          minChargedFraction = cms.double(0),
                                          maxChargedEMFraction = cms.double(0.99),
                                          jetPtFilter = cms.double(30),
                                          ExcludeLepIsoTrackPhotons = cms.bool(True),
                                          JetConeSize = cms.double(0.4),
                                          MuonTag = cms.InputTag('GoodLeptons:IdIsoMuon'),
                                          ElecTag = cms.InputTag('GoodLeptons:IdIsoElectron'),
                                          IsoElectronTrackTag = cms.InputTag('IsolatedElectronTracksVeto'),
                                          IsoMuonTrackTag = cms.InputTag('IsolatedMuonTracksVeto'),
                                          IsoPionTrackTag = cms.InputTag('IsolatedPionTracksVeto'),
                                          PhotonTag = cms.InputTag('GoodPhotons','bestPhoton'),
                                          ### TEMPORARY ###
                                          #VetoHF = cms.bool(False),
                                          #VetoEta = cms.double(3.0)
                                          )


## --- Setup MC Truth templates writer ---------------------------------
from rs.MCResolutions.mcresolutions_cfi import MCResolutions
process.MCReso = MCResolutions.clone()
process.MCReso.jetTag = mcResoJetTag
process.MCReso.packedGenParticles = mcResoPackedGenTag
process.MCReso.fileName = mcResoFileName
process.MCReso.leptonTag = mcResoLeptonTag
process.MCReso.genBinning = mcResoGenBinning
process.MCReso.neutrinosInBinning = mcResoNeutrinosInBinning
process.MCReso.useFilters = flagUseFilters
process.MCReso.energyBinning = flagEnergyBinning


## --- MET Filters -----------------------------------------------
## We don't use "import *" because the cff contains some modules for which the C++ class doesn't exist
## and this triggers an error under unscheduled mode
#from TreeMaker.Utils.filterdecisionproducer_cfi import filterDecisionProducer
#process.EcalDeadCellTriggerPrimitiveFilter = filterDecisionProducer.clone(
#    trigTagArg1 = cms.string('TriggerResults'),
#    trigTagArg2 = cms.string(''),
#    trigTagArg3 = cms.string("RECO"),
#    filterName  = cms.string("Flag_EcalDeadCellTriggerPrimitiveFilter"),
#)
#process.EcalDeadCellTriggerPrimitiveFilter = filterDecisionProducer.clone(
#    trigTagArg1 = cms.string('TriggerResults'),
#    trigTagArg2 = cms.string(''),
#    trigTagArg3 = cms.string("RECO"),
#    filterName  = cms.string("Flag_EcalDeadCellTriggerPrimitiveFilter"),
#)

# baseline producers
process.Baseline = cms.Sequence(
)

print "*** HT jets producer **************************************************"
from TreeMaker.Utils.subJetSelection_cfi import SubJetSelection
process.HTJets = SubJetSelection.clone(
                                       JetTag   = cms.InputTag('GoodJets'),
                                       MinPt    = cms.double(30),
                                       MaxEta   = cms.double(2.4),
)
process.Baseline += process.HTJets

print "*** NVtx jets producer **************************************************"
from TreeMaker.Utils.primaryvertices_cfi import primaryvertices
process.NVtx = primaryvertices.clone(
                                      VertexCollection  = cms.InputTag('offlineSlimmedPrimaryVertices'),
                                      )
process.Baseline += process.NVtx
#VarsInt.extend(['NVtx'])

from TreeMaker.Utils.filterdecisionproducer_cfi import filterDecisionProducer

print "*** globalTightHalo2016Filter jets producer **************************************************"
process.globalTightHalo2016Filter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_globalTightHalo2016Filter"),
)
process.Baseline += process.globalTightHalo2016Filter
#VarsInt.extend(['globalTightHalo2016Filter'])

print "*** HBHENoiseFilter jets producer **************************************************"
process.HBHENoiseFilter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_HBHENoiseFilter"),
)
process.Baseline += process.HBHENoiseFilter
#VarsInt.extend(['HBHENoiseFilter'])

print "*** HBHEIsoNoiseFilter jets producer **************************************************"
process.HBHEIsoNoiseFilter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_HBHENoiseIsoFilter"),
)
process.Baseline += process.HBHEIsoNoiseFilter
#VarsInt.extend(['HBHEIsoNoiseFilter'])

print "*** EcalDeadCellTriggerPrimitiveFilter jets producer **************************************************"
process.EcalDeadCellTriggerPrimitiveFilter = filterDecisionProducer.clone(
    trigTagArg1 = cms.string('TriggerResults'),
    trigTagArg2 = cms.string(''),
    trigTagArg3 = cms.string(tagName),
    filterName  = cms.string("Flag_EcalDeadCellTriggerPrimitiveFilter"),
)
process.Baseline += process.EcalDeadCellTriggerPrimitiveFilter
#VarsInt.extend(['EcalDeadCellTriggerPrimitiveFilter'])

print "*** eeBadScFilter jets producer **************************************************"
process.eeBadScFilter = filterDecisionProducer.clone(
    trigTagArg1  = cms.string('TriggerResults'),
    trigTagArg2  = cms.string(''),
    trigTagArg3  = cms.string(tagName),
    filterName  =   cms.string("Flag_eeBadScFilter"),
    )
process.Baseline += process.eeBadScFilter
#VarsInt.extend(['eeBadScFilter'])

print "*** BadChargedCandidateFilter jets producer **************************************************"
# some filters need to be rerun
process.load('RecoMET.METFilters.BadChargedCandidateFilter_cfi')
process.BadChargedCandidateFilter.muons = cms.InputTag("slimmedMuons")
process.BadChargedCandidateFilter.PFCandidates = cms.InputTag("packedPFCandidates")
process.BadChargedCandidateFilter.taggingMode = True
process.Baseline += process.BadChargedCandidateFilter
#VarsBool.extend(['BadChargedCandidateFilter'])

print "*** BadPFMuonFilter jets producer **************************************************"
process.load('RecoMET.METFilters.BadPFMuonFilter_cfi')
process.BadPFMuonFilter.muons = cms.InputTag("slimmedMuons")
process.BadPFMuonFilter.PFCandidates = cms.InputTag("packedPFCandidates")
process.BadPFMuonFilter.taggingMode = True
process.Baseline += process.BadPFMuonFilter
#VarsBool.extend(['BadPFMuonFilter'])

## --- MHT jets producer -----------------------------------------------
print "*** MHT jets producer **************************************************"
from TreeMaker.Utils.subJetSelection_cfi import SubJetSelection
process.MHTJets = SubJetSelection.clone(
                                        JetTag  = cms.InputTag('GoodJets'),
                                        MinPt   = cms.double(30),
                                        MaxEta  = cms.double(5.0),
                                        )
process.Baseline += process.MHTJets

## --- MHT producer ----------------------------------------------------
print "*** MHT producer **************************************************"
from TreeMaker.Utils.mhtdouble_cfi import mhtdouble
process.MHT = mhtdouble.clone(
                              JetTag  = cms.InputTag('MHTJets'),
)
process.Baseline += process.MHT
#VarsDouble.extend(['MHT:Pt(MHT)','MHT:Phi(MHT)'])

print "*** NoMuonJet jets producer **************************************************"
## --- NoMuonJet producer -----------------------------------------------
from TreeMaker.Utils.nomuonjet_cfi import nomuonjet
process.NoMuonInt = nomuonjet.clone(
                                        JetTag     = cms.InputTag('HTJets'),
                                        MHTPhiTag  = cms.InputTag('MHT:Phi'),
)
process.Baseline += process.NoMuonInt
#VarsInt.extend(['NoMuonInt:NoMuonInt'])

## --- Setup dump event content ----------------------------------------
process.dump   = cms.EDAnalyzer("EventContentAnalyzer")

process.p = cms.Path(
                     #process.dump *
                     process.IsolatedElectronTracksVeto *
                     process.IsolatedMuonTracksVeto *
                     process.IsolatedPionTracksVeto *
                     process.patJetCorrFactorsReapplyJEC *
                     process.patJetsReapplyJEC *
                     process.GoodLeptons *
                     process.GoodPhotons *
                     process.GoodJets *
                     process.WeightProducer *
                     process.Baseline *
                     process.MCReso
                     )
