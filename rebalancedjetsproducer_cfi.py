import FWCore.ParameterSet.Config as cms

rebalancedjetsProducer = cms.EDProducer('RebalancedJetsProducer',
    jetCollection = cms.InputTag('GoodJets'),
    btagTag = cms.string('pfCombinedInclusiveSecondaryVertexV2BJetTags'),
    SmearingFile = cms.string('file.root'),
)
