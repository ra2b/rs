import FWCore.ParameterSet.Config as cms

randsjetsProducer = cms.EDProducer('RandSJetsProducer',
    jetCollection = cms.InputTag('selectedUpdatedPatJets'),
    btagTag = cms.string('pfCombinedInclusiveSecondaryVertexV2BJetTags'),
    genJetCollection = cms.InputTag('slimmedGenJets'),
    SmearingFile = cms.string('file.root'),
    NTries  = cms.int32(10),
    NSmearsPerTry  = cms.int32(1),
    treemakerWeightName = cms.InputTag('PrescaleWeightProducer','weight'),
    PrintJets = cms.bool(False),
    doGenSmear = cms.bool(False),
)
