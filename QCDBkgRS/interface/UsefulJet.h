#ifndef USEFULJET_H
#define USEFULJET_H

#include <cmath>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include "TMinuit.h"
#include "TLorentzVector.h"
#include "TF1.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TFile.h"


double BTAG_CSV = 0.8484;
double JET_PT_THRESH = 30;
double lhdMhtThresh = 15;

struct UsefulJet
{ 
  TLorentzVector tlv;
  double csv;
  int patIdx;
  double origPt;
  bool operator < (const UsefulJet& jet) const 
  {
    return (tlv.Pt() > jet.tlv.Pt());
  }
  UsefulJet& operator+=(const UsefulJet& other)
  {
    tlv += other.tlv;
    return *this;
  }
  UsefulJet& operator-=(const UsefulJet& other)
  {
    tlv -= other.tlv;
    return *this;
  }
  UsefulJet& operator*=(const double multiplier)
  {
    tlv *= multiplier;
    return *this;
  }
UsefulJet(TLorentzVector tlv_ = TLorentzVector(), double csv_ = 0, int patIdx_ = -1, double origPt_ = -1.0) :
  tlv(tlv_), csv(csv_), patIdx(patIdx_), origPt(origPt_) {}
  double Pt() const{return tlv.Pt();}//
  double Px() const{return tlv.Px();}//
  double Py() const{return tlv.Py();}//
  double Pz() const{return tlv.Pz();}//
  double Eta(){return tlv.Eta();}
  double Phi() const{return tlv.Phi();}//
  double E(){return tlv.E();}
  double M(){return tlv.M();}
  double Csv(){return csv;}
  double PatIdx(){return patIdx;}
  double OrigPt(){return origPt;}
  UsefulJet Clone()
  {
    UsefulJet newjet(tlv, csv, patIdx, origPt);
    return newjet;
  }
  double DeltaR(const UsefulJet& other)
  {
    return tlv.DeltaR(other.tlv);
  }
  double DeltaR(const TLorentzVector& other)
  {
    return tlv.DeltaR(tlv);
  }
};

UsefulJet operator+(UsefulJet first, // parameter as value, move-construct (or elide)
		    const UsefulJet& second) 
{
  first += second; // implement in terms of mutating operator
  return first; // NRVO (or move-construct)
}

UsefulJet operator-(UsefulJet first, // parameter as value, move-construct (or elide)
		    const UsefulJet& second) 
{
  first -= second; // implement in terms of mutating operator
  return first; // NRVO (or move-construct)
}


std::vector<UsefulJet> CreateUsefulJetVector(std::vector<TLorentzVector> tlvVec, std::vector<double> csvVec){
  std::vector<UsefulJet> usefulJetVector;
  for (unsigned int i = 0; i< tlvVec.size(); i++)
    {
      UsefulJet jet = UsefulJet(tlvVec[i], csvVec[i]);
      usefulJetVector.push_back(jet);
    }
  return usefulJetVector;
}

TLorentzVector getMHT(std::vector<TLorentzVector> tlvVec, double thresh){
  //cout << "running getMHT" << endl;
  TLorentzVector mhtvec;
  for(unsigned int i=0; i < tlvVec.size(); i++)
    {
      TLorentzVector tlv = tlvVec.at(i);
      if (! (tlv.Pt()>thresh)) continue;
      if (! (abs(tlv.Eta())<5.0)) continue;
      mhtvec-=tlv;
    }
  return mhtvec;
}
TLorentzVector getMHT(std::vector<UsefulJet> usefulVec, double thresh)
{

  TLorentzVector mhtvec;
  for(unsigned int i=0; i < usefulVec.size(); i++)
    {
      TLorentzVector tlv = usefulVec.at(i).tlv;
      if (! (tlv.Pt()>thresh)) continue;
      if (! (abs(tlv.Eta())<5.0)) continue;
      mhtvec-=tlv;
    }
  return mhtvec;
}   
double getHT(std::vector<TLorentzVector> tlvVec, double thresh, double etathresh=2.4){
  double ht = 0;
  for(unsigned int i=0; i < tlvVec.size(); i++){
    TLorentzVector jet = tlvVec.at(i);
    if (!(abs(jet.Eta())<etathresh)) continue;
    if (!(jet.Pt()>thresh)) continue;
    ht+=jet.Pt();
  }
  return ht;
}
double getHT(std::vector<UsefulJet> usefulVec, double thresh, double etathresh=2.4){
  double ht = 0;
  for(unsigned int i=0; i < usefulVec.size(); i++){
    UsefulJet jet = usefulVec.at(i);
    if (!(abs(jet.Eta())<etathresh)) continue;
    if (!(jet.Pt()>thresh)) continue;
    ht+=jet.Pt();
  }
  return ht;
}

int countBTags(std::vector<UsefulJet> RecoJets, double thresh = JET_PT_THRESH){
  int nbjets = 0;
  for (unsigned int ireco = 0; ireco < RecoJets.size(); ireco++){
    if (!(RecoJets[ireco].csv>BTAG_CSV)) continue;
    double pt = RecoJets[ireco].Pt();
    if (!(pt>thresh)) continue;
    nbjets+=1;
  }
  return nbjets;
}

void EndowGenJets(std::vector<UsefulJet> recojets, std::vector<UsefulJet> & genjets)
{
  double DrNearest, dR_;
  double csv;
  for(unsigned int igen=0; igen<genjets.size(); igen++)
    {
      DrNearest = 9;
      csv = 0;
      for(unsigned int ireco=0; ireco<recojets.size(); ireco++)
	{
	  dR_ = genjets[igen].tlv.DeltaR(recojets[ireco].tlv);
	  if(!(dR_<DrNearest)) continue;
	  DrNearest=dR_;
	  csv = recojets[ireco].csv;
	  if (dR_<0.25) break;
	}
      genjets[igen].csv = csv;
    }
}

void getLeadingBJet_CC(std::vector<UsefulJet> RecoJets, int & ibjet, int & nbjets, UsefulJet & leadingbjet){
  nbjets = 0;
  ibjet = -1;
  leadingbjet = UsefulJet();
  double highestPt = 0;
  for (unsigned int ireco = 0; ireco < RecoJets.size(); ireco++){
    if (!(RecoJets[ireco].csv>BTAG_CSV)) continue;
    double pt = RecoJets[ireco].Pt();
    if (!(pt>lhdMhtThresh)) continue;
    nbjets+=1;
    if (!(pt>highestPt)) continue;
    highestPt = pt;
    ibjet = ireco;
    leadingbjet = RecoJets[ireco].Clone();
  }
  return;
}

void findJetToPin(std::vector<UsefulJet> jetVec, int nparams, int & ipin, double & cstart )
{
  double ht = getHT(jetVec, JET_PT_THRESH);
  double desiredMht = TMath::Min(120.0, ht/3);// /3 worked better than /2 fyi, for HT300
  //double desiredMht = 120;
  TLorentzVector mhtvec = getMHT(jetVec,30);
  double mhtPt = mhtvec.Pt(); double mhtPhi = mhtvec.Phi();
  //cout << "starting on MHT=" << mhtPt << endl;
  if (mhtPt<desiredMht) {
    ipin = -1; cstart = 1.0;
    return;  
  }
  //cout << "high met event, " << mhtvec.Pt() << endl;
  for (int i = 0; i<nparams; i++)
    {
      double denom = 2*(-pow(desiredMht,2)+pow(jetVec[i].Pt(),2)+pow(mhtPt,2)+2*(jetVec[i].Pt())*
  			mhtPt*TMath::Cos(mhtPhi-jetVec[i].Phi()));
      double num = 2*pow(jetVec[i].Pt(),2)+2*jetVec[i].Pt()*mhtPt*TMath::Cos(mhtPhi-jetVec[i].Phi());
      double discriminant = 2.0*pow(jetVec[i].Pt(),2)*(2*pow(desiredMht,2)+pow(mhtPt,2)*
  						       (TMath::Cos(2*(mhtPhi-jetVec[i].Phi()))-1));
      double num1 = num+TMath::Sqrt(discriminant);
      double num2 = num-TMath::Sqrt(discriminant);
      double c1 = num1/denom;
      if (fabs(c1-1)<0.8){
  	ipin = i; cstart = c1;
  	return;
      }
      double c2 = num2/denom;
      if (fabs(c2-1)<0.8){
  	ipin = i; cstart = c2;
  	return;
      }
    }
  std::cout << "couldn't find jet to pin, returning 1" << std::endl;
  ipin = -2; cstart = 1.0;
  return;
}

struct TemplateSet
{ 
  TH1F * hPtTemplate;
  TH1F * hEtaTemplate;
  TH1F * hHtTemplate;

  std::vector<std::vector<std::vector<TH1F*>>> ResponseHistos;
  std::vector<std::vector<std::vector<TGraph*>>> ResponseFunctions;
  std::vector<TGraph*> gGenMhtPtTemplatesB0, gGenMhtPtTemplatesB1, gGenMhtPtTemplatesB2, gGenMhtPtTemplatesB3;
  std::vector<TGraph*> gGenMhtDPhiTemplatesB0, gGenMhtDPhiTemplatesB1, gGenMhtDPhiTemplatesB2, gGenMhtDPhiTemplatesB3;
				    
  double rebThresh;
  double lhdMhtThresh;
  std::vector<UsefulJet> untouchedJets;

  int nparams;
  std::vector<UsefulJet> dynamicJets;

  TemplateSet() {}
};






#endif
