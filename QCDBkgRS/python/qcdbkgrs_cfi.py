import FWCore.ParameterSet.Config as cms

## Default QCD Smearing method
QCDfromSmearing = cms.EDAnalyzer('QCDBkgRS',
	RebalanceJetPt = cms.double(15.),
	RebalanceMode = cms.string('MHT'),
	useEnergyTemplateBinning = cms.bool(True),                              
	useBayesFitter = cms.bool(True),                              
	runOnMc = cms.bool(False),# remember to use proper templates!!!False
    doGenSmear = cms.bool(False),
    useEtaPhiSmearing = cms.bool(True),
	NSmearedJets = cms.int32(-1),
	SmearedJetPt = cms.double(15.),
	SmearCollection = cms.string('Reco'),
	PtBinEdges_scaling = cms.vdouble(0., 7000.),
	EtaBinEdges_scaling = cms.vdouble(0.0, 5.0),
	AdditionalSmearing = cms.vdouble(0.1),
	LowerTailScaling = cms.vdouble(1.0),
	UpperTailScaling = cms.vdouble(1.0),
	AdditionalSmearing_variation = cms.double(1.0),
	LowerTailScaling_variation = cms.double(1.0),
	UpperTailScaling_variation = cms.double(1.0),
	#PtBinEdges = cms.vdouble(0, 20, 30, 50, 80, 120, 170, 230, 300, 380, 470, 570, 680, 800, 1000, 1300, 1700, 2200, 2800, 3500, 4300, 5200, 6500),
	#EtaBinEdges = cms.vdouble(0, 0.3, 0.5, 0.8, 1.1, 1.4, 1.7, 2.0, 2.3, 2.8, 3.2, 4.1, 5.0),

	PtBinEdges = cms.vdouble(0,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,44,48,52,56,60,66,72,78,84,90,100,110,120,130,140,150,160,170,180,190,200,220,240,260,280,300,330,360,390,420,450,500,550,600,650,700,750,800,900,1000,1100,1200,1300,1400,1500,1600,1800,2000,2200,2500,3000,3500,4000,5000,6000),    ## Sam's amazing binning
	EtaBinEdges = cms.vdouble(0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5.0), ## Sam's binning

	#PtBinEdges = cms.vdouble(0, 5, 10, 15, 20, 25, 30, 50, 80, 120, 170, 230, 300, 380, 470, 570, 680, 800, 1000, 1300, 1700, 2200, 2800, 3500), # some old binning
	#EtaBinEdges = cms.vdouble(0, 0.5, 1.1, 1.7, 2.3, 3.2, 5.0), # some old binning
	genjetCollection = cms.InputTag('slimmedGenJets'),
    PrintJets = cms.bool(False),
    #FitSucceed = cms.bool(True),
    btagTag = cms.string('pfCombinedInclusiveSecondaryVertexV2BJetTags'),
	btagCut = cms.double(0.80),
    NSmearsPerTry  = cms.int32(10),

    jetCollection = cms.InputTag('RandSJets'),
    jetCollectionBase = cms.InputTag('RandSJets:RandSJetsTry'),
    FitSucceed = cms.InputTag('RandSJets:FitSucceed'),
    

	PFCaloRatio = cms.InputTag('MET:PFCaloPtRatio'),
	NVtx = cms.InputTag('NVtx'),

	NoMuon = cms.InputTag('NoMuonInt:NoMuonInt'),
	HBHENoise = cms.InputTag('HBHENoiseFilter'),
	HBHEIsoNoise = cms.InputTag('HBHEIsoNoiseFilter'),
	eeBadSc = cms.InputTag('eeBadScFilter'),
	ecalDeadCell = cms.InputTag('EcalDeadCellTriggerPrimitiveFilter'),
	globalTightHalo = cms.InputTag('globalTightHalo2016Filter'),
	badChargedCandidate = cms.InputTag('BadChargedCandidateFilter'),
	badMuon = cms.InputTag('BadPFMuonFilter'),
	jetID = cms.InputTag('GoodJets:JetID'),

    ##	jetCollection = cms.InputTag('slimmedJets'),
	jetCollection_reb = cms.string('rebalancedJets'),
	jetCollection_smeared = cms.string('smearedJets'),
	genjetCollection_smeared = cms.string('smearedGenJets'),
	leptonTag = cms.InputTag('GoodLeptons'),

###########							'HBHENoiseFilter''HBHEIsoNoiseFilter''eeBadScFilter''EcalDeadCellTriggerPrimitiveFilter''globalTightHalo2016Filter''BadChargedCandidateFilter''BadPFMuonFilter'
###########                  	VarsDouble.extend(['MET:Pt(MET)','MET:Phi(METPhi)','MET:CaloPt(CaloMET)','MET:CaloPhi(CaloMETPhi)','MET:PFCaloPtRatio(PFCaloMETRatio)'])
   
	packedGenParticles = cms.InputTag('packedGenParticles'),
	VertexCollection  = cms.InputTag('offlineSlimmedPrimaryVertices'),
	uncertaintyName = cms.string(''),
	InputHistoPt_HF = cms.string('hResponsePt_HF'),
	InputHistoEta_HF = cms.string('hResponseEta_HF'),
	InputHistoPhi_HF = cms.string('hResponsePhi_HF'),
	InputHistoPt_NoHF = cms.string('hResponsePt_NoHF'),
	InputHistoEta_NoHF = cms.string('hResponseEta_NoHF'),
	InputHistoPhi_NoHF = cms.string('hResponsePhi_NoHF'),
	RebalanceCorrectionFile = cms.string('/nfs/dust/cms/user/csander/RA2/AdditionalInputFiles_8TeV/RebalanceCorrection.root'),                              
	BTagEfficiencyFile = cms.string('/nfs/dust/cms/user/csander/RA2/AdditionalInputFiles_8TeV/RebalanceCorrection.root'),                              
	ControlPlots = cms.bool(False),
	IsData       = cms.bool(False),
	IsMadgraph   = cms.bool(False),                              
	SmearingFile = cms.string('file.root'),
	RebalancingFile = cms.string('file.root'),
	OutputFile = cms.string('SmearedAndScaledResponse'),
	NRebin = cms.int32(1),
	treemakerWeightName = cms.InputTag('PrescaleWeightProducer','weight'),
    smearingWeightName = cms.InputTag('RandSJets','SmearingWeight'), 
	absoluteTailScaling = cms.bool(True),
    cleverPrescaleTreating = cms.bool(True),
    useRebalanceCorrectionFactors = cms.bool(False),                              
    testMode = cms.bool(False),                              
    useBTagEfficiencyFactors = cms.bool(True),                              
    useCleverRebalanceCorrectionFactors = cms.bool(False),
	A0RMS = cms.double(2.5),
	A1RMS = cms.double(10.0),
    probExtreme = cms.double(0.),
    HTSeedTag = cms.InputTag('HT'),
    HTSeedMin = cms.double(300.),
    NJetsSeedTag = cms.InputTag('NJets'),
    NJetsSeedMin = cms.int32(2),
	MHTmin = cms.double(0.),
	MHTmax = cms.double(1500.),
	HTmin = cms.double(0.),
	HTmax = cms.double(5000.),
	NbinsMHT = cms.int32(150),
	NbinsHT = cms.int32(100),
	Ntries = cms.int32(100),
	NJetsSave = cms.int32(2),
    HTSave = cms.double(300.),
    MHTSave = cms.double(0.),
	JetsHTPt = cms.double(30.),
	JetsHTEta = cms.double(2.4),
	JetsMHTPt = cms.double(30.),
	JetsMHTEta = cms.double(5.0),
	JetDeltaMin = cms.vdouble(0.5, 0.5, 0.3),
	MHTcut_low = cms.double(200.),
	MHTcut_medium = cms.double(350.),
	MHTcut_high = cms.double(500.),
	HTcut_low = cms.double(350.),
	HTcut_medium = cms.double(500.),
	HTcut_high = cms.double(800.),
	HTcut_veryhigh = cms.double(1000.),
	HTcut_extremehigh = cms.double(1200.)
)




