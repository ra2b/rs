// -*- C++ -*-
//
// Package:    QCDBkgRS
// Class:      QCDBkgRS
//
/**\class QCDBkgRS QCDBkgRS.cc RA2Classic/QCDBkgRS/src/QCDBkgRS.cc

   Description: [one line class summary]

   Implementation:
   [Notes on implementation]
*/
//
// Original Author:  Kristin Heine,,,DESY
//         Created:  Tue Aug  7 15:55:02 CEST 2012
// $Id: QCDBkgRS.cc,v 1.6 2013/01/08 11:14:22 kheine Exp $
//
//

#include "rs/QCDBkgRS/interface/QCDBkgRS.h"
#include "rs/QCDBkgRS/interface/UsefulJet.h"
#include "rs/Met110Mht110FakePho.cpp"

//#include "rs/QCDBkgRS/src/BayesRandS.cc"


#include <TROOT.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TPostScript.h>

//double BTAG_CSV = 0.80;



//--------------------------------------------------------------------------
QCDBkgRS::QCDBkgRS(const edm::ParameterSet& iConfig)
{
  rebalancedJetPt_ = iConfig.getParameter<double> ("RebalanceJetPt");
  rebalanceMode_ = iConfig.getParameter<std::string> ("RebalanceMode");
  useEnergyTemplateBinning_ = iConfig.getParameter<bool> ("useEnergyTemplateBinning");
  useBayesFitter_ = iConfig.getParameter<bool> ("useBayesFitter");
  PrintJets_ = iConfig.getParameter<bool> ("PrintJets");
  runOnMc_ = iConfig.getParameter<bool> ("runOnMc");
  useEtaPhiSmearing_ = iConfig.getParameter<bool> ("useEtaPhiSmearing");
  nSmearedJets_ = iConfig.getParameter<int> ("NSmearedJets");
  smearedJetPt_ = iConfig.getParameter<double> ("SmearedJetPt");
  PtBinEdges_scaling_ = iConfig.getParameter<std::vector<double> > ("PtBinEdges_scaling");
  EtaBinEdges_scaling_ = iConfig.getParameter<std::vector<double> > ("EtaBinEdges_scaling");
  AdditionalSmearing_ = iConfig.getParameter<std::vector<double> > ("AdditionalSmearing");
  LowerTailScaling_ = iConfig.getParameter<std::vector<double> > ("LowerTailScaling");
  UpperTailScaling_ = iConfig.getParameter<std::vector<double> > ("UpperTailScaling");
  AdditionalSmearing_variation_ = iConfig.getParameter<double> ("AdditionalSmearing_variation");
  LowerTailScaling_variation_ = iConfig.getParameter<double> ("LowerTailScaling_variation");
  UpperTailScaling_variation_ = iConfig.getParameter<double> ("UpperTailScaling_variation");
  smearCollection_ = iConfig.getParameter<std::string> ("SmearCollection");
  vertices_ = iConfig.getParameter<edm::InputTag>("VertexCollection");
  genjets_ = iConfig.getParameter<edm::InputTag> ("genjetCollection");
  jetCollection_ = iConfig.getParameter<edm::InputTag> ("jetCollection");
  NoMuonFilter_              = iConfig.getParameter<edm::InputTag> ("NoMuon");
  HBHENoiseFilter_           = iConfig.getParameter<edm::InputTag> ("HBHENoise");
  HBHEIsoNoiseFilter_        = iConfig.getParameter<edm::InputTag> ("HBHEIsoNoise");
  eeBadScFilter_             = iConfig.getParameter<edm::InputTag> ("eeBadSc");
  ecalDeadCellFilter_        = iConfig.getParameter<edm::InputTag> ("ecalDeadCell");
  globalTightHaloFilter_     = iConfig.getParameter<edm::InputTag> ("globalTightHalo");
  badChargedCandidateFilter_ = iConfig.getParameter<edm::InputTag> ("badChargedCandidate");
  badMuonFilter_             = iConfig.getParameter<edm::InputTag> ("badMuon");
  jetIDFilter_               = iConfig.getParameter<edm::InputTag> ("jetID");
  PFCaloRatio_ = iConfig.getParameter<edm::InputTag> ("PFCaloRatio");
  NVtxFilter_ = iConfig.getParameter<edm::InputTag> ("NVtx");
  jets_reb_ = iConfig.getParameter<std::string> ("jetCollection_reb");
  jets_smeared_ = iConfig.getParameter<std::string> ("jetCollection_smeared");
  genjets_smeared_ = iConfig.getParameter<std::string> ("genjetCollection_smeared");
  btagTag_ = iConfig.getParameter<std::string> ("btagTag");
  btagCut_ = iConfig.getParameter<double> ("btagCut");
  leptonTag_ = iConfig.getParameter<edm::InputTag> ("leptonTag");
  if(runOnMc_) genParticlesTag_ = iConfig.getParameter<edm::InputTag>("packedGenParticles");
  uncertaintyName_ = iConfig.getParameter<std::string> ("uncertaintyName");
  inputhistPtHF_ = iConfig.getParameter<std::string> ("InputHistoPt_HF");
  inputhistEtaHF_ = iConfig.getParameter<std::string> ("InputHistoEta_HF");
  inputhistPhiHF_ = iConfig.getParameter<std::string> ("InputHistoPhi_HF");
  inputhistPtNoHF_ = iConfig.getParameter<std::string> ("InputHistoPt_NoHF");
  inputhistEtaNoHF_ = iConfig.getParameter<std::string> ("InputHistoEta_NoHF");
  inputhistPhiNoHF_ = iConfig.getParameter<std::string> ("InputHistoPhi_NoHF");
  RebalanceCorrectionFile_ = iConfig.getParameter<std::string> ("RebalanceCorrectionFile");
  BTagEfficiencyFile_ = iConfig.getParameter<std::string> ("BTagEfficiencyFile");
  controlPlots_ = iConfig.getParameter<bool> ("ControlPlots");
  isData_ = iConfig.getParameter<bool> ("IsData");
  isMadgraph_ = iConfig.getParameter<bool> ("IsMadgraph");
  smearingfile_ = iConfig.getParameter<std::string> ("SmearingFile");
  outputfile_ = iConfig.getParameter<std::string> ("OutputFile");
  NRebin_ = iConfig.getParameter<int> ("NRebin");
  treemakerWeightName_ = iConfig.getParameter<edm::InputTag> ("treemakerWeightName");
  smearingWeightName_ = iConfig.getParameter<edm::InputTag> ("smearingWeightName");
  absoluteTailScaling_ = iConfig.getParameter<bool> ("absoluteTailScaling");
  cleverPrescaleTreating_ = iConfig.getParameter<bool> ("cleverPrescaleTreating");
  useRebalanceCorrectionFactors_ = iConfig.getParameter<bool> ("useRebalanceCorrectionFactors");
  useBTagEfficiencyFactors_ = iConfig.getParameter<bool> ("useBTagEfficiencyFactors");
  useCleverRebalanceCorrectionFactors_ = iConfig.getParameter<bool> ("useCleverRebalanceCorrectionFactors");
  A0RMS_ = iConfig.getParameter<double> ("A0RMS");
  A1RMS_ = iConfig.getParameter<double> ("A1RMS");
  probExtreme_ = iConfig.getParameter<double> ("probExtreme");
  HTSeedTag_ = iConfig.getParameter<edm::InputTag> ("HTSeedTag");
  HTSeedMin_ = iConfig.getParameter<double> ("HTSeedMin");
  NJetsSeedTag_ = iConfig.getParameter<edm::InputTag> ("NJetsSeedTag");
  NJetsSeedMin_ = iConfig.getParameter<int> ("NJetsSeedMin");
  MHTmin_ = iConfig.getParameter<double> ("MHTmin");
  MHTmax_ = iConfig.getParameter<double> ("MHTmax");
  HTmin_ = iConfig.getParameter<double> ("HTmin");
  HTmax_ = iConfig.getParameter<double> ("HTmax");
  NbinsMHT_ = iConfig.getParameter<int> ("NbinsMHT");
  NbinsHT_ = iConfig.getParameter<int> ("NbinsHT");
  Ntries_ = iConfig.getParameter<int> ("Ntries");
  NSmearsPerTry_ = iConfig.getParameter<int> ("NSmearsPerTry");
  NJetsSave_ = iConfig.getParameter<int> ("NJetsSave");
  HTSave_ = iConfig.getParameter<double> ("HTSave");
  MHTSave_ = iConfig.getParameter<double> ("MHTSave");
  JetsHTPt_ = iConfig.getParameter<double> ("JetsHTPt");
  JetsHTEta_ = iConfig.getParameter<double> ("JetsHTEta");
  JetsMHTPt_ = iConfig.getParameter<double> ("JetsMHTPt");
  JetsMHTEta_ = iConfig.getParameter<double> ("JetsMHTEta");
  JetDeltaMin_ = iConfig.getParameter<std::vector<double> > ("JetDeltaMin");
  MHTcut_low_ = iConfig.getParameter<double> ("MHTcut_low");
  MHTcut_medium_ = iConfig.getParameter<double> ("MHTcut_medium");
  MHTcut_high_ = iConfig.getParameter<double> ("MHTcut_high");
  HTcut_low_ = iConfig.getParameter<double> ("HTcut_low");
  HTcut_medium_ = iConfig.getParameter<double> ("HTcut_medium");
  HTcut_high_ = iConfig.getParameter<double> ("HTcut_high");
  HTcut_veryhigh_ = iConfig.getParameter<double> ("HTcut_veryhigh");
  HTcut_extremehigh_ = iConfig.getParameter<double> ("HTcut_extremehigh");
  PtBinEdges_ = iConfig.getParameter<std::vector<double> > ("PtBinEdges");
  EtaBinEdges_ = iConfig.getParameter<std::vector<double> > ("EtaBinEdges");
  unsigned int needed_dim = (PtBinEdges_scaling_.size() - 1) * (EtaBinEdges_scaling_.size() - 1);
  if (AdditionalSmearing_.size() != needed_dim) {
    throw edm::Exception(edm::errors::Configuration, "AdditionalSmearing has not correct dimension");
  }
  if (LowerTailScaling_.size() != needed_dim) {
    throw edm::Exception(edm::errors::Configuration, "LowerTailScaling has not correct dimension");
  }
  if (UpperTailScaling_.size() != needed_dim) {
    throw edm::Exception(edm::errors::Configuration, "UpperTailScaling has not correct dimension");
  }

  leptonToken_ = consumes<int>(leptonTag_);
  NJetsSeedToken_ = consumes<int>(NJetsSeedTag_);
  HTSeedToken_ = consumes<double>(HTSeedTag_);
  treemakerWeightToken_ = consumes<double>(treemakerWeightName_);
  smearingWeightToken_ = consumes<double>(smearingWeightName_);
  genjetsToken_ = consumes<edm::View<reco::GenJet> >(genjets_);
  jetsToken_ = consumes<edm::View<pat::Jet> >(jetCollection_);

  for (int itry=0; itry<Ntries_; itry++)
    {
      for (int iextra=0; iextra<NSmearsPerTry_; iextra++)
	{
	  char buffer [50];
	  sprintf (buffer, "selectedUpdatedPatJetsTry%dSmear%d", itry, iextra);
	  jetCollectionVector_.push_back(edm::InputTag(buffer));
	  jetsTokenVector_.push_back(consumes<edm::View<pat::Jet> >(jetCollectionVector_.back()));
	}
    }
  FitSucceed_ = iConfig.getParameter<edm::InputTag> ("FitSucceed");
  fitSucceedToken_ = consumes<bool >(FitSucceed_);

  NoMuonToken_ = consumes<int>(NoMuonFilter_);
  HBHENoiseToken_ = consumes<int>(HBHENoiseFilter_);
  HBHEIsoNoiseToken_ = consumes<int>(HBHEIsoNoiseFilter_);
  eeBadScToken_ = consumes<int>(eeBadScFilter_);
  ecalDeadCellToken_ = consumes<int>(ecalDeadCellFilter_);
  globalTightHaloToken_ = consumes<int>(globalTightHaloFilter_);
  badChargedCandidateToken_ = consumes<bool>(badChargedCandidateFilter_);
  badMuonToken_ = consumes<bool>(badMuonFilter_);
  jetIDToken_ = consumes<bool>(jetIDFilter_);


  PFCaloRatioToken_ = consumes<double>(PFCaloRatio_);
  NVtxToken_ = consumes<int>(NVtxFilter_);

  verticesToken_ = consumes<reco::VertexCollection>(vertices_);
  if(runOnMc_) genParticlesToken_ = consumes<edm::View<pat::PackedGenParticle> >(genParticlesTag_);


  // Different seed per initialization
  gRandom->SetSeed(0);
  rand_ = new TRandom3(0);

  // get object of class SmearFunction
  /////////smearFunc_ = new SmearFunction(iConfig);


}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
QCDBkgRS::~QCDBkgRS()
{
  PtBinEdges_.clear();
  EtaBinEdges_.clear();

  //delete smearFunc_;

  if (rand_)
    delete rand_;
}
//--------------------------------------------------------------------------

//
// member functions
//

//--------------------------------------------------------------------------
std::string QCDBkgRS::GetName(const std::string plot, const std::string uncert, const std::string ptbin) const {
  std::string result(plot);
  if (uncert != "" && uncert != " ")
    result += "_" + uncert;
  if (ptbin != "" && ptbin != " ")
    result += "_" + ptbin;
  return result;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
int QCDBkgRS::GetIndex(const double& x, const std::vector<double>* vec) {
  int index = -1;
  // this is a check
  //int index = 0;
  for (std::vector<double>::const_iterator it = vec->begin(); it != vec->end(); ++it) {
    if ((*it) > fabs(x))
      break;
    ++index;
  }
  if (index < 0)
    index = 0;
  if (index > (int) vec->size() - 2)
    index = vec->size() - 2;

  return index;
}
//--------------------------------------------------------------------------


/*
//--------------------------------------------------------------------------
// pt resolution for KinFitter
double QCDBkgRS::JetResolution_Pt2(const double& pt, const double& eta, const int& i) {
  int ijet;
  i < 1 ? ijet = i : ijet = 0;
  int i_eta = GetIndex(eta, &EtaBinEdges_);
  //return pow(pt, 2) * pow(smearFunc_->getSigmaPtScaledForRebalancing(ijet, i_eta)->Eval(pt), 2);
  return pow(pt, 2) * pow(smearFunc_->getSigmaPtForRebalancing(ijet, i_eta)->Eval(pt), 2);
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// relative pt resolution for KinFitter
double QCDBkgRS::JetResolution_Ptrel(const double& pt, const double& eta, const int& i) {
  int ijet;
  i < 1 ? ijet = i : ijet = 0;
  int i_eta = GetIndex(eta, &EtaBinEdges_);
  return smearFunc_->getSigmaPtScaledForRebalancing(ijet, i_eta)->Eval(pt);
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// eta resolution for KinFitter
double QCDBkgRS::JetResolution_Eta(const double& pt, const double& eta, const int& i, const int& iflav) {
  int ijet;
  i < 1 ? ijet = i : ijet = 0;
  int i_eta = GetIndex(eta, &EtaBinEdges_);
  int i_Pt = GetIndex(pt, &PtBinEdges_);
  return smearFunc_->SigmaEtaHist.at(iflav).at(ijet).at(i_eta).at(i_Pt);
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// phi resolution for KinFitter
double QCDBkgRS::JetResolution_Phi(const double& pt, const double& eta, const int& i, const int& iflav) {
  int ijet;
  i < 1 ? ijet = i : ijet = 0;
  int i_eta = GetIndex(eta, &EtaBinEdges_);
  int i_Pt = GetIndex(pt, &PtBinEdges_);
  return smearFunc_->SigmaPhiHist.at(iflav).at(ijet).at(i_eta).at(i_Pt);
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// pt resolution for smearing
double QCDBkgRS::JetResolutionHist_Pt_Smear(const double& pt, const double& eta, const int& i, const double& HT, const int& NJets, const bool btag) {
  int ijet;
  i < 1 ? ijet = i : ijet = 0;
  int i_Pt = GetIndex(pt, &PtBinEdges_);
  int i_eta = GetIndex(eta, &EtaBinEdges_);

  double res = 1.0;
  if( btag ){
    // get heavy flavor smear function
    res = smearFunc_->getSmearFunc(1, ijet, i_eta, i_Pt)->GetRandom();
    //cout << "B-jet: response " << res << endl;
  }
  else {
    // get no heavy flavor smear function
    res = smearFunc_->getSmearFunc(0, ijet, i_eta, i_Pt)->GetRandom();
    //cout << "noB-jet: response " << res << endl;
  }

  return res;
}

*/
//--------------------------------------------------------------------------
// Get Rebalance corrections:
//--------------------------------------------------------------------------
double QCDBkgRS::GetRebalanceCorrection(double jet_pt, bool btag)
{
  if( jet_pt > 1000. ) jet_pt = 999.;

  if ( jet_pt > rebalancedJetPt_ ) {
    int i_bin = h_RebCorrectionFactor->FindBin(jet_pt);

    if (!useCleverRebalanceCorrectionFactors_){
      double result = 0;
      if (btag) {
	result = h_RebCorrectionFactor_b->GetBinContent(i_bin);
      } else {
	result = h_RebCorrectionFactor->GetBinContent(i_bin);
      }
      if (result < 0.01) result = 1.;
      return result;
    } else {
      double result = 0;
      if (btag) {
	result = h_2DRebCorrectionFactor_b_py.at(i_bin-1)->GetRandom();;
	cout << "result reb bjet: " << result << endl;
      } else {
	result = h_2DRebCorrectionFactor_py.at(i_bin-1)->GetRandom();;
      }
      if (result < 0.01) result = 1.;
      return result;
    }
  }

  else return 1.;

}
//--------------------------------------------------------------------------

double QCDBkgRS::ReadXYHist(TH1F* hist, double x)
{
  // Given x, return y for a 2d histogram.
  //cout << "Called ReadXYHist..." << endl;
  int the_bin = hist->GetXaxis()->FindBin(x);
  if ( hist->IsBinOverflow(the_bin) || hist->IsBinUnderflow(the_bin) ){
    // overflow/underflow can be empty
    cout << "Warning: Reading from under-/overflow; x=" << x << "bin=" << endl;
    // Not optimal; I would like to return the python equiv of None.
    // How to do that?
    return 0;
  }
  double y = hist->GetBinContent(the_bin);
  return y;
}


//--------------------------------------------------------------------------
// Get BTag Efficiencies:
//--------------------------------------------------------------------------
double QCDBkgRS::GetBTagEfficiency(double pt, double eta)
{
  //cout << "Called GetBTagEfficiency..." << endl;
  // Set to unrealistic value:
  double btageff = 0.;
  int eta_bin = GetIndex(eta, &EtaBinEdges_);
  h_BTagEfficiencyFactor = BTagEfficiencyFactors[eta_bin];
  //btageff = h_BTagEfficiencyFactor->GetBinContent(h_BTagEfficiencyFactor->GetXaxis()->FindBin(pt));
  btageff = ReadXYHist(h_BTagEfficiencyFactor, pt);
  //cout << "Found efficiency " << btageff << " for pt " << pt << " and eta " << eta << endl;

  if( btageff == 0.0 ){
    // Reset to unrealistic value.
    btageff = 0.;
  }

  return btageff;

}
//--------------------------------------------------------------------------
// Get BMisTag Efficiencies:
//--------------------------------------------------------------------------
double QCDBkgRS::GetBMisTagEfficiency(double pt, double eta)
{
  //cout << "Called GetBmistagEfficiency..." << endl;
  double bmistageff = 1.;
  int eta_bin = GetIndex(eta, &EtaBinEdges_);
  h_BMisTagEfficiencyFactor = BMisTagEfficiencyFactors[eta_bin];
  //bmistageff = h_BMisTagEfficiencyFactor->GetBinContent(h_BMisTagEfficiencyFactor->GetXaxis()->FindBin(pt));
  bmistageff = ReadXYHist(h_BMisTagEfficiencyFactor, pt);
  //cout << "Found mistag efficiency " << bmistageff << " for pt " << pt << " and eta " << eta << endl;

  return bmistageff;
}




//--------------------------------------------------------------------------
// Check for B content in GenJet:
//--------------------------------------------------------------------------
bool QCDBkgRS::GenJetHasBContent(const edm::View<reco::GenJet>::const_iterator& generatorJet)
{
  try{
    for (reco::Candidate::const_iterator daugh = generatorJet->begin(); daugh != generatorJet->end (); daugh++) {
      if ( (&*daugh != NULL) && (daugh != generatorJet->end ())) { // in range

	//const reco::Candidate* constituent = &*daugh; // deref
	//double constituent_pt = constituent->pt();
	// deref
	const reco::Candidate* constituent = dynamic_cast<const reco::Candidate*>(&(*daugh));
	int constituent_pid = std::abs(constituent->pdgId());
	constituent_pid = constituent_pid % 10000;
    
	// Check if pid is that of a b meson:
	if (((500 <= constituent_pid) && (constituent_pid < 600)) || ((5000 <= constituent_pid) && (constituent_pid < 6000))){
	  double conesize = 0.5;
	  // Check if it was inside a cone of 0.5:
	  // (Should you check for pt?)
	  if((deltaR(*generatorJet, *constituent) < conesize)){
	    return true;
	  }
	}
      }
    }
  } catch(std::bad_cast &e) {
    cout << e.what() << "jet constituent not of type reco::Candidate" <<endl;
    return false;

  }
  return false;
}

//
//--------------------------------------------------------------------------
// Get NBTag:
//--------------------------------------------------------------------------
double QCDBkgRS::GetNBTag(double pt, double eta)
{
  //cout << "Called GetNBTag..." << endl;
  double nbtag = 0.;
  int eta_bin = GetIndex(eta, &EtaBinEdges_);
  h_NBTag = NBTags[eta_bin];
  //btageff = h_BTagEfficiencyFactor->GetBinContent(h_BTagEfficiencyFactor->GetXaxis()->FindBin(pt));
  nbtag = ReadXYHist(h_NBTag, pt);
  //cout << "Found NBTag " << nbtag << " for pt " << pt << " and eta " << eta << endl;


  return nbtag;

}

//
//--------------------------------------------------------------------------
// Get NnoBTag:
//--------------------------------------------------------------------------
double QCDBkgRS::GetNnoBTag(double pt, double eta)
{
  //cout << "Called GetNnoBTag..." << endl;
  double n_no_btag = 0.;
  int eta_bin = GetIndex(eta, &EtaBinEdges_);
  h_NnoBTag = NnoBTags[eta_bin];
  //btageff = h_BTagEfficiencyFactor->GetBinContent(h_BTagEfficiencyFactor->GetXaxis()->FindBin(pt));
  n_no_btag = ReadXYHist(h_NnoBTag, pt);
  //cout << "Found NnoBTag " << n_no_btag << " for pt " << pt << " and eta " << eta << endl;


  return n_no_btag;

}

//
//--------------------------------------------------------------------------
// Get NBTrue:
//--------------------------------------------------------------------------
double QCDBkgRS::GetNBTrue(double pt, double eta)
{
  //cout << "Called GetNBTrue..." << endl;
  double nbtrue = 0.;
  int eta_bin = GetIndex(eta, &EtaBinEdges_);
  h_NBTrue = NBTrues[eta_bin];
  //btageff = h_BTagEfficiencyFactor->GetBinContent(h_BTagEfficiencyFactor->GetXaxis()->FindBin(pt));
  nbtrue = ReadXYHist(h_NBTrue, pt);
  //cout << "Found NBTrue ratio " << nbtrue << " for pt " << pt << " and eta " << eta << endl;


  return nbtrue;
}




//--------------------------------------------------------------------------
void QCDBkgRS::FillTheTree(std::vector<UsefulJet> &Jets_rands, int itry, double FinalWeight) {//switch this to pat jets in the future for speed considerations

  TLorentzVector mhtVec = getMHT(Jets_rands, JetsMHTPt_);
  MHT_pred = mhtVec.Pt();
  if (MHT_pred >= MHTSave_)
    {
      HT_pred = getHT(Jets_rands, JetsHTPt_);
      if (HT_pred >= HTSave_)
	{
	  Njets_pred = calcNJets(Jets_rands);
	  if (Njets_pred >= NJetsSave_)
	    {
	      FillPredictions(Jets_rands, itry, FinalWeight);
	      double ht5 = getHT(Jets_rands, JetsHTPt_, 5.0);
	      HTRatio_pred = ht5/HT_pred;
	      PredictionTree->Fill();
	      // clean variables in tree
	      weight = 0.;
	      Ntries_pred = 0.;
	      Njets_pred = 0;
	      BTags_pred = 0;
	      HT_pred = 0.;
	      MHT_pred = 0.;
	      Jet1Pt_pred = 0.;
	      Jet2Pt_pred = 0.;
	      Jet3Pt_pred = 0.;
	      Jet4Pt_pred = 0.;
	      Jet1Eta_pred = 0.;
	      Jet2Eta_pred = 0.;
	      Jet3Eta_pred = 0.;
	      Jet4Eta_pred = 0.;
	      DeltaPhi1_pred = -11;
	      DeltaPhi2_pred = -11;
	      DeltaPhi3_pred = -11;
	      DeltaPhi4_pred = -11;
	      DeltaPhiMinN_pred = 0.;
	      HTRatio_pred = 0.;
	    }
	}
    }





  
  /*
    for (int i = 1; i <= Ntries_; ++i) {
    int Ntries2 = 3;
    double FinalWeight = WeightFromTreemaker_;
    if (cleverPrescaleTreating_ == true && WeightFromTreemaker_ > 1) {
    Ntries2 = (int) WeightFromTreemaker_*3;
      if (Ntries2 > 300) Ntries2 = 300;
      FinalWeight = WeightFromTreemaker_ / Ntries2;
    }
    for (int j = 1; j <= Ntries2; ++j) {
      // double btag_correction = 1.;
      // double p_btrue = 0.;
      // double p_btag = 0.;
      Jets_smeared.clear();
      // Clear map:
      dynamic_jet_btag_map.clear();
      newdynamic_jet_btag_map.clear();

      // Iterate over jets:
      int ijet = 0;
      for (std::vector<UsefulJet>::iterator it = Jets_reb.begin(); it != Jets_reb.end(); ++it) {
	if (ijet < n2smear){

	  double scale; 
	  double newEta;
	  double newPhi;
	  bool btrue = false;
	  int iflav = it->Csv()>BTAG_CSV;
	  
	  if(useEnergyTemplateBinning_){
	    scale = JetResolutionHist_Pt_Smear(it->E(), it->Eta(), ijet, HT, NJets_reb, btrue);
	    newEta = rand_->Gaus(it->Eta(), JetResolution_Eta(it->E(), it->Eta(), ijet, iflav));
	    newEta = rand_->Gaus(it->Eta(), JetResolution_Eta(it->E(), it->Eta(), ijet, iflav));
	    newPhi = rand_->Gaus(it->Phi(), JetResolution_Phi(it->E(), it->Eta(), ijet, iflav));
	  }else{
	    scale = JetResolutionHist_Pt_Smear(it->Pt(), it->Eta(), ijet, HT, NJets_reb, iflav);
	    newEta = rand_->Gaus(it->Eta(), JetResolution_Eta(it->Pt(), it->Eta(), ijet, iflav));
	    newPhi = rand_->Gaus(it->Phi(), JetResolution_Phi(it->Pt(), it->Eta(), ijet, iflav));
	  }

	  double newE = it->E() * scale;
	  double newMass = it->M() * scale;
	  double newPt = sqrt(newE*newE-newMass*newMass)/cosh(newEta);


	  // No smearing:
	  if(!useEtaPhiSmearing_){
	    newEta = it->Eta();
	    newPhi = it->Phi();
	  }
	  //
					
	  TLorentzVector newP4;
	  newP4.SetPtEtaPhiM(newPt, newEta, newPhi, it->M());
	  UsefulJet smearedJet(newP4,it->Csv());
	  Jets_smeared.push_back(smearedJet);

	} 
	else {
	  UsefulJet neglectedJet(*it);
	  Jets_smeared.push_back(neglectedJet);
	}
	ijet+=1;
      }

      // h_BTagCorrectionFactor_smear->Fill(btag_correction);
      //GreaterByPt<reco::Candidate> ptComparator_;// Do we need this to sort in the next line?
      std::sort(Jets_smeared.begin(), Jets_smeared.end());

      //Fill HT and MHT prediction histos for i-th iteration of smearing
      int NJets = calcNJets(Jets_smeared);
      //			std::cout << "no of jets after smearing = " << NJets << std::endl;
      if (NJets >= NJetsSave_) {
	//FillPredictions(Jets_smeared, i, FinalWeight*btag_correction);
	//FillPredictions(Jets_smeared, i, FinalWeight, dynamic_jet_btag_map);
	FillPredictions(Jets_smeared, i, FinalWeight, dynamic_jet_btag_map);

	if( HT_pred > HTSave_ && MHT_pred > MHTSave_){
	  PredictionTree->Fill();
	}

	// clean variables in tree
	weight = 0.;
	Ntries_pred = 0.;
	Njets_pred = 0;
	BTags_pred = 0;
	HT_pred = 0.;
	MHT_pred = 0.;
	Jet1Pt_pred = 0.;
	Jet2Pt_pred = 0.;
	Jet3Pt_pred = 0.;
	Jet4Pt_pred = 0.;
	Jet1Eta_pred = 0.;
	Jet2Eta_pred = 0.;
	Jet3Eta_pred = 0.;
	Jet4Eta_pred = 0.;
	DeltaPhi1_pred = -11;
	DeltaPhi2_pred = -11;
	DeltaPhi3_pred = -11;
	DeltaPhi4_pred = -11;
	DeltaPhiMinN_pred = 0.;
      }
    }
  }
*/

  return;
}


//--------------------------------------------------------------------------
int QCDBkgRS::calcNJets(std::vector<UsefulJet>& Jets_smeared) {
  int NJets = 0;
  for (vector<UsefulJet>::iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->Pt() > JetsHTPt_ && std::abs(it->Eta()) < JetsHTEta_) {
      ++NJets;
    }
  }
  return NJets;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
int QCDBkgRS::calcNJets(const std::vector<pat::Jet>& Jets_smeared) {
  int NJets = 0;
  for (vector<pat::Jet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      ++NJets;
    }
  }
  return NJets;
}
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
int QCDBkgRS::calcNBJets(std::vector<UsefulJet>& Jets_smeared, std::map <const UsefulJet*, bool>& btag_map) {
  int NBJets = 0;
  for (vector<UsefulJet>::iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    //if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_ && it->bDiscriminator(btagTag_) > btagCut_) {
    //
    if ((it->Pt() > JetsHTPt_ && std::abs(it->Eta()) < JetsHTEta_) && btag_map[&(*it)]) {
      ++NBJets;
    }
  }
  return NBJets;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
int QCDBkgRS::calcNBJets(const std::vector<pat::Jet>& Jets_smeared, std::map <const reco::Jet*, bool>& btag_map) {
  int NBJets = 0;
  for (vector<pat::Jet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    //if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_ && it->bDiscriminator(btagTag_) > btagCut_) {
    //
    if ((it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) && btag_map[&(*it)]) {
      ++NBJets;
    }
  }
  return NBJets;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
double QCDBkgRS::calcHT(std::vector<UsefulJet>& Jets_smeared) {
  double HT = 0;
  for (std::vector<UsefulJet>::iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->Pt() > JetsHTPt_ && std::abs(it->Eta()) < JetsHTEta_) {
      HT += it->Pt();
    }
  }
  return HT;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
double QCDBkgRS::calcHT(const std::vector<pat::Jet>& Jets_smeared) {
  double HT = 0;
  for (vector<pat::Jet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      HT += it->pt();
    }
  }
  return HT;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
bool QCDBkgRS::calcMinDeltaPhi(const std::vector<pat::Jet>& Jets_smeared, math::PtEtaPhiMLorentzVector& MHT) {
  bool result = true;
  unsigned int i = 0;
  for (vector<pat::Jet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      if (i < JetDeltaMin_.size()) {
	if (std::abs(deltaPhi(MHT, *it)) < JetDeltaMin_.at(i))
	  result = false;
	++i;
      } else {
	break;
      }
    }
  }
  return result;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
math::PtEtaPhiMLorentzVector QCDBkgRS::calcMHT(std::vector<UsefulJet>& Jets_smeared, double ptcut) {
  math::PtEtaPhiMLorentzVector MHT(0, 0, 0, 0);
  for (vector<UsefulJet>::iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->Pt() > ptcut && std::abs(it->Eta()) < JetsMHTEta_) {
      math::PtEtaPhiMLorentzVector temp(it->Pt(), it->Eta(), it->Phi(), it->M());
      MHT -= temp;
    }
  }
  return MHT;
}
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
math::PtEtaPhiMLorentzVector QCDBkgRS::calcMHT(const std::vector<pat::Jet>& Jets_smeared, double ptcut) {
  math::PtEtaPhiMLorentzVector MHT(0, 0, 0, 0);
  for (vector<pat::Jet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > ptcut && std::abs(it->eta()) < JetsMHTEta_) {
      MHT -= it->p4();
    }
  }
  return MHT;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
void QCDBkgRS::FillLeadingJetPredictions(std::vector<UsefulJet>& Jets_smeared) {
  int NJets = 0;
  for (vector<UsefulJet>::iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->Pt() > JetsHTPt_ && std::abs(it->Eta()) < JetsHTEta_) {
      ++NJets;

      if( NJets == 1 ) {
	Jet1Pt_pred = it->Pt();
	Jet1Eta_pred = it->Eta();
      }
      if( NJets == 2 ) {
	Jet2Pt_pred = it->Pt();
	Jet2Eta_pred = it->Eta();
      }
      if( NJets == 3 ) {
	Jet3Pt_pred = it->Pt();
	Jet3Eta_pred = it->Eta();
      }
      if( NJets == 4 ) {
	Jet4Pt_pred = it->Pt();
	Jet4Eta_pred = it->Eta();
	break;
      }
    }
  }
  if( NJets < 3 ) {
    Jet3Pt_pred = -1.;
    Jet3Eta_pred = 9999.;
  }
  if( NJets < 4 ) {
    Jet4Pt_pred = -1.;
    Jet4Eta_pred = 9999.;
  }

  return;
}
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
void QCDBkgRS::FillLeadingJetPredictions(const std::vector<pat::Jet>& Jets_smeared) {
  int NJets = 0;
  for (vector<pat::Jet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      ++NJets;

      if( NJets == 1 ) {
	Jet1Pt_pred = it->pt();
	Jet1Eta_pred = it->eta();
      }
      if( NJets == 2 ) {
	Jet2Pt_pred = it->pt();
	Jet2Eta_pred = it->eta();
      }
      if( NJets == 3 ) {
	Jet3Pt_pred = it->pt();
	Jet3Eta_pred = it->eta();
      }
      if( NJets == 4 ) {
	Jet4Pt_pred = it->pt();
	Jet4Eta_pred = it->eta();
	break;
      }
    }
  }
  if( NJets < 3 ) {
    Jet3Pt_pred = -1.;
    Jet3Eta_pred = 9999.;
  }
  if( NJets < 4 ) {
    Jet4Pt_pred = -1.;
    Jet4Eta_pred = 9999.;
  }

  return;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
void QCDBkgRS::FillDeltaPhiPredictions(std::vector<UsefulJet>& Jets_smeared, math::PtEtaPhiMLorentzVector& vMHT) {

  int NJets = 0;

  for (vector<UsefulJet>::iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {

    if (it->Pt() > JetsHTPt_ && std::abs(it->Eta()) < JetsHTEta_) {
      ++NJets;
	 
      TLorentzVector tlvMht;
      tlvMht.SetPtEtaPhiM(vMHT.Pt(),0,vMHT.Phi(),vMHT.M());

      if( NJets == 1 ) DeltaPhi1_pred = fabs(it->tlv.DeltaPhi(tlvMht));
      if( NJets == 2 ) DeltaPhi2_pred = fabs(it->tlv.DeltaPhi(tlvMht));
      if( NJets == 3 ) DeltaPhi3_pred = fabs(it->tlv.DeltaPhi(tlvMht));
      if( NJets == 4 ) 
	{
	  DeltaPhi4_pred = fabs(it->tlv.DeltaPhi(tlvMht));
	  break;
	}
    }
  } 
  if( NJets == 2 ) {
    DeltaPhi3_pred = 9999.;
    DeltaPhi4_pred = 9999.;
  }
  if( NJets == 3 ) {
    DeltaPhi4_pred = 9999.;
  }
  
  return;
}
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
void QCDBkgRS::FillDeltaPhiPredictions(const std::vector<pat::Jet>& Jets_smeared, math::PtEtaPhiMLorentzVector& vMHT) {

  int NJets = 0;
  double dpnhat_min = 9999;

  for (vector<pat::Jet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {

    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      ++NJets;

      double deltaT = 0.0;
      float jres = 0.1;
      double sum = 0.0;
      double dphi = 0.0;
      double dpnhat = 0;

      if (NJets < 4) {
	dphi = std::abs(deltaPhi(vMHT, *it));
	for (vector<pat::Jet>::const_iterator jt = Jets_smeared.begin(); jt != Jets_smeared.end(); ++jt) {
	  if (&(*it) == &(*jt)) continue;
	  sum += (jt->px()*it->py()-it->px()*jt->py()) * (jt->px()*it->py()-it->px()*jt->py());
	}
	deltaT = jres*sqrt(sum)/it->pt();
      }

      if (deltaT/vMHT.pt() >= 1.0) dpnhat = dphi/(TMath::Pi()/2.0);
      else dpnhat=dphi/asin(deltaT/vMHT.pt());
      if (dpnhat < dpnhat_min) dpnhat_min = dpnhat;

      if( NJets == 1 ) {
	DeltaPhi1_pred = dphi;
      }
      if( NJets == 2 ) {
	DeltaPhi2_pred = dphi;
      }
      if( NJets == 3 ) {
	DeltaPhi3_pred = dphi;
      }
      if( NJets == 4 ) {
	DeltaPhi4_pred = dphi;
	break;
      }
    }
  }

  DeltaPhiMinN_pred = dpnhat_min;

  if( NJets == 2 ) {
    DeltaPhi3_pred = 9999.;
    DeltaPhi4_pred = 9999.;
  }
  if( NJets == 3 ) {
    DeltaPhi4_pred = 9999.;
  }

  return;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
int QCDBkgRS::calcNJets_gen(const std::vector<reco::GenJet>& Jets_smeared) {
  int NJets = 0;
  for (vector<reco::GenJet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      ++NJets;
    }
  }
  return NJets;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
int QCDBkgRS::calcNBJets_gen(const std::vector<reco::GenJet>& Jets_smeared, std::map <const reco::GenJet*, bool>& b_map) {
  int NBJets = 0;
  for (vector<reco::GenJet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      if (b_map[&(*it)]){
	++NBJets;
	//cout << "Found a bjet: NBJets is now " << NBJets << endl;
      }
    }
  }
  return NBJets;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
double QCDBkgRS::calcHT_gen(const std::vector<reco::GenJet>& Jets_smeared) {
  double HT = 0;
  for (vector<reco::GenJet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      HT += it->pt();
    }
  }
  return HT;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
bool QCDBkgRS::calcMinDeltaPhi_gen(const std::vector<reco::GenJet>& Jets_smeared, math::PtEtaPhiMLorentzVector& MHT) {
  bool result = true;
  unsigned int i = 0;
  for (vector<reco::GenJet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      if (i < JetDeltaMin_.size()) {
	if (std::abs(deltaPhi(MHT, *it)) < JetDeltaMin_.at(i))
	  result = false;
	++i;
      } else {
	break;
      }
    }
  }
  return result;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
math::PtEtaPhiMLorentzVector QCDBkgRS::calcMHT_gen(const std::vector<reco::GenJet>& Jets_smeared, double ptcut) {
  math::PtEtaPhiMLorentzVector MHT(0, 0, 0, 0);
  for (vector<reco::GenJet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > ptcut && std::abs(it->eta()) < JetsMHTEta_) {
      MHT -= it->p4();
    }
  }
  return MHT;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
void QCDBkgRS::FillLeadingJetPredictions_gen(const std::vector<reco::GenJet>& Jets_smeared) {
  int NJets = 0;
  for (vector<reco::GenJet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {
    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      ++NJets;

      if( NJets == 1 ) {
	Jet1Pt_pred = it->pt();
	Jet1Eta_pred = it->eta();

      }
      if( NJets == 2 ) {
	Jet2Pt_pred = it->pt();
	Jet2Eta_pred = it->eta();
      }
      if( NJets == 3 ) {
	Jet3Pt_pred = it->pt();
	Jet3Eta_pred = it->eta();
      }
      if( NJets == 4 ) {
	Jet4Pt_pred = it->pt();
	Jet4Eta_pred = it->eta();
	break;
      }
    }
  }
  if( NJets < 3 ) {
    Jet3Pt_pred = -1.;
    Jet3Eta_pred = 9999.;
  }
  if( NJets < 4 ) {
    Jet4Pt_pred = -1.;
    Jet4Eta_pred = 9999.;
  }

  return;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
void QCDBkgRS::FillDeltaPhiPredictions_gen(const std::vector<reco::GenJet>& Jets_smeared, math::PtEtaPhiMLorentzVector& vMHT) {

  int NJets = 0;
  double dpnhat_min = 9999;

  for (vector<reco::GenJet>::const_iterator it = Jets_smeared.begin(); it != Jets_smeared.end(); ++it) {

    if (it->pt() > JetsHTPt_ && std::abs(it->eta()) < JetsHTEta_) {
      ++NJets;

      double deltaT = 0.0;
      float jres = 0.1;
      double sum = 0.0;
      double dphi = 0;
      double dpnhat = 0;

      if (NJets < 4) {
	dphi = std::abs(deltaPhi(vMHT, *it));
	for (vector<reco::GenJet>::const_iterator jt = Jets_smeared.begin(); jt != Jets_smeared.end(); ++jt) {
	  if (&(*it) == &(*jt)) continue;
	  sum += (jt->px()*it->py()-it->px()*jt->py()) * (jt->px()*it->py()-it->px()*jt->py());
	}
	deltaT = jres*sqrt(sum)/it->pt();
      }

      if (deltaT/vMHT.pt() >= 1.0) dpnhat = dphi/(TMath::Pi()/2.0);
      else dpnhat=dphi/asin(deltaT/vMHT.pt());
      if (dpnhat < dpnhat_min) dpnhat_min = dpnhat;

      if( NJets == 1 ) {
	DeltaPhi1_pred = dphi;
      }
      if( NJets == 2 ) {
	DeltaPhi2_pred = dphi;
      }
      if( NJets == 3 ) {
	DeltaPhi3_pred = dphi;
      }
      if( NJets == 4 ) {
	DeltaPhi4_pred = dphi;
	break;
      }
    }
  }

  DeltaPhiMinN_pred = dpnhat_min;

  if( NJets == 2 ) {
    DeltaPhi3_pred = 9999.;
    DeltaPhi4_pred = 9999.;
  }
  if( NJets == 3 ) {
    DeltaPhi4_pred = 9999.;
  }

  return;
}
//--------------------------------------------------------------------------


int QCDBkgRS::countBJets_Useful(std::vector<UsefulJet> stdjetvec, double thresh){
  int count = 0;
  for (unsigned int j=0; j<stdjetvec.size(); j++){
    if (stdjetvec[j].Pt()>thresh and abs(stdjetvec[j].Eta())<2.4 and stdjetvec[j].csv>BTAG_CSV) count+=1;
  }
  return count;
}

//--------------------------------------------------------------------------
void QCDBkgRS::FillPredictions(std::vector<UsefulJet>& Jets_smeared, const int& i, const double& FinalWeight) {

  int NJets = calcNJets(Jets_smeared);
  int NBJets = countBJets_Useful(Jets_smeared, JetsHTPt_);
  double HT = calcHT(Jets_smeared);
  math::PtEtaPhiMLorentzVector vMHT = calcMHT(Jets_smeared, JetsMHTPt_);
  math::PtEtaPhiMLorentzVector vMET = calcMHT(Jets_smeared, 30.);
  double MHT = vMHT.pt();

  weight = FinalWeight;
  Ntries_pred = i;
  Njets_pred = NJets;
  BTags_pred = NBJets;
  HT_pred = HT;
  MHT_pred = MHT;
  std::vector<double> HltEffCenterUpDown = Eff_Met110Mht110FakePho_CenterUpDown(HT_pred, MHT_pred, Njets_pred);
  TrigEffWeightNom = HltEffCenterUpDown[0];
  TrigEffWeightUp = HltEffCenterUpDown[1];
  TrigEffWeightDown = HltEffCenterUpDown[2];

  FillDeltaPhiPredictions(Jets_smeared, vMET);
  FillLeadingJetPredictions(Jets_smeared);
  //	std::cout << "Predicted HT, MHT = " << HT << ", " << MHT << std::endl;

  return;
}
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
// ------------ method called for each event  ------------
void QCDBkgRS::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;
  bool Verbose = false;

  edm::EventAuxiliary aux = iEvent.eventAuxiliary();
  runNum_       = aux.run();
  evtNum_       = aux.event();
  smearNum_     = 0;
	
  edm::Handle<int> HBHENoiseHandle_;
  edm::Handle<int> HBHEIsoNoiseHandle_;
  edm::Handle<int> eeBadScHandle_;
  edm::Handle<int> ecalDeadCellHandle_;
  edm::Handle<int> globalTightHaloHandle_;
  edm::Handle<int> NVtxHandle_;
  edm::Handle<int> NoMuonHandle_;
  edm::Handle<bool> badChargedCandidateHandle_;
  edm::Handle<bool> badMuonHandle_;
  edm::Handle<bool> jetIDHandle_;

	
  iEvent.getByToken(NoMuonToken_,NoMuonHandle_);
  iEvent.getByToken(HBHENoiseToken_,HBHENoiseHandle_);
  iEvent.getByToken(HBHEIsoNoiseToken_,HBHEIsoNoiseHandle_);
  iEvent.getByToken(eeBadScToken_,eeBadScHandle_);
  iEvent.getByToken(ecalDeadCellToken_,ecalDeadCellHandle_);
  iEvent.getByToken(globalTightHaloToken_,globalTightHaloHandle_);
  if(!runOnMc_){
    iEvent.getByToken(badChargedCandidateToken_,badChargedCandidateHandle_);
    iEvent.getByToken(badMuonToken_,badMuonHandle_);
  }
  iEvent.getByToken(jetIDToken_,jetIDHandle_);
  iEvent.getByToken(NVtxToken_,NVtxHandle_);

  edm::Handle<bool> FitSucceed;
  iEvent.getByToken(fitSucceedToken_, FitSucceed);
  if(Verbose) std::cout  << "fit succeed in the tree macro is " << (*FitSucceed) << std::endl;

  edm::Handle<double> FilterPFCaloRatio;
  iEvent.getByToken(PFCaloRatioToken_, FilterPFCaloRatio);


if(!runOnMc_){
		if((*globalTightHaloHandle_) == 0 ||
		(*badChargedCandidateHandle_) == 0 ||
		(*badMuonHandle_) == 0){ return;}
	}	
   if ((*NoMuonHandle_) == 0 ||
	(*HBHENoiseHandle_) == 0 ||
	(*HBHEIsoNoiseHandle_) == 0 ||
	(*eeBadScHandle_) == 0 ||
	(*ecalDeadCellHandle_) == 0 ||
	!(*jetIDHandle_) ||
	(*NVtxHandle_) == 0 ||
	(*FilterPFCaloRatio) > 5){ return;}


  //LeptonVeto
  edm::Handle<int> NLeptons;
  iEvent.getByToken(leptonToken_, NLeptons);
  if ((*NLeptons) != 0)
    return;

  //NJets
  edm::Handle<int> NJetsSeed;
  iEvent.getByToken(NJetsSeedToken_, NJetsSeed);
  if ((*NJetsSeed) < NJetsSeedMin_)
    return;

  //HT
  edm::Handle<double> HTSeed;
  iEvent.getByToken(HTSeedToken_, HTSeed);
  if ((*HTSeed) < HTSeedMin_)
    return;

  HT_seed = (*HTSeed);

  //Weight
  edm::Handle<double> weight_treemaker;
  iEvent.getByToken(treemakerWeightToken_, weight_treemaker);
  WeightFromTreemaker_ = (weight_treemaker.isValid() ? (*weight_treemaker) : 1.0);
  WeightFromTreemaker_ *= (*FitSucceed);
  if (!weight_treemaker.isValid()) cout << "weight not found" << endl;

  edm::Handle<double> weight_smearing;
  iEvent.getByToken(smearingWeightToken_, weight_smearing);
  WeightFromSmearing_ = (weight_smearing.isValid() ? (*weight_smearing) : 1.0);



  if (controlPlots_) {
    h_weight->Fill(log10(WeightFromTreemaker_));
    h_weightedWeight->Fill(log10(WeightFromTreemaker_), WeightFromTreemaker_);
  }

  // Number of vertices
  edm::Handle<reco::VertexCollection> vertices;
  iEvent.getByToken(verticesToken_,vertices);
  if( vertices.isValid() ) {
    vtxN = vertices->size();
  }
  
  double FinalWeight = WeightFromTreemaker_ * WeightFromSmearing_ * (*FitSucceed);
  if (Verbose) std::cout << "all the weights going in are: "<< "WeightFromTreemaker_="<<WeightFromTreemaker_<<
		 ", nWeightFromSmearing_=" << WeightFromSmearing_ << ", (*FitSucceed)="<<(*FitSucceed) << std::endl;
  
  if (smearCollection_ == "Reco" && FinalWeight!=0 && (*FitSucceed)) {
    if(PrintJets_) std::cout << "macro:::::::"<< std::endl;
    for (int itry = 0; itry < Ntries_; itry++)
      {      
	Ntries_pred = itry;
	for (int iextra=0; iextra < NSmearsPerTry_; iextra++)
	  {
	    edm::Handle<edm::View<pat::Jet> > Jets_rands;
      	    //iEvent.getByToken(jetsToken_, Jets_rands);
      	    iEvent.getByToken(jetsTokenVector_[itry], Jets_rands);
	    edm::View<pat::Jet> RandSJets = *Jets_rands;
	    std::vector<UsefulJet> UJets_rands;
	    for (edm::View<pat::Jet>::const_iterator it = RandSJets.begin(); it != RandSJets.end(); ++it) {
	      TLorentzVector lvi;
	      lvi.SetPxPyPzE(it->px(), it->py(), it->pz(), it->energy());
	      double csv = it->bDiscriminator(btagTag_);
	      if(PrintJets_ && (itry==0 ||itry==1)&& iextra==0)
		std::cout << itry << "-->pt, eta, csv = " << it->pt() << ", " << it->eta()  << ", " <<  csv << ", " << std::endl;
	      UJets_rands.emplace_back(UsefulJet(lvi,csv));
	    }
	    std::sort(UJets_rands.begin(), UJets_rands.end());
	    FillTheTree(UJets_rands, itry, FinalWeight);
	  }
      }
  }  
}

// ------------ method called once each job just before starting event loop  ------------
void QCDBkgRS::beginJob()
{

  cout << "Starting job" << endl;
  debug = 0;

  edm::Service<TFileService> fs;
  if (!fs) {
    throw edm::Exception(edm::errors::Configuration, "TFile Service is not registered in cfg file");
  }

  if (controlPlots_) {
    h_SeedEvents_HT_NJet2 = fs->make<TH2F> ("SeedEvents_HT_NJet2", "Seed Events", NbinsHT_, HTmin_, HTmax_, 60, 0, 60 );
    h_SeedEvents_HT_NJet2->Sumw2();
    h_SeedEvents_HT_NJet3 = fs->make<TH2F> ("SeedEvents_HT_NJet3", "Seed Events", NbinsHT_, HTmin_, HTmax_, 60, 0, 60 );
    h_SeedEvents_HT_NJet3->Sumw2();
    h_SeedEvents_HT_NJet4 = fs->make<TH2F> ("SeedEvents_HT_NJet4", "Seed Events", NbinsHT_, HTmin_, HTmax_, 60, 0, 60 );
    h_SeedEvents_HT_NJet4->Sumw2();
    h_SeedEvents_HT_NJet5 = fs->make<TH2F> ("SeedEvents_HT_NJet5", "Seed Events", NbinsHT_, HTmin_, HTmax_, 60, 0, 60 );
    h_SeedEvents_HT_NJet5->Sumw2();
    h_SeedEvents_HT_NJet6 = fs->make<TH2F> ("SeedEvents_HT_NJet6", "Seed Events", NbinsHT_, HTmin_, HTmax_, 60, 0, 60 );
    h_SeedEvents_HT_NJet6->Sumw2();
    h_SeedEvents_HT_NJet7 = fs->make<TH2F> ("SeedEvents_HT_NJet7", "Seed Events", NbinsHT_, HTmin_, HTmax_, 60, 0, 60 );
    h_SeedEvents_HT_NJet7->Sumw2();
    h_SeedEvents_HT_NJet8 = fs->make<TH2F> ("SeedEvents_HT_NJet8", "Seed Events", NbinsHT_, HTmin_, HTmax_, 60, 0, 60 );
    h_SeedEvents_HT_NJet8->Sumw2();

    h_DeltaPhiRecoGenJet1_GenHT_LowMHT = fs->make<TH2F> ("h_DeltaPhiRecoGenJet1_GenHT_LowMHT", "h_DeltaPhiRecoGenJet1_GenHT_LowMHT", 100, 0, 0.5, 80, 0, 4000 );
    h_DeltaPhiRecoGenJet1_GenHT_LowMHT->Sumw2();
    h_DeltaPhiRecoGenJet2_GenHT_LowMHT = fs->make<TH2F> ("h_DeltaPhiRecoGenJet2_GenHT_LowMHT", "h_DeltaPhiRecoGenJet2_GenHT_LowMHT", 100, 0, 0.5, 80, 0, 4000 );
    h_DeltaPhiRecoGenJet2_GenHT_LowMHT->Sumw2();
    h_DeltaPhiRecoGenJet3_GenHT_LowMHT = fs->make<TH2F> ("h_DeltaPhiRecoGenJet3_GenHT_LowMHT", "h_DeltaPhiRecoGenJet3_GenHT_LowMHT", 100, 0, 0.5, 80, 0, 4000 );
    h_DeltaPhiRecoGenJet3_GenHT_LowMHT->Sumw2();

    h_DeltaPhiRecoGenJet1_GenHT_HighMHT = fs->make<TH2F> ("h_DeltaPhiRecoGenJet1_GenHT_HighMHT", "h_DeltaPhiRecoGenJet1_GenHT_HighMHT", 100, 0, 0.5, 80, 0, 4000 );
    h_DeltaPhiRecoGenJet1_GenHT_HighMHT->Sumw2();
    h_DeltaPhiRecoGenJet2_GenHT_HighMHT = fs->make<TH2F> ("h_DeltaPhiRecoGenJet2_GenHT_HighMHT", "h_DeltaPhiRecoGenJet2_GenHT_HighMHT", 100, 0, 0.5, 80, 0, 4000 );
    h_DeltaPhiRecoGenJet2_GenHT_HighMHT->Sumw2();
    h_DeltaPhiRecoGenJet3_GenHT_HighMHT = fs->make<TH2F> ("h_DeltaPhiRecoGenJet3_GenHT_HighMHT", "h_DeltaPhiRecoGenJet3_GenHT_HighMHT", 100, 0, 0.5, 80, 0, 4000 );
    h_DeltaPhiRecoGenJet3_GenHT_HighMHT->Sumw2();

    h_AddRelJetActivity07GenJet1_GenHT_LowMHT = fs->make<TH2F> ("h_AddRelJetActivity07GenJet1_GenHT_LowMHT", "h_AddRelJetActivity07GenJet1_GenHT_LowMHT", 100, 0, 1., 80, 0, 4000 );
    h_AddRelJetActivity07GenJet1_GenHT_LowMHT->Sumw2();
    h_AddRelJetActivity07GenJet2_GenHT_LowMHT = fs->make<TH2F> ("h_AddRelJetActivity07GenJet2_GenHT_LowMHT", "h_AddRelJetActivity07GenJet2_GenHT_LowMHT", 100, 0, 1., 80, 0, 4000 );
    h_AddRelJetActivity07GenJet2_GenHT_LowMHT->Sumw2();
    h_AddRelJetActivity07GenJet3_GenHT_LowMHT = fs->make<TH2F> ("h_AddRelJetActivity07GenJet3_GenHT_LowMHT", "h_AddRelJetActivity07GenJet3_GenHT_LowMHT", 100, 0, 1., 80, 0, 4000 );
    h_AddRelJetActivity07GenJet3_GenHT_LowMHT->Sumw2();

    h_AddRelJetActivity07GenJet1_GenHT_HighMHT = fs->make<TH2F> ("h_AddRelJetActivity07GenJet1_GenHT_HighMHT", "h_AddRelJetActivity07GenJet1_GenHT_HighMHT", 100, 0, 1., 80, 0, 4000 );
    h_AddRelJetActivity07GenJet1_GenHT_HighMHT->Sumw2();
    h_AddRelJetActivity07GenJet2_GenHT_HighMHT = fs->make<TH2F> ("h_AddRelJetActivity07GenJet2_GenHT_HighMHT", "h_AddRelJetActivity07GenJet2_GenHT_HighMHT", 100, 0, 1., 80, 0, 4000 );
    h_AddRelJetActivity07GenJet2_GenHT_HighMHT->Sumw2();
    h_AddRelJetActivity07GenJet3_GenHT_HighMHT = fs->make<TH2F> ("h_AddRelJetActivity07GenJet3_GenHT_HighMHT", "h_AddRelJetActivity07GenJet3_GenHT_HighMHT", 100, 0, 1., 80, 0, 4000 );
    h_AddRelJetActivity07GenJet3_GenHT_HighMHT->Sumw2();

    h_nJets_gen = fs->make<TH1F> ("NJets_gen", "NJets", 15, 0., 15);
    h_nJets_gen->Sumw2();
    h_nJets_reco = fs->make<TH1F> ("NJets_reco", "NJets", 15, 0., 15);
    h_nJets_reco->Sumw2();
    h_nJets_reb = fs->make<TH1F> ("NJets_reb", "NJets", 15, 0., 15);
    h_nJets_reb->Sumw2();
    h_nJets_smear = fs->make<TH1F> ("NJets_smear", "NJets", 15, 0., 15);
    h_nJets_smear->Sumw2();

    // Control plots for btag corrections
    h_pBTrue_smear = fs->make<TH1F> ("pBTrue_smear", "pBTrue", 1000, 0., 100.);
    h_pBTrue_smear->Sumw2();
    h_pBTag_smear = fs->make<TH1F> ("pBTag_smear", "pBTag", 1000, 0., 100.);
    h_pBTag_smear->Sumw2();
    h_BTagCorrectionFactor_smear = fs->make<TH1F> ("BTagCorrectionFactor_smear", "BTagCorr", 100000, 0., 1000.);
    h_BTagCorrectionFactor_smear->Sumw2();
    h_NB_Old = fs->make<TH1F> ("NB_Old", "NBOld", 15, 0., 15.);
    h_NB_Old->Sumw2();
    h_NB_New = fs->make<TH1F> ("NB_New", "NBNew", 15, 0., 15.);
    h_NB_New->Sumw2();
    h_random_BTag_smear = fs->make<TH1F> ("Random_Number_BTag_smear", "BTagCorr", 1000, 0., 1.);
    h_random_BTag_smear->Sumw2();
    h_random_BTrue_smear = fs->make<TH1F> ("Random_Number_BTrue_smear", "BTagCorr", 1000, 0., 1.);
    h_random_BTrue_smear->Sumw2();
  }

    cout << "Book histograms for btag effs" << endl;
    int etabins = 12; //EtaBinEdges_.size() - 1; This was 12 in MC Resolutions
    int etamax = EtaBinEdges_.back();
    int etamin = EtaBinEdges_.front();

    cout << "# eta bins: " << etabins << " and etamin: " << etamin << " and etamax: " << etamax << endl;
    h_btageff_genPt_Response = fs->make<TH2F> ("h_btageff_genPt_Response", "h_btageff_genPt_Response", 200, 0., 1000., 10, 0., 5.);
    h_btageff_genPt_Response->Sumw2();
    h_bmistageff_genPt_Response = fs->make<TH2F> ("h_bmistageff_genPt_Response", "h_bmistageff_genPt_Response", 200, 0., 1000., 10, 0, 5.);
    h_bmistageff_genPt_Response->Sumw2();
    h_btageff_SmearPt_Response = fs->make<TH2F> ("h_btageff_SmearPt_Response", "h_btageff_SmearPt_Response", 200, 0., 1000., 10, 0., 5.);
    h_btageff_SmearPt_Response->Sumw2();
    h_bmistageff_SmearPt_Response = fs->make<TH2F> ("h_bmistageff_SmearPt_Response", "h_bmistageff_SmearPt_Response", 200, 0., 1000., 10, 0., 5.);
    h_bmistageff_SmearPt_Response->Sumw2();
    h_trueb_SmearedPt_Eta = fs->make<TH2F> ("h_trueb_SmearedPt_Eta", "h_trueb_SmearedPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_trueb_SmearedPt_Eta->Sumw2();
    h_trueb_random_SmearedPt_Eta = fs->make<TH2F> ("h_trueb_random_SmearedPt_Eta", "h_trueb_random_SmearedPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_trueb_random_SmearedPt_Eta->Sumw2();
    h_trueb_genPt_Eta = fs->make<TH2F> ("h_trueb_genPt_Eta", "h_trueb_genPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_trueb_genPt_Eta->Sumw2();
    h_no_trueb_genPt_Eta = fs->make<TH2F> ("h_no_trueb_genPt_Eta", "h_no_trueb_genPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_no_trueb_genPt_Eta->Sumw2();
    h_no_trueb_SmearedPt_Eta = fs->make<TH2F> ("h_no_trueb_SmearedPt_Eta", "h_no_trueb_SmearedPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_no_trueb_SmearedPt_Eta->Sumw2();
    h_trueb_btag_genPt_Eta = fs->make<TH2F> ("h_trueb_btag_genPt_Eta", "h_trueb_btag_genPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_trueb_btag_genPt_Eta->Sumw2();
    h_trueb_btag_SmearedPt_Eta = fs->make<TH2F> ("h_trueb_btag_SmearedPt_Eta", "h_trueb_btag_SmearedPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_trueb_btag_SmearedPt_Eta->Sumw2();
    h_no_trueb_btag_genPt_Eta = fs->make<TH2F> ("h_no_trueb_btag_genPt_Eta", "h_no_trueb_btag_genPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_no_trueb_btag_genPt_Eta->Sumw2();
    h_no_trueb_btag_SmearedPt_Eta = fs->make<TH2F> ("h_no_trueb_btag_SmearedPt_Eta", "h_no_trueb_btag_SmearedPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_no_trueb_btag_SmearedPt_Eta->Sumw2();
    h_btag_SmearedPt_Eta = fs->make<TH2F> ("h_btag_SmearedPt_Eta", "h_btag_SmearedPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_btag_SmearedPt_Eta->Sumw2();
    h_btag_old_SmearedPt_Eta = fs->make<TH2F> ("h_btag_old_SmearedPt_Eta", "h_btag_old_SmearedPt_Eta", 200, 0., 1000., etabins, etamin, etamax);
    h_btag_old_SmearedPt_Eta->Sumw2();

    h_JetPt_gen = fs->make<TH1F> ("JetPt_gen", "Jet pt", 1000, 0., 1000.);
    h_JetPt_gen->Sumw2();
    h_JetPt_reco = fs->make<TH1F> ("JetPt_reco", "Jet pt", 1000, 0., 1000.);
    h_JetPt_reco->Sumw2();
    h_JetPt_reb = fs->make<TH1F> ("JetPt_reb", "Jet pt", 1000, 0., 1000.);
    h_JetPt_reb->Sumw2();
    h_JetPt_smear = fs->make<TH1F> ("JetPt_smear", "Jet pt", 1000, 0., 1000.);
    h_JetPt_smear->Sumw2();
    h_bJetPt_gen = fs->make<TH1F> ("bJetPt_gen", "BJet pt", 1000, 0., 1000.);
    h_bJetPt_gen->Sumw2();
    h_bJetPt_reco = fs->make<TH1F> ("bJetPt_reco", "BJet pt", 1000, 0., 1000.);
    h_bJetPt_reco->Sumw2();
    h_bJetPt_reb = fs->make<TH1F> ("bJetPt_reb", "BJet pt", 1000, 0., 1000.);
    h_bJetPt_reb->Sumw2();
    h_bJetPt_smear = fs->make<TH1F> ("bJetPt_smear", "BJet pt", 1000, 0., 1000.);
    h_bJetPt_smear->Sumw2();
    h_nonbJetPt_gen = fs->make<TH1F> ("nonbJetPt_gen", "nonBJet pt", 1000, 0., 1000.);
    h_nonbJetPt_gen->Sumw2();
    h_nonbJetPt_reco = fs->make<TH1F> ("nonbJetPt_reco", "nonBJet pt", 1000, 0., 1000.);
    h_nonbJetPt_reco->Sumw2();
    h_nonbJetPt_reb = fs->make<TH1F> ("nonbJetPt_reb", "nonBJet pt", 1000, 0., 1000.);
    h_nonbJetPt_reb->Sumw2();
    h_nonbJetPt_smear = fs->make<TH1F> ("nonbJetPt_smear", "nonBJet pt", 1000, 0., 1000.);
    h_nonbJetPt_smear->Sumw2();

    h_deltaR_rebCorr = fs->make<TH1F> ("deltaR_rebCorr", "deltaR", 400, 0., 2.);
    h_deltaR_rebCorr->Sumw2();

    h_RebCorrection_vsReco = fs->make<TH2F> ("RebCorrection_vsReco", "Jet pt", 1000, 0., 1000., 100, 0., 3.);
    h_RebCorrection_vsReco->Sumw2();
    h_RebCorrection_vsReco_b = fs->make<TH2F> ("RebCorrection_vsReco_b", "Jet pt", 1000, 0., 1000., 100, 0., 3.);
    h_RebCorrection_vsReco_b->Sumw2();

    h_RecJetMatched_Pt = fs->make<TH1F> ("RecJetMatched_Pt", "RecJetMatched_Pt", 1000, 0., 1000.);
    h_RecJetMatched_Pt->Sumw2();
    h_RecJetNotMatched_Pt = fs->make<TH1F> ("RecJetNotMatched_Pt", "RecJetNotMatched_Pt", 1000, 0., 1000.);
    h_RecJetNotMatched_Pt->Sumw2();
    h_RecJetMatched_JetBin1_Pt = fs->make<TH1F> ("RecJetMatched_JetBin1_Pt", "RecJetMatched_JetBin1_Pt", 1000, 0., 1000.);
    h_RecJetMatched_JetBin1_Pt->Sumw2();
    h_RecJetNotMatched_JetBin1_Pt = fs->make<TH1F> ("RecJetNotMatched_JetBin1_Pt", "RecJetNotMatched_JetBin1_Pt", 1000, 0., 1000.);
    h_RecJetNotMatched_JetBin1_Pt->Sumw2();
    h_RecJetMatched_JetBin2_Pt = fs->make<TH1F> ("RecJetMatched_JetBin2_Pt", "RecJetMatched_JetBin2_Pt", 1000, 0., 1000.);
    h_RecJetMatched_JetBin2_Pt->Sumw2();
    h_RecJetNotMatched_JetBin2_Pt = fs->make<TH1F> ("RecJetNotMatched_JetBin2_Pt", "RecJetNotMatched_JetBin2_Pt", 1000, 0., 1000.);
    h_RecJetNotMatched_JetBin2_Pt->Sumw2();
    h_RecJetMatched_JetBin3_Pt = fs->make<TH1F> ("RecJetMatched_JetBin3_Pt", "RecJetMatched_JetBin3_Pt", 1000, 0., 1000.);
    h_RecJetMatched_JetBin3_Pt->Sumw2();
    h_RecJetNotMatched_JetBin3_Pt = fs->make<TH1F> ("RecJetNotMatched_JetBin3_Pt", "RecJetNotMatched_JetBin3_Pt", 1000, 0., 1000.);
    h_RecJetNotMatched_JetBin3_Pt->Sumw2();
    h_RecJetMatched_JetBin4_Pt = fs->make<TH1F> ("RecJetMatched_JetBin4_Pt", "RecJetMatched_JetBin4_Pt", 1000, 0., 1000.);
    h_RecJetMatched_JetBin4_Pt->Sumw2();
    h_RecJetNotMatched_JetBin4_Pt = fs->make<TH1F> ("RecJetNotMatched_JetBin4_Pt", "RecJetNotMatched_JetBin4_Pt", 1000, 0., 1000.);
    h_RecJetNotMatched_JetBin4_Pt->Sumw2();

    h_RecJetRes_Pt = fs->make<TH2F> ("RecJetRes_Pt", "RecJetRes_Pt", 100, 0., 1000., 100, 0., 3.);
    h_RecJetRes_Pt->Sumw2();
    h_RecJetRes_Eta = fs->make<TH2F> ("RecJetRes_Eta", "RecJetRes_Eta", 100, -5., 5., 100, 0., 3.);
    h_RecJetRes_Eta->Sumw2();
    h_RebJetRes_Pt = fs->make<TH2F> ("RebJetRes_Pt", "RebJetRes_Pt", 100, 0., 1000., 100, 0., 3.);
    h_RebJetRes_Pt->Sumw2();
    h_RebJetRes_Eta = fs->make<TH2F> ("RebJetRes_Eta", "RebJetRes_Eta", 100, -5., 5., 100, 0., 3.);
    h_RebJetRes_Eta->Sumw2();

    h_SmearedJetRes_Pt = fs->make<TH2F> ("SmearedJetRes_Pt", "SmearedJetRes_Pt", 100, 0., 1000., 100, 0., 3.);
    h_SmearedJetRes_Pt->Sumw2();
    h_SmearedJetRes_Eta = fs->make<TH2F> ("SmearedJetRes_Eta", "SmearedJetRes_Eta", 100, -5., 5., 100, 0., 3.);
    h_SmearedJetRes_Eta->Sumw2();

    h_HT_gen = fs->make<TH1F> ("HT_gen", "HT_gen", NbinsHT_, HTmin_, HTmax_);
    h_HT_gen->Sumw2();
    h_HT_rec = fs->make<TH1F> ("HT_rec", "HT_rec", NbinsHT_, HTmin_, HTmax_);
    h_HT_rec->Sumw2();
    h_HT_reb = fs->make<TH1F> ("HT_reb", "HT_reb", NbinsHT_, HTmin_, HTmax_);
    h_HT_reb->Sumw2();
    h_HT_smeared = fs->make<TH1F> ("HT_smeared", "HT_smeared", NbinsHT_, HTmin_, HTmax_);
    h_HT_smeared->Sumw2();

    h_HT_JetBin1_gen = fs->make<TH1F> ("HT_JetBin1_gen", "HT_JetBin1_gen", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin1_gen->Sumw2();
    h_HT_JetBin1_rec = fs->make<TH1F> ("HT_JetBin1_rec", "HT_JetBin1_rec", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin1_rec->Sumw2();
    h_HT_JetBin1_reb = fs->make<TH1F> ("HT_JetBin1_reb", "HT_JetBin1_reb", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin1_reb->Sumw2();
    h_HT_JetBin1_smeared = fs->make<TH1F> ("HT_JetBin1_smeared", "HT_JetBin1_smeared", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin1_smeared->Sumw2();

    h_HT_JetBin2_gen = fs->make<TH1F> ("HT_JetBin2_gen", "HT_JetBin2_gen", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin2_gen->Sumw2();
    h_HT_JetBin2_rec = fs->make<TH1F> ("HT_JetBin2_rec", "HT_JetBin2_rec", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin2_rec->Sumw2();
    h_HT_JetBin2_reb = fs->make<TH1F> ("HT_JetBin2_reb", "HT_JetBin2_reb", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin2_reb->Sumw2();
    h_HT_JetBin2_smeared = fs->make<TH1F> ("HT_JetBin2_smeared", "HT_JetBin2_smeared", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin2_smeared->Sumw2();

    h_HT_JetBin3_gen = fs->make<TH1F> ("HT_JetBin3_gen", "HT_JetBin3_gen", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin3_gen->Sumw2();
    h_HT_JetBin3_rec = fs->make<TH1F> ("HT_JetBin3_rec", "HT_JetBin3_rec", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin3_rec->Sumw2();
    h_HT_JetBin3_reb = fs->make<TH1F> ("HT_JetBin3_reb", "HT_JetBin3_reb", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin3_reb->Sumw2();
    h_HT_JetBin3_smeared = fs->make<TH1F> ("HT_JetBin3_smeared", "HT_JetBin3_smeared", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin3_smeared->Sumw2();

    h_HT_JetBin4_gen = fs->make<TH1F> ("HT_JetBin4_gen", "HT_JetBin4_gen", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin4_gen->Sumw2();
    h_HT_JetBin4_rec = fs->make<TH1F> ("HT_JetBin4_rec", "HT_JetBin4_rec", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin4_rec->Sumw2();
    h_HT_JetBin4_reb = fs->make<TH1F> ("HT_JetBin4_reb", "HT_JetBin4_reb", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin4_reb->Sumw2();
    h_HT_JetBin4_smeared = fs->make<TH1F> ("HT_JetBin4_smeared", "HT_JetBin4_smeared", NbinsHT_, HTmin_, HTmax_);
    h_HT_JetBin4_smeared->Sumw2();

    h_HTall_gen = fs->make<TH1F> ("HTall_gen", "HTall_gen", NbinsHT_, HTmin_, HTmax_);
    h_HTall_gen->Sumw2();
    h_HTall_rec = fs->make<TH1F> ("HTall_rec", "HTall_rec", NbinsHT_, HTmin_, HTmax_);
    h_HTall_rec->Sumw2();
    h_HTall_reb = fs->make<TH1F> ("HTall_reb", "HTall_reb", NbinsHT_, HTmin_, HTmax_);
    h_HTall_reb->Sumw2();
    h_HTall_smeared = fs->make<TH1F> ("HTall_smeared", "HTall_smeared", NbinsHT_, HTmin_, HTmax_);
    h_HTall_smeared->Sumw2();

    h_HThigh_gen = fs->make<TH1F> ("HThigh_gen", "HThigh_gen", NbinsHT_, HTmin_, HTmax_);
    h_HThigh_gen->Sumw2();
    h_HThigh_rec = fs->make<TH1F> ("HThigh_rec", "HThigh_rec", NbinsHT_, HTmin_, HTmax_);
    h_HThigh_rec->Sumw2();
    h_HThigh_reb = fs->make<TH1F> ("HThigh_reb", "HThigh_reb", NbinsHT_, HTmin_, HTmax_);
    h_HThigh_reb->Sumw2();
    h_HThigh_smeared = fs->make<TH1F> ("HThigh_smeared", "HThigh_smeared", NbinsHT_, HTmin_, HTmax_);
    h_HThigh_smeared->Sumw2();

    h_MHTall_gen = fs->make<TH1F> ("MHTall_gen", "MHTall_gen", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHTall_gen->Sumw2();
    h_MHTall_rec = fs->make<TH1F> ("MHTall_rec", "MHTall_rec", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHTall_rec->Sumw2();
    h_MHTall_reb = fs->make<TH1F> ("MHTall_reb", "MHTall_reb", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHTall_reb->Sumw2();
    h_MHTall_smeared = fs->make<TH1F> ("MHTall_smeared", "MHTall_smeared", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHTall_smeared->Sumw2();

    h_MHThigh_gen = fs->make<TH1F> ("MHThigh_gen", "MHThigh_gen", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHThigh_gen->Sumw2();
    h_MHThigh_rec = fs->make<TH1F> ("MHThigh_rec", "MHThigh_rec", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHThigh_rec->Sumw2();
    h_MHThigh_reb = fs->make<TH1F> ("MHThigh_reb", "MHThigh_reb", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHThigh_reb->Sumw2();
    h_MHThigh_smeared = fs->make<TH1F> ("MHThigh_smeared", "MHThigh_smeared", NbinsMHT_, MHTmin_, MHTmax_);
    h_MHThigh_smeared->Sumw2();

    h_deltaPhiJet1Jet2_gen = fs->make<TH1F> ("deltaPhiJet1Jet2_gen", "deltaPhiJet1Jet2_gen", 50, 0, TMath::Pi());
    h_deltaPhiJet1Jet2_gen->Sumw2();
    h_deltaPhiJet1Jet2_rec = fs->make<TH1F> ("deltaPhiJet1Jet2_rec", "deltaPhiJet1Jet2_rec", 50, 0, TMath::Pi());
    h_deltaPhiJet1Jet2_rec->Sumw2();
    h_deltaPhiJet1Jet2_reb = fs->make<TH1F> ("deltaPhiJet1Jet2_reb", "deltaPhiJet1Jet2_reb", 50, 0, TMath::Pi());
    h_deltaPhiJet1Jet2_reb->Sumw2();
    h_deltaPhiJet1Jet2_smeared = fs->make<TH1F> ("deltaPhiJet1Jet2_smeared", "deltaPhiJet1Jet2_smeared", 50, 0, TMath::Pi());
    h_deltaPhiJet1Jet2_smeared->Sumw2();

    h_fitProb = fs->make<TH1F> ("h_fitProb", "h_fitProb", 100, 0., 1.);
    h_fitProb->Sumw2();
    h_weight = fs->make<TH1F> ("h_weight", "h_weight", 70, -1., 6.);
    h_weight->Sumw2();
    h_weightedWeight = fs->make<TH1F> ("h_weightedWeight", "h_weightedWeight", 70, -1., 6.);
    h_weightedWeight->Sumw2();


  //// get rebalance correction histo
  if( useRebalanceCorrectionFactors_ ) {
    TFile *f_rebCorr = new TFile(RebalanceCorrectionFile_.c_str(), "READ", "", 0);
    h_RebCorrectionFactor = (TH1F*) f_rebCorr->FindObjectAny("RebCorrection_vsReco_px");
    h_RebCorrectionFactor_b = (TH1F*) f_rebCorr->FindObjectAny("RebCorrection_vsReco_b_px");
    h_2DRebCorrectionFactor = (TH2F*) f_rebCorr->FindObjectAny("RebCorrection_vsReco");
    h_2DRebCorrectionFactor_b = (TH2F*) f_rebCorr->FindObjectAny("RebCorrection_vsReco_b");
    //// Do projections for each x-bin
    for (int ii = 1; ii <= h_2DRebCorrectionFactor_b->GetXaxis()->GetNbins(); ++ii){
      TH1D* tmp_py = new TH1D(*h_2DRebCorrectionFactor_b->ProjectionY("py", ii, ii));
      h_2DRebCorrectionFactor_b_py.push_back(tmp_py);
    }
    for (int ii = 1; ii <= h_2DRebCorrectionFactor->GetXaxis()->GetNbins(); ++ii){
      TH1D* tmp_py = new TH1D(*h_2DRebCorrectionFactor->ProjectionY("py", ii, ii));
      h_2DRebCorrectionFactor_py.push_back(tmp_py);
    }
  }


  // define output tree
  PredictionTree = fs->make<TTree> ("QCDPrediction", "QCDPrediction", 0);
  PredictionTree->SetAutoSave(10000000000);
  PredictionTree->SetAutoFlush(1000000);

  // set branches for output tree
  PredictionTree->Branch("NVtx", &vtxN);
  PredictionTree->Branch("Ntries",&Ntries_pred);
  PredictionTree->Branch("NJets",&Njets_pred);
  PredictionTree->Branch("BTags",&BTags_pred);
  PredictionTree->Branch("Weight",&weight);
  PredictionTree->Branch("HT_seed", &HT_seed);
  PredictionTree->Branch("HT", &HT_pred);
  PredictionTree->Branch("MHT", &MHT_pred);
  PredictionTree->Branch("Jet1Pt", &Jet1Pt_pred);
  PredictionTree->Branch("Jet2Pt", &Jet2Pt_pred);
  PredictionTree->Branch("Jet3Pt", &Jet3Pt_pred);
  PredictionTree->Branch("Jet4Pt", &Jet4Pt_pred);
  PredictionTree->Branch("Jet1Eta", &Jet1Eta_pred);
  PredictionTree->Branch("Jet2Eta", &Jet2Eta_pred);
  PredictionTree->Branch("Jet3Eta", &Jet3Eta_pred);
  PredictionTree->Branch("Jet4Eta", &Jet4Eta_pred);
  PredictionTree->Branch("DeltaPhi1", &DeltaPhi1_pred);
  PredictionTree->Branch("DeltaPhi2", &DeltaPhi2_pred);
  PredictionTree->Branch("DeltaPhi3", &DeltaPhi3_pred);
  PredictionTree->Branch("DeltaPhi4", &DeltaPhi4_pred);
  PredictionTree->Branch("HTRatio", &HTRatio_pred);
  PredictionTree->Branch("TrigEffWeightNom", &TrigEffWeightNom);
  PredictionTree->Branch("TrigEffWeightUp", &TrigEffWeightUp);
  PredictionTree->Branch("TrigEffWeightDown", &TrigEffWeightDown);
  cout << "Set branches for output tree." << endl;

}
//--------------------------------------------------------------------------



// ------------ method called once each job just after ending the event loop  ------------
void QCDBkgRS::endJob()
{

}



//--------------------------------------------------------------------------
// rebalance the events using a kinematic fit and transverse momentum balance, using UsefulJets
//
/*
bool QCDBkgRS::RebalanceJets_KinFitter(std::vector<UsefulJet*> UJets_rec, std::vector<UsefulJet> &UJets_reb) {

  bool result = true;

  //// Interface to KinFitter
  TKinFitter* myFit = new TKinFitter();

  std::vector<TLorentzVector*> lvec_m;

  std::vector<TMatrixD*> covMat_m;

  std::vector<TFitParticleEtEtaPhi*> fitted;
  std::vector<TFitParticleEtEtaPhi*> measured;
  std::map<int, const UsefulJet*> JetMap;
  double dPx = 0;
  double dPy = 0;
  double HTreco = 0;
  double HTreb = 0;
  double MHTx_low = 0;
  double MHTy_low = 0;

  std::vector<double> CSVs_reb;

  //// Fill measured particles to vector
  int i = 0;
  for ( std::vector<UsefulJet *>::iterator it = UJets_rec.begin(); it != UJets_rec.end(); ++it) {
    //if (it->pt() < rebalancedJetPt_ || abs(it->pt()) > 3.0) {
    //if (it->pt() < rebalancedJetPt_ || it->chargedEmEnergyFraction()>0.9 || it->muonEnergyFraction()>0.9) {
    // Typically, this rebalancedJetPt is min. 10 GeV:
    if ((*it)->Pt() < rebalancedJetPt_) {
      if (rebalanceMode_ == "MHTall") {
	MHTx_low -= (*it)->Px();
	MHTy_low -= (*it)->Py();
	TLorentzVector lvi;
	lvi.SetPxPyPzE((*it)->Px(), (*it)->Py(), (*it)->Pz(), (*it)->E());
	double csv = (*it)->Csv();
	UJets_reb.emplace_back(lvi,csv);
      }
    } else {

      JetMap[i] = (*it);

      // The particles before fitting
      double tmppx, tmppy, tmppz, tmpe;

      CSVs_reb.push_back((*it)->Csv());

      if( useRebalanceCorrectionFactors_ ) {
	bool btag = ((*it)->Csv() > btagCut_);
	tmppx = (*it)->Px()/GetRebalanceCorrection( (*it)->Pt(), btag );
	tmppy = (*it)->Py()/GetRebalanceCorrection( (*it)->Pt(), btag );
	tmppz = (*it)->Pz()/GetRebalanceCorrection( (*it)->Pt(), btag );
	tmpe = (*it)->E()/GetRebalanceCorrection( (*it)->Pt(), btag );
      }
      else {
	tmppx = (*it)->Px();
	tmppy = (*it)->Py();
	tmppz = (*it)->Pz();
	tmpe = (*it)->E();
      }

      TLorentzVector* lv = new TLorentzVector(tmppx, tmppy, tmppz, tmpe);
      lvec_m.push_back(lv);
      TMatrixD* cM = new TMatrixD(3, 3);

      if(useEnergyTemplateBinning_){
	(*cM)(0, 0) = JetResolution_Pt2((*it)->E(), (*it)->Eta(), i);
	if(useEtaPhiSmearing_){
	  (*cM)(1, 1) = pow(JetResolution_Eta((*it)->E(), (*it)->Eta(), 0, 0), 2);
	  (*cM)(2, 2) = pow(JetResolution_Phi((*it)->E(), (*it)->Eta(), 0, 0), 2);
	} else {
	  (*cM)(1, 1) = 0.000001;
	  (*cM)(2, 2) = 0.000001;
	}
      }else{
	(*cM)(0, 0) = JetResolution_Pt2((*it)->Pt(), (*it)->Eta(), i);
	if(useEtaPhiSmearing_){
	  (*cM)(1, 1) = pow(JetResolution_Eta((*it)->Pt(), (*it)->Eta(), 0, 0), 2);
	  (*cM)(2, 2) = pow(JetResolution_Phi((*it)->Pt(), (*it)->Eta(), 0, 0), 2);
	} else {
	  (*cM)(1, 1) = 0.000001;
	  (*cM)(2, 2) = 0.000001;
	}
      }
      covMat_m.push_back(cM);
      char name[10];
      sprintf(name, "jet%i", i);
      TFitParticleEtEtaPhi* jet1 = new TFitParticleEtEtaPhi(name, name, lvec_m.back(), covMat_m.back());
      measured.push_back(jet1);
      TFitParticleEtEtaPhi* jet2 = new TFitParticleEtEtaPhi(name, name, lvec_m.back(), covMat_m.back());
      fitted.push_back(jet2);
      myFit->addMeasParticle(fitted.back());
      ++i;
    }
  }

  //// Add momentum constraints
  double MET_constraint_x = 0.;
  double MET_constraint_y = 0.;
  if (rebalanceMode_ == "MHTall") {
    //// rebalance MHT of all jets
    MET_constraint_x = MHTx_low;
    MET_constraint_y = MHTy_low;
  } else if (rebalanceMode_ == "MHThigh") {
    //// rebalance MHT of fitted jets
    MET_constraint_x = 0.;
    MET_constraint_y = 0.;
  } else {
    //// default: rebalance MHT of fitted jets
    MET_constraint_x = 0.;
    MET_constraint_y = 0.;
  }
  TFitConstraintEp* momentumConstr1 = new TFitConstraintEp("px", "px", 0, TFitConstraintEp::pX, MET_constraint_x);
  TFitConstraintEp* momentumConstr2 = new TFitConstraintEp("py", "py", 0, TFitConstraintEp::pY, MET_constraint_y);
  for (unsigned int i = 0; i < fitted.size(); ++i) {
    momentumConstr1->addParticle(fitted.at(i));
    momentumConstr2->addParticle(fitted.at(i));
  }
  myFit->addConstraint(momentumConstr1);
  myFit->addConstraint(momentumConstr2);

  //// Set fit parameters
  myFit->setVerbosity(0);
  myFit->setMaxNbIter(100);
  myFit->setMaxF(0.01 * 2);
  myFit->setMaxDeltaS(1.e-3);
  myFit->fit();

  //cout << "KinFitter: " << myFit->getStatus() << endl;
  int status = myFit->getStatus();

  double chi2 = 0;
  //double F = 0;
  double prob = 0;
  //if (status == 0 || status == 1) {
  if (status == 0) {
    chi2 = myFit->getS();
    //F = myFit->getF();
    int dof = myFit->getNDF();
    prob = TMath::Prob(chi2, dof);
    //if (prob < 1.e-8) result = false;
    //    if (prob < 0.01) result = false;
    //cout << "chi2, prop, F = " << chi2 << " " << prob << " " << F << endl;
  } else {
    chi2 = 99999;
    prob = 0;
    //F = 99999;
    result = false;
    //cout << "chi2, prop, F = " << chi2 << " " << prob << " " << F << endl;
  }
  if (controlPlots_)
    h_fitProb->Fill(prob);

  //// Get the output of KinFitter
  for (unsigned int i = 0; i < measured.size(); ++i) {
    // create new rebalanced Jet
    TLorentzVector newP4;
    newP4.SetPxPyPzE(	fitted.at(i)->getCurr4Vec()->Px(), fitted.at(i)->getCurr4Vec()->Py(),
			fitted.at(i)->getCurr4Vec()->Pz(), fitted.at(i)->getCurr4Vec()->E());
    UsefulJet rebalancedJet(newP4, CSVs_reb.at(i));
    HTreco += rebalancedJet.Pt();
    //cout << "RECO: " << i << "th: pt = " << rebalancedJet.pt() << " phi = " << rebalancedJet.phi() << endl;
    //cout << "REB: " << i << "th: pt = " << rebalancedJet.pt() << " phi = " << rebalancedJet.phi() << endl;

    UJets_reb.push_back(rebalancedJet);
    dPx -= newP4.Px() - measured.at(i)->getCurr4Vec()->Px();
    dPy -= newP4.Py() - measured.at(i)->getCurr4Vec()->Py();
    HTreb += rebalancedJet.Pt();
  }
  //cout << "HT reco = " << HTreco << endl;
  //cout << "HT reb  = " << HTreb << endl;

  delete myFit;
  for (unsigned int i = 0; i < measured.size(); ++i) {
    delete lvec_m.at(i);
    delete covMat_m.at(i);
    delete measured.at(i);
    delete fitted.at(i);
  }
  delete momentumConstr1;
  delete momentumConstr2;

  return result;
}
*/

//define this as a plug-in
DEFINE_FWK_MODULE(QCDBkgRS);
