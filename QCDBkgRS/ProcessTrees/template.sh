#!/bin/bash
# cms software setup
export SCRAM_ARCH=slc6_amd64_gcc530
echo $PWD
ls
scram project CMSSW_8_0_22
cd CMSSW_8_0_22/src
echo $SCRAM_ARCH
eval `scramv1 runtime -sh`
cd ${_CONDOR_SCRATCH_DIR}
echo $PWD
ls
python ANALYZER RUNARGS 

#for filename in littletree*.root; do
#    xrdcp $filename root://cmseos.fnal.gov//store/user/sbein/MustFindSusyTrees/
#    rm $filename
#done

