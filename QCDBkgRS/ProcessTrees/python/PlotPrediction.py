from ROOT import *
from glob import glob
gStyle.SetOptStat(0)
from utils import *
from selectionstrings import *
import sys
gROOT.SetBatch(1)
ROOT.v5.TFormula.SetMaxima(10000)
datamc = "Data"
lumi = '123'
Version = '2016'

nsmear = 500 # set to 100 not 250 for now to match what was done in DESY Vormwald (leave 1/2.5 in 0_*.py)
#the test c series (and b):
#nsmear = 100#Test100
nsmear = 500 #test500


try: systematic = sys.argv[1]
except: systematic = 'Nominal'
try: TriggerEff = sys.argv[2]
except: TriggerEff = 'Nom'#, 'Nom', 'Up', 'Down'
try: episode = sys.argv[3]
except: episode = 'A'
try: experiment = sys.argv[4]
except: experiment = 'all'


if experiment =='all': 
    experimentSelection = ''
    expnumber, numberexp = '1', '1'
else: 
    expnumber, numberexp = experiment.split('of')
    experimentSelection = ' && (int(10*HT_seed)%'+numberexp+')+1=='+expnumber
    expnumber, numberexp = str(float(expnumber)), str(float(numberexp))


print 'number of experiments:', numberexp
print 'this experiment is number', expnumber
print 'experiment selection is', experimentSelection


if Version=='Ichep': SearchBinsA, SearchBinsB, SearchBinsC = SearchBinsICHEPa, SearchBinsICHEPb, SearchBinsICHEPc
if Version=='2016': SearchBinsA, SearchBinsB, SearchBinsC = SearchBins2016a, SearchBins2016b, SearchBins2016c	
print Version, systematic

redirect = 'root://cmsxrootd.fnal.gov/'
nbins = {'Ichep':160, '2016':174}

cPrediction = TChain('QCDfromSmearing/QCDPrediction')

filenamelistfilenamelist = glob('filelists/filelist'+systematic.replace('Ldp','Nominal')+'*.txt')
for filenamelist in filenamelistfilenamelist:
    filelistfile =open(filenamelist)
    filelines = filelistfile.readlines()
    filelistfile.close()
    print filenamelist
    ifile = 0
    for line in filelines:
        #if not '0002' in line: continue
        filehead = line.strip().replace('/eos/uscms','').replace('/pnfs/desy.de/cms/tier2','')
        filehead = redirect+filehead
        if ifile<3: print redirect+filehead
        if ifile==3: print '...'
        ifile+=1
        cPrediction.Add(filehead)
cPrediction.Show(0)

print 'nentries(prediction) =', cPrediction.GetEntries()

deltaPhiDef =  '(abs(DeltaPhi1)>0.5 && abs(DeltaPhi2)>0.5 && abs(DeltaPhi3)>0.3 && abs(DeltaPhi4)>0.3)'
invDeltaPhiDef = ' (abs(DeltaPhi1)<0.5 || abs(DeltaPhi2)<0.5 || abs(DeltaPhi3)<0.3 || abs(DeltaPhi4)<0.3)'
if 'Ldp' in systematic: dphicut = invDeltaPhiDef
else: dphicut = deltaPhiDef

cuts = '('+dphicut + experimentSelection+')'

if episode=='A': obsBins = SearchBinsA+'-1>>hadc('+str(nbins[Version])+',0,'+str(nbins[Version])+')'
if episode=='B': obsBins = SearchBinsB+'-1>>hadc('+str(nbins[Version])+',0,'+str(nbins[Version])+')'
if episode=='C': obsBins = SearchBinsC+'-1>>hadc('+str(nbins[Version])+',0,'+str(nbins[Version])+')'

print 'drawing, multipliying by', numberexp
cPrediction.Draw(obsBins,numberexp+'*TrigEffWeight'+TriggerEff+'*Weight/'+str(nsmear)+'*('+cuts+')')
hPrediction = cPrediction.GetHistogram().Clone('hPrediction')

tl.DrawLatex(0.2,0.7,cuts)

RUNARGS = ''
for arg in sys.argv[1:]:
    RUNARGS+=arg
    
newfile = TFile('Prediction'+RUNARGS+'.root','recreate')
#c1.Write('TreeClosure_BinByBin2016')
hPrediction.Write('hPrediction')
print 'created', newfile.GetName()
newfile.Close()








