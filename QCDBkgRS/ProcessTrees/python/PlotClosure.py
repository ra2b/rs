from ROOT import *
gStyle.SetOptStat(0)
from utils import *
from selectionstrings import *
import sys
gROOT.SetBatch(1)
ROOT.v5.TFormula.SetMaxima(10000)

datamc = "MC"
if datamc =='MC': ra2bweight = 'Weight'
else: ra2bweight = 'PrescaleWeightHt'
lumi = 1.0


try: filelistfilename = sys.argv[1]
except: filelistfilename = 'filelistNominalQcd_I.txt'
filelistfile = open(filelistfilename)
filelines = filelistfile.readlines()
filelistfile.close()

newfile = TFile('Closure/closure'+filelistfilename.replace('filelists/filelist','').replace('.txt','')+'.root','recreate')

cPrediction = TChain('QCDfromSmearing/QCDPrediction')
cExpectation = TChain('RA2TreeMaker/PreSelection')

nsmear = 5
rebin = {'MHT':10, 'BTags':1, 'NJets':1, 'HT':10, 'Jet1Pt':10, 'abs(DeltaPhi1)':10, 'abs(DeltaPhi2)':10, 'abs(DeltaPhi3)':10}

for line in filelines:
    filehead = line.strip().replace('/eos/uscms/','').replace('/store/user/sbein/','')
    cPrediction.Add('root://cmseos.fnal.gov//store/user/sbein/'+filehead)
    cExpectation.Add('root://cmseos.fnal.gov//store/user/sbein/'+filehead)
    #cPrediction.Add(filehead)
    #cExpectation.Add(filehead)


print 'nentries(prediction) =', cPrediction.GetEntries()
print 'nentries(expectation) =', cExpectation.GetEntries()

deltaPhiCut =  '&& (abs(DeltaPhi1)>0.5 && abs(DeltaPhi2)>0.5 && abs(DeltaPhi3)>0.3 && abs(DeltaPhi4)>0.3)'
invDeltaPhiCut = ' && (abs(DeltaPhi1)<0.5 || abs(DeltaPhi2)<0.5 || abs(DeltaPhi3)<0.3 || abs(DeltaPhi4)<0.3)'
dphicut = ''

obsBinsA, selectionBinsA = SearchBins2016a+'>>hadc(174,1,175)', '(HT>200) '+dphicut
obsBinsB, selectionBinsB = SearchBins2016b+'>>hadc(174,1,175)', '(HT>200) '+dphicut
obsBinsC, selectionBinsC = SearchBins2016c+'>>hadc(174,1,175)', '(HT>200) '+dphicut
cPrediction.Draw(obsBinsA,str(lumi*1000)+'*Weight/'+str(nsmear)+'*('+selectionBinsA+')')
hPredictionA = cPrediction.GetHistogram().Clone('hPredictionA')
cPrediction.Draw(obsBinsB,str(lumi*1000)+'*Weight/'+str(nsmear)+'*('+selectionBinsB+')')
hPredictionB = cPrediction.GetHistogram().Clone('hPredictionB')
cPrediction.Draw(obsBinsC,str(lumi*1000)+'*Weight/'+str(nsmear)+'*('+selectionBinsC+')')
hPredictionC = cPrediction.GetHistogram().Clone('hPredictionC')
hPrediction = hPredictionA.Clone('hPrediction')
hPrediction.Add(hPredictionB)
hPrediction.Add(hPredictionC)
histoStyler(hPrediction, kTeal+2)
cleaning = '(BadChargedCandidateFilter && BadPFMuonFilter && EcalDeadCellTriggerPrimitiveFilter && eeBadScFilter && globalTightHalo2016Filter && GoodLeptons==0 && HBHEIsoNoiseFilter && HBHENoiseFilter && PFCaloMETRatio && NoMuonInt && NVtx!=0 && JetID)'
cExpectation.Draw(obsBinsA,str(lumi*1000)+'*'+ra2bweight+'*('+selectionBinsA+')*'+cleaning, 'hist')
hExpectationA = cExpectation.GetHistogram().Clone('hExpectationA')
cExpectation.Draw(obsBinsB,str(lumi*1000)+'*'+ra2bweight+'*('+selectionBinsA+')*'+cleaning, 'hist')
hExpectationB = cExpectation.GetHistogram().Clone('hExpectationB')
cExpectation.Draw(obsBinsC,str(lumi*1000)+'*'+ra2bweight+'*('+selectionBinsA+')*'+cleaning, 'hist')
hExpectationC = cExpectation.GetHistogram().Clone('hExpectationC')
hExpectation = hExpectationA.Clone('hExpectation')
hExpectation.Add(hExpectationB)
hExpectation.Add(hExpectationC)
histoStyler(hExpectation, kBlack)
legend = mklegend()
hFracDiff = FabDraw(c1,legend,hExpectation,hPrediction,datamc,lumi,False)
tl.DrawLatex(0.2,0.7,selectionBinsA)
c1.Update()
newfile.cd()
c1.Write('TreeClosure_BinByBin2016')
hPrediction.Write('hPrediction')

ObsAndSelectionList1d = [['MHT>>hadc(20,0,1000)', 'HT>200 && MHT>100 && NJets>1'],\
#['MHT>>hadc(20,250,1250)', 'HT>200 && MHT>240 && NJets>1 && BTags==0'],\
#['MHT>>hadc(20,250,1250)', 'HT>200 && MHT>240 && NJets>1 && BTags==1'],\
#['MHT>>hadc(20,250,1250)', 'HT>200 && MHT>240 && NJets>1 && BTags>4'],\
#['MHT>>hadc(20,250,1250)', 'HT>200 && MHT>240 && NJets>1 && BTags>2'],\
#['BTags>>hadc(6,0,6)', 'HT>200 && MHT>240 && NJets>1'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>300 && NJets>1'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>400 && NJets>1'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>600 && NJets>1'],\
#['BTags>>hadc(6,0,6)', 'HT>200 && MHT>240 && NJets>3'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>300 && NJets>3'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>400 && NJets>3'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>600 && NJets>3'],\
#['BTags>>hadc(6,0,6)', 'HT>200 && MHT>240 && NJets>5'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>300 && NJets>5'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>400 && NJets>5'],\
                         ['BTags>>hadc(6,0,6)', 'HT>200 && MHT>600 && NJets>5'],\
#s['NJets>>hadc(10,0,12)', 'HT>200 && MHT>240 && NJets>1'],\
                         ['NJets>>hadc(10,2,12)', 'HT>200 && MHT>300 && NJets>1'],\
                         ['NJets>>hadc(10,2,12)', 'HT>200 && MHT>400 && NJets>1'],\
                         ['NJets>>hadc(10,2,12)', 'HT>200 && MHT>600 && NJets>1'],\
                         ['abs(DeltaPhi1)>>hadc(20,0,3.142)', 'HT>200 && MHT>300 && NJets>1'],\
                         ['abs(DeltaPhi1)>>hadc(20,0,3.142)', 'HT>200 && MHT>300 && NJets>1 && BTags==1'],\
                         ['abs(DeltaPhi1)>>hadc(20,0,3.142)', 'HT>200 && MHT>300 && NJets>1 && BTags==2'],\
                         ['abs(DeltaPhi2)>>hadc(20,0,3.142)', 'HT>200 && MHT>300 && NJets>1'],\
                         ['abs(DeltaPhi3)>>hadc(20,0,3.142)', 'HT>200 && MHT>300 && NJets>1'],\
                         ['abs(DeltaPhi4)>>hadc(20,0,3.142)', 'HT>200 && MHT>300 && NJets>1'],\
                         ['HT>>hadc(20,0,2000)', 'HT>200 && MHT>300 && NJets>1'],\
                         ]

ObsAndSelectionList2d = [['MHT:NJets', 'HT>300 && NJets>1 && MHT>100'],\
                         ['MHT:BTags', 'HT>300 && NJets>1 && MHT>100']]


                         
for ObsAndSelection in ObsAndSelectionList1d:
    obs, selection = ObsAndSelection
    cPrediction.Draw(obs,str(lumi*1000)+'*Weight/'+str(nsmear)+'*('+selection+')')
    hPrediction = cPrediction.GetHistogram().Clone('hPrediction')
    try: rebin_ = rebin[obs]
    except: rebin_ = 1
    hPrediction.Rebin(rebin_)
    histoStyler(hPrediction, kTeal+2)
    cExpectation.Draw(obs,str(lumi*1000)+'*'+ra2bweight+'*('+selection+')*(BadChargedCandidateFilter && BadPFMuonFilter && EcalDeadCellTriggerPrimitiveFilter && eeBadScFilter && globalTightHalo2016Filter && GoodLeptons==0 && HBHEIsoNoiseFilter && HBHENoiseFilter && PFCaloMETRatio && NoMuonInt && NVtx!=0 && JetID)', 'hist')
    hExpectation = cExpectation.GetHistogram().Clone('hExpectation')
    hExpectation.Rebin(rebin_)
    histoStyler(hExpectation, kBlack)

    
    c1 = mkcanvas('c1')
    legend = mklegend()
    hFracDiff = FabDraw(c1,legend,hExpectation,hPrediction,datamc,lumi,False)
    tl.DrawLatex(0.2,0.7,selection)


    c1.Update()
    newfile.cd()
    hPrediction.Write('hPrediction'+obs+'_'+selection.replace(' ','').replace('&','-'))
    hExpectation.Write('hExpectation'+obs+'_'+selection.replace(' ','').replace('&','-'))
    c1.Write('TreeClosure'+obs+'_'+selection.replace(' ','').replace('&','-'))
    #pause()
    #break



exit(0)


    
for ObsAndSelection in ObsAndSelectionList2d:
    obs, selection = ObsAndSelection
    c2 = mkcanvas('c2')
    c2.SetRightMargin(0.15)
    cPrediction.Draw(obs+'>>hadc(5,0,5,5,100,1100)','Weight/'+str(nsmear)+'*('+selection+')')
    hPrediction = cPrediction.GetHistogram().Clone('hPrediction')
    hPrediction.Sumw2()
    cExpectation.Draw(obs+'>>hadc(5,0,5,5,100,1100)',ra2bweight+'*('+selection+')*(BadChargedCandidateFilter && BadPFMuonFilter && EcalDeadCellTriggerPrimitiveFilter && eeBadScFilter && globalTightHalo2016Filter && GoodLeptons==0 && HBHEIsoNoiseFilter && HBHENoiseFilter && PFCaloMETRatio && NoMuonInt && NVtx!=0 && JetID)','text')
    hExpectation = cExpectation.GetHistogram().Clone('hExpectation')
    hExpectation.Sumw2()
    hFracDiff = hPrediction.Clone('hFracDiff')
    hFracDiff.GetZaxis().SetRangeUser(-2,2)
    hFracDiff.Add(hExpectation,-1)
    hFracDiff.Divide(hExpectation)
    hFracDiff.SetTitle('frac. diff')
    hFracDiff.Draw('colz text E')
    c2.Update()
    pause()



'''
cExpectation.Draw('BTags:MHT>>hadc(6,0,600,5,0,5)','FitSucceed')
hPass = cExpectation.GetHistogram().Clone('hPass')
cExpectation.Draw('BTags:MHT>>hadc(6,0,600,5,0,5)','')
hTot = cExpectation.GetHistogram().Clone('hTot')
hPass.Divide(hTot)
hPass.Draw('colz text e')
c1.Update()
pause()
'''

'''
if ((*NoMuonHandle_) == 0 ||
	(*HBHENoiseHandle_) == 0 ||
	(*HBHEIsoNoiseHandle_) == 0 ||
	(*eeBadScHandle_) == 0 ||
	(*ecalDeadCellHandle_) == 0 ||
	!(*jetIDHandle_) ||
	(*NVtxHandle_) == 0 ||
	(*FilterPFCaloRatio)
'''

