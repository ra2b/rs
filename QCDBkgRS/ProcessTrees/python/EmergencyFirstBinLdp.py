from ROOT import *
from glob import glob
gStyle.SetOptStat(0)
from utils import *
from selectionstrings import *
import sys
gROOT.SetBatch(1)
ROOT.v5.TFormula.SetMaxima(10000)
datamc = "Data"
lumi = 0.001


Version = 'Ichep'
Version = '2016'
try: systematic = sys.argv[1]
except: systematic = 'Nominal'
try: kinvar = sys.argv[2]
except: kinvar = 'NJets'
try: CR = sys.argv[3]
except: CR = 'bin1'


TriggerEff = 'Nom'


print Version, systematic

redirect = {'Ldp':'root://cmseos.fnal.gov//store/user/sbein/', \
            'CoreDown':'root://cmsxrootd.fnal.gov//store/user/jsonneve/', \
            'CoreUp':'root://cmsxrootd.fnal.gov//store/user/jsonneve/', \
            'TailUp':'root://cmsxrootd.fnal.gov//store/user/mniedzie/', \
            'TailDown':'root://cmsxrootd.fnal.gov//store/user/mniedzie/', \
            'Nominal':'root://cmseos.fnal.gov//store/user/sbein/'}
nbins = {'Ichep':160, '2016':174}

cPrediction = TChain('QCDfromSmearing/QCDPrediction')
nsmear = 250
newfile = TFile('LowMht/Prediction'+systematic+Version+kinvar+'CR'+CR+'.root','recreate')

filenamelistfilenamelist = glob('filelists/filelist'+systematic.replace('Ldp','Nominal')+'*.txt')
for filenamelist in filenamelistfilenamelist:
    filelistfile =open(filenamelist)
    filelines = filelistfile.readlines()
    filelistfile.close()
    ifile = 0
    for line in filelines:
        #if not '0002' in line: continue
        filehead = line.strip().replace('/eos/uscms/','').replace('store/user/sbein/','').replace('/store/user/jsonneve/','').replace('/pnfs/desy.de/cms/tier2/store/user/mniedzie/','')
        if ifile<1: print redirect[systematic]+filehead
        if ifile==1: print '...'
        ifile+=1
        cPrediction.Add(redirect[systematic]+filehead)

cPrediction.Show(0)

print 'nentries(prediction) =', cPrediction.GetEntries()

deltaPhiDef =  '(abs(DeltaPhi1)>0.5 && abs(DeltaPhi2)>0.5 && abs(DeltaPhi3)>0.3 && abs(DeltaPhi4)>0.3)'
invDeltaPhiDef = ' (abs(DeltaPhi1)<0.5 || abs(DeltaPhi2)<0.5 || abs(DeltaPhi3)<0.3 || abs(DeltaPhi4)<0.3)'
if systematic=='Ldp': dphicut = invDeltaPhiDef
else: dphicut = deltaPhiDef

CR1Def = '((300<=HT) * (HT<500) * (250<=MHT) * (MHT<300))'
CR2Def = '((500<=HT) * (HT<1000) * (250<=MHT) * (MHT<300))'
CR3Def = '((1000<=HT) * (250<=MHT) * (MHT<300))'
Bin1Def = '((300<=HT) * (HT<500) * (300<=MHT) * (MHT<350) * (NJets==2) * (BTags==0))'

if CR=='c1':
    cuts = dphicut + ' && NVtx>0 && ' + CR1Def
    obs, selection = kinvar+'>>hadc(12,0,12)', cuts
    cPrediction.Draw(obs,str(lumi*1000)+'*TrigEffWeight'+TriggerEff+'*Weight/'+str(nsmear)+'*('+selection+')')
    hPredictionNJetsC1 = cPrediction.GetHistogram().Clone('hPrediction'+kinvar+'C1')
    hPredictionNJetsC1.Write()

if CR=='c2':
    cuts = dphicut + ' && NVtx>0 && ' + CR2Def
    obs, selection = kinvar+'>>hadc(12,0,12)', cuts
    cPrediction.Draw(obs,str(lumi*1000)+'*TrigEffWeight'+TriggerEff+'*Weight/'+str(nsmear)+'*('+selection+')')
    hPredictionNJetsC2 = cPrediction.GetHistogram().Clone('hPrediction'+kinvar+'C2')
    hPredictionNJetsC2.Write()

if CR=='c3':
    cuts = dphicut + ' && NVtx>0 && ' + CR3Def
    obs, selection = kinvar+'>>hadc(12,0,12)', cuts
    cPrediction.Draw(obs,str(lumi*1000)+'*TrigEffWeight'+TriggerEff+'*Weight/'+str(nsmear)+'*('+selection+')')
    hPredictionNJetsC3 = cPrediction.GetHistogram().Clone('hPrediction'+kinvar+'C3')
    hPredictionNJetsC3.Write()

if CR=='bin1':
    cuts = dphicut + ' && NVtx>0 && ' + Bin1Def
    obs, selection = kinvar+'>>hadc(12,0,12)', cuts
    cPrediction.Draw(obs,str(lumi*1000)+'*TrigEffWeight'+TriggerEff+'*Weight/'+str(nsmear)+'*('+selection+')')
    hPredictionNJetsBin1 = cPrediction.GetHistogram().Clone('hPrediction'+kinvar+'Bin1')
    hPredictionNJetsBin1.Write()


print 'created file', newfile.GetName()
newfile.Close()
