import os, sys

dirStem=os.getcwd()
print 'DIRSTEM=',dirStem



RUNARGS = ' '
for arg in sys.argv[1:]:
    RUNARGS+=arg+' '

    
analyzer = sys.argv[0].replace('whip_','').replace('python/','').replace('jobs/','').replace('.py',RUNARGS.replace(' ','')+'.py')
try:
    jobnumber = str(int(sys.argv[0][sys.argv[0].rfind('_')+1:sys.argv[0].find('.py')]))
except: jobnumber = ''
print 'number, analyzer=', jobnumber, ',', analyzer

if '_' in analyzer: 
    oneofmany=True
else:
    #analyzer = analyzer.replace(sys.argv[1]+sys.argv[2],'')
    os.system('cp python/'+analyzer.replace(RUNARGS.replace(' ',''),'')+' jobs/'+analyzer)
    os.system('cp python/utils.py jobs/')
    os.system('cp python/selectionstrings.py jobs/')
    print 'setting one of many', False
    oneofmany  = False
if oneofmany:
    scriptdir = 'jobs/'
else:
    scriptdir = 'jobs/'


JDLtemplate = open(dirStem+'/template.jdl')
JDLtemplines = JDLtemplate.readlines()
JDLtemplate.close()

SHtemplate = open(dirStem+'/template.sh')
SHtemplines = SHtemplate.readlines()
SHtemplate.close()


job = analyzer.replace('.py','')
print 'creating jobs:',job

newjdl = open('jobs/'+job+'.jdl','w')
for jdlline in JDLtemplines:
    if 'template' in jdlline:
        jdlline = jdlline.replace('template', job)
    if 'XXX' in jdlline:
        jdlline = jdlline.replace('XXX',analysisName)
    if 'DDD' in jdlline:
        jdlline = jdlline.replace('DDD',job)
    if 'DIRECTORY' in jdlline:
        jdlline = jdlline.replace('DIRECTORY',dirStem)
    if 'ANALYZER' in jdlline:
        jdlline = jdlline.replace('ANALYZER',analyzer)
    if 'SCRIPTDIR' in jdlline:
        jdlline = jdlline.replace('SCRIPTDIR',scriptdir)
    newjdl.write(jdlline)
newjdl.close()
newsh = open('jobs/'+job+'.sh','w')
for shline in SHtemplines:
    if 'template' in shline:
        shline=shline.replace('template',job)
    if 'XXX' in shline:
        shline=shline.replace('XXX',analysisName)
    if 'DDD' in shline:
        shline=shline.replace('DDD',job)
    if 'DIRECTORY' in shline:
        shline = shline.replace('DIRECTORY',dirStem)
    if 'RUNARGS' in shline:
        shline = shline.replace('RUNARGS',RUNARGS)
    if 'ANALYZER' in shline:
        shline = shline.replace('ANALYZER',analyzer)
    if 'SCRIPTDIR' in shline:
        shline = shline.replace('SCRIPTDIR',scriptdir)
    newsh.write(shline)
newsh.close()

outputdir = 'Output/'+RUNARGS[:-1].replace(' ','')
if not os.path.exists(outputdir): os.system('mkdir '+outputdir)
print 'cd '+outputdir
os.chdir(outputdir)
cmd =  'condor_submit '+'../../jobs/'+job+'.jdl'
print cmd
os.system(cmd)
    
print 'done'
