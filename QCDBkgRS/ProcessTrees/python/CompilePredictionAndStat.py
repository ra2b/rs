from ROOT import *
from glob import glob
import os, sys

datamc = "Data"
lumi = '36.3'
Version = '2016'

try: systematic = sys.argv[1]
except: systematic = 'Nominal'
TriggerEff='Nom'


nExperiments = 5
nSmearsPerEvent = 500


#nExperiments = 1#Test100
#nSmearsPerEvent = 100#Test100

nExperiments = 5 #buttered up to test with 500
nSmearsPerEvent = 500

#nExperiments = 1

histlist = []
flist = []
for i in range(1, nExperiments+1): 
    incommon = systematic+'*'+TriggerEff+'*'+str(i)+'of'+str(nExperiments)
    print 'attempted glob', 'Output/'+incommon+'/*.root'
    print 'actualized glob', glob('Output/'+incommon+'/*.root')
    fExpI = glob('Output/'+incommon+'/*.root')
    print i, fExpI
    targetfile = 'Output/_'+systematic+TriggerEff+str(i)+'of'+str(nExperiments)+'.root'
    haddcommand = 'hadd -f '+targetfile
    for f in fExpI: haddcommand+=' '+f
    print haddcommand
    os.system(haddcommand)
    f = TFile(targetfile)
    flist.append(f)
    h = f.Get('hPrediction').Clone('hPrediction'+str(i)+'of'+str(nExperiments))
    histlist.append(h)
 
PredictionCV = histlist[0].Clone('PredictionCV')
PredictionCV.Sumw2()
PredictionCV.Reset()
PredictionStat = histlist[0].Clone('PredictionStat')
PredictionStat.Reset()
xax = PredictionCV.GetXaxis()
import numpy as np
for ibin in range(1,xax.GetNbins()+1):
    predictionset = []
    stringofzeroes = True
    for h in histlist: 
        predictionset.append(h.GetBinContent(ibin))
        if h.GetBinContent(ibin)!=0: stringofzeroes=False
    if stringofzeroes: predictionset[-1] = 1.0*nExperiments/nSmearsPerEvent
    npset = np.array(predictionset)
    PredictionCV.SetBinContent(ibin, np.mean(npset))
    PredictionCV.SetBinError(ibin, np.std(npset))
    PredictionStat.SetBinContent(ibin, np.std(npset))
    print ibin, np.mean(npset), '+/-', np.std(npset)

fnew = TFile(systematic+'Prediction'+lumi+'.root','recreate')
PredictionCV.Write()
PredictionStat.Write()
fnew.Close()
os.system('echo $PWD/'+systematic+'Prediction'+lumi+'.root')

