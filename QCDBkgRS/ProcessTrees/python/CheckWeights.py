from ROOT import *
from glob import glob
gStyle.SetOptStat(0)
from utils import *
from selectionstrings import *
import sys
gROOT.SetBatch(1)
ROOT.v5.TFormula.SetMaxima(10000)
datamc = "Data"
lumi = 0.001


Version = 'ICHEP'
Version = '2016'
try: systematic = sys.argv[1]
except: systematic = 'TailUp'

TriggerEff = 'Nom'

if datamc =='MC': ra2bweight = 'Weight'
else: ra2bweight = 'PrescaleWeightHt'
if Version=='ICHEP': SearchBinsA, SearchBinsB, SearchBinsC = SearchBinsICHEPa, SearchBinsICHEPb, SearchBinsICHEPc
if Version=='2016': SearchBinsA, SearchBinsB, SearchBinsC = SearchBins2016a, SearchBins2016b, SearchBins2016c	
print Version, systematic

redirect = {'Ldp':'root://cmseos.fnal.gov//store/user/sbein/', \
			'CoreDown':'root://cmsxrootd.fnal.gov//store/user/jsonneve/', \
			'CoreUp':'root://cmsxrootd.fnal.gov//store/user/jsonneve/', \
			'TailUp':'root://cmsxrootd.fnal.gov//store/user/mniedzie/', \
			'TailDown':'root://cmsxrootd.fnal.gov//store/user/mniedzie/', \
			'Nominal':'root://cmseos.fnal.gov//store/user/sbein/'}
nbins = {'ICHEP':160, '2016':174}

cPrediction = TChain('QCDfromSmearing/QCDPrediction')
nsmear = 100
newfile = TFile('PredictionCheckWeights.root','recreate')

ifile = 0
filenamelistfilenamelist = glob('filelists/filelist'+systematic+'*.txt')
for filenamelist in filenamelistfilenamelist:
	filelistfile =open(filenamelist)
	filelines = filelistfile.readlines()
	filelistfile.close()
	for line in filelines:
		#if not '0002' in line: continue
		filehead = line.strip().replace('/eos/uscms/','').replace('store/user/sbein/','').replace('/store/user/jsonneve/','').replace('/pnfs/desy.de/cms/tier2/store/user/mniedzie/','')
		if ifile<10: print redirect[systematic]+filehead
		if ifile==10: print '...'
		ifile+=1
		cPrediction.Add(redirect[systematic]+filehead)

cPrediction.Show(0)

print 'nentries(prediction) =', cPrediction.GetEntries()

deltaPhiDef =  '(abs(DeltaPhi1)>0.5 && abs(DeltaPhi2)>0.5 && abs(DeltaPhi3)>0.3 && abs(DeltaPhi4)>0.3)'
invDeltaPhiDef = ' (abs(DeltaPhi1)<0.5 || abs(DeltaPhi2)<0.5 || abs(DeltaPhi3)<0.3 || abs(DeltaPhi4)<0.3)'
if systematic=='Ldp': dphicut = invDeltaPhiDef
else: dphicut = deltaPhiDef

cuts = dphicut + ' && NVtx>0 '

obsBinsA, selectionBinsA = '('+SearchBinsA+'-1):Weight', cuts
obsBinsB, selectionBinsB = '('+SearchBinsB+'-1):Weight', cuts
obsBinsC, selectionBinsC = '('+SearchBinsC+'-1):Weight', cuts
cPrediction.Draw('Weight','('+selectionBinsA+') && ((500<=HT)*(HT<1000)*(350<=MHT)*(MHT<500)*(2<=NJets)*(NJets<=2)*(BTags==0))')
hPredictionA = cPrediction.GetHistogram().Clone('hPredictionA')
#cPrediction.Draw(obsBinsB,str(lumi*1000)+'*TrigEffWeight'+TriggerEff+'*Weight/'+str(nsmear)+'*('+selectionBinsB+')')
#hPredictionB = cPrediction.GetHistogram().Clone('hPredictionB')
#cPrediction.Draw(obsBinsC,str(lumi*1000)+'*TrigEffWeight'+TriggerEff+'*Weight/'+str(nsmear)+'*('+selectionBinsC+')')
#hPredictionC = cPrediction.GetHistogram().Clone('hPredictionC')
hPrediction = hPredictionA.Clone('hPrediction')
#hPrediction.Add(hPredictionB)
#hPrediction.Add(hPredictionC)
histoStyler(hPrediction, kTeal+2)
tl.DrawLatex(0.2,0.7,selectionBinsA)
c1.Update()
newfile.cd()
c1.Write('TreeClosure_BinByBin2016')
hPrediction.Write('hPrediction')

