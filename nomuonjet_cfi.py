import FWCore.ParameterSet.Config as cms

nomuonjet = cms.EDProducer('NoMuonInt',
JetTag_               = cms.InputTag('JetTag'),
MHTPhiTag_            = cms.InputTag('MHTPhiTag'),
)
