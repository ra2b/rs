// -*- C++ -*-
//
// Package:   NoMuonInt 
// Class:     NoMuonInt 
// 
/**\class NoMuonInt NoMuonInt.cc 

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
//
//


// system include files
#include <memory>
#include <TMath.h>
#include "TLorentzVector.h"
// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDProducer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/Candidate/interface/CandidateFwd.h"
//
// class declaration
//

class NoMuonInt : public edm::EDProducer {
   public:
      explicit NoMuonInt(const edm::ParameterSet&);
      ~NoMuonInt();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

   private:
      virtual void beginJob() ;
      virtual void produce(edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;
      
      virtual void beginRun(edm::Run&, edm::EventSetup const&);
      virtual void endRun(edm::Run&, edm::EventSetup const&);
      virtual void beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);
      virtual void endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);
      edm::InputTag JetTag_;
      edm::InputTag MHTPhiTag_;
	   edm::EDGetTokenT<edm::View<pat::Jet> > JetTok_;
	   edm::EDGetTokenT<double> MHTPhiTok_;


      // ----------member data ---------------------------
};

//
// constants, enums and typedefs
//


//
// static data member definitions
//

//
// constructors and destructor
//
NoMuonInt::NoMuonInt(const edm::ParameterSet& iConfig)
{
   //register your product
   JetTag_ = iConfig.getParameter<edm::InputTag>("JetTag");
   MHTPhiTag_ = iConfig.getParameter<edm::InputTag>("MHTPhiTag");
   JetTok_ = consumes<edm::View<pat::Jet> >(JetTag_);
   MHTPhiTok_ = consumes<double>(MHTPhiTag_);

	 produces<int>("NoMuonInt");
/* Examples
   produces<ExampleData2>();

   //if do put with a label
   produces<ExampleData2>("label");
 
   //if you want to put into the Run
   produces<ExampleData2,InRun>();
*/
   //now do what ever other initialization is needed
  
}


NoMuonInt::~NoMuonInt()
{
 
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called to produce the data  ------------
void
NoMuonInt::produce(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;

  edm::Handle<edm::View<pat::Jet> > Jets;
  edm::Handle< double > MHTPhi;

  iEvent.getByToken(JetTok_,Jets);
  iEvent.getByToken(MHTPhiTok_,MHTPhi);

  //	double mhtphi = *MHTPhi;
  int noMuonJet = 1;

  // if( Jets.isValid() ) {
  //   for(unsigned j = 0; j < Jets->size(); ++j){
  //     if(TMath::IsNaN(Jets->at(j).phi() - (*MHTPhi))){
  // 	noMuonJet = 0;
  // 	std::cout << "jetphi - metphi isNaN" << std::endl;
  // 	break;
  //     }
  //     if(Jets->at(j).pt() > 200 && Jets->at(j).chargedMuEnergyFraction() > 0.5 && reco::deltaPhi(Jets->at(j).phi(),(*MHTPhi)) > (TMath::Pi() - 0.4)){
  // 	std::cout << "charged fraction " << Jets->at(j).chargedMuEnergyFraction() << std::endl;
  // 	noMuonJet = 0;
  // 	break;
  //     }
  //   }
  // }

  if( Jets.isValid() ) {
    for(unsigned j = 0; j < Jets->size(); ++j){
      if(TMath::IsNaN(Jets->at(j).phi() - (*MHTPhi))){
	noMuonJet = 0;
	std::cout << "jetphi - metphi isNaN" << std::endl;
	break;
      }
      if(Jets->at(j).pt() > 200 && Jets->at(j).chargedMuEnergyFraction() > 0.1 && fabs(reco::deltaPhi(Jets->at(j).phi(),(*MHTPhi))) > (TMath::Pi() - 0.4)){
	std::cout << "charged fraction " << Jets->at(j).chargedMuEnergyFraction() << std::endl;
	noMuonJet = 0;
	break;
      }
    }
  }

  else std::cout<<"sth is wong, NoMuonInt.cc"<<std::endl;
  std::auto_ptr<int> NoMuonInt(new int(noMuonJet));
	iEvent.put(NoMuonInt,"NoMuonInt");

}

// ------------ method called once each job just before starting event loop  ------------
void 
NoMuonInt::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
NoMuonInt::endJob() {
}

// ------------ method called when starting to processes a run  ------------
void 
NoMuonInt::beginRun(edm::Run&, edm::EventSetup const&)
{
}

// ------------ method called when ending the processing of a run  ------------
void 
NoMuonInt::endRun(edm::Run&, edm::EventSetup const&)
{
}

// ------------ method called when starting to processes a luminosity block  ------------
void 
NoMuonInt::beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&)
{
}

// ------------ method called when ending the processing of a luminosity block  ------------
void 
NoMuonInt::endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&)
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
NoMuonInt::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(NoMuonInt);

