// -*- C++ -*-
//
// Package:    RandSJetsProducer
// Class:      RandSJetsProducer
// 
/**\class RandSJetsProducer RandSJetsProducer.cc RA2Classic/RandSJetsProducer/src/RandSJetsProducer.cc

   Description: [one line class summary]

   Implementation:
   [Notes on implementation]
*/
//
// Original Author:  Sam Bein
//         Created:  Fri Nov 11 16:35:33 CEST 2014
// $Id$
//
//


// system include files
#include <memory>
// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDProducer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/Candidate/interface/CandidateFwd.h"
#include "PhysicsTools/PatAlgos/plugins/PATJetProducer.h"

#include "rs/QCDBkgRS/interface/UsefulJet.h"
#include "TLorentzVector.h"
#include "TFile.h"

//
// class declaration
//

class RandSJetsProducer : public edm::EDProducer {
public:
  explicit RandSJetsProducer(const edm::ParameterSet&);
  ~RandSJetsProducer();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  virtual void beginJob() ;
  virtual void produce(edm::Event&, const edm::EventSetup&);
  virtual void endJob() ;
      
  virtual void beginRun(edm::Run&, edm::EventSetup const&);
  virtual void endRun(edm::Run&, edm::EventSetup const&);
  virtual void beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);
  virtual void endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);


  bool RebalanceJets_BayesFitter(std::vector<UsefulJet>);
  std::vector<UsefulJet> SmearJets(std::vector<UsefulJet>, int, std::vector<double> & smearvector);
  void GleanTemplatesFromFile(TFile* ftemplate);

  edm::InputTag jetTag_;
  edm::EDGetTokenT<edm::View<pat::Jet> > jetsToken_;

  edm::InputTag genJetTag_;
  edm::EDGetTokenT<edm::View<reco::GenJet> > genJetsToken_;

  bool PrintJets_;  
  bool doGenSmear_;

  edm::InputTag treemakerWeightName_;
  edm::EDGetTokenT<double> treemakerWeightToken_;
  double WeightFromTreemaker_;

  /*
  edm::InputTag pfCandidatesTag_;
  edm::EDGetTokenT<edm::View<pat::PackedCandidate>> pfCandidatesTok_;
  */

  std::string btagTag_;
  int nTries_;
  int nSmearsPerTry_;

  std::string smearingfile_;
  TFile* TemplatesFile;

  // ----------member data ---------------------------
};

  TemplateSet _Templates_;//global struct
  double *_par_; 
  UsefulJet _leadjet_; double _iLeadJet_ = 0; double _LeadJetPt_ = 0; 
  int _nbjets_ = 0; int _ibjet_;
  TGraph * _ActiveMhtPrior_, * _ActiveMhtPhiPrior_; 
  TLorentzVector _ActiveMht_; double _ActiveHt_; int _iht_;
  double _ActiveLikelihood_; 
  double _ptNew_, _ptBinC_, _PtBinOther_, _interpolatedFactor_;
  double _a_, _b_; 
  int _ietaNew_, _iptNew_, _otherbin_;
  double _rmht_, _rdphi_;

//
// constants, enums and typedefs
//


//
// static data member definitions

//
// constructors and destructor
//
RandSJetsProducer::RandSJetsProducer(const edm::ParameterSet& iConfig)
{
  //register your product
  jetTag_ = iConfig.getParameter<edm::InputTag> ("jetCollection");
  jetsToken_ = consumes<edm::View<pat::Jet> >(jetTag_);

  /*
  pfCandidatesTag_		= iConfig.getParameter<edm::InputTag>("pfCandidatesTag");
  pfCandidatesTok_ = consumes<edm::View<pat::PackedCandidate>>(pfCandidatesTag_);
  */

  genJetTag_ = iConfig.getParameter<edm::InputTag> ("genJetCollection");
  genJetsToken_ = consumes<edm::View<reco::GenJet> >(genJetTag_);
  PrintJets_ = iConfig.getParameter<bool> ("PrintJets");
  doGenSmear_ = iConfig.getParameter<bool> ("doGenSmear");
  treemakerWeightName_ = iConfig.getParameter<edm::InputTag> ("treemakerWeightName");
  treemakerWeightToken_ = consumes<double>(treemakerWeightName_);
  btagTag_ = iConfig.getParameter<std::string> ("btagTag");
  nTries_     = iConfig.getParameter<int>("NTries");
  nSmearsPerTry_     = iConfig.getParameter<int>("NSmearsPerTry");
  std::cout << "I feel like I shouldn't have to be doing this at 2 am, but NSmearsPerTry = " << nSmearsPerTry_ << std::endl;

  smearingfile_ = iConfig.getParameter<std::string> ("SmearingFile");
  std::cout << "Did I make it past this hurdle?" << std::endl;

  produces<std::vector<pat::Jet> >("RebalancedJets");
  for (int itry=0; itry<nTries_; itry++)
    {
      for (int iextra=0; iextra<nSmearsPerTry_; iextra++)
	{
      char buffer [50];
      sprintf (buffer, "RandSJetsTry%dSmear%d", itry, iextra);
      produces<std::vector<pat::Jet> > (buffer);
      
      /*
      sprintf (buffer, "sammysPackedCandidatesTry%dSmear%d", itry, iextra);
      produces< std::vector<pat::PackedCandidate> > (buffer);
      */
	}
    }
  produces<double >("SmearingWeight");
  produces<bool >("FitSucceed");
  //produces< std::vector<pat::PackedCandidate> > ("sammysPackedCandidates");

}


RandSJetsProducer::~RandSJetsProducer()
{
 

}


//
// member functions
//

void RandSJetsProducer::GleanTemplatesFromFile(TFile* ftemplate)
{

  std::cout << "about to glean" << std::endl;
  TH1F* hPtTemplate = (TH1F*)ftemplate->Get("hPtTemplate");
  TAxis* templatePtAxis = (TAxis*)hPtTemplate->GetXaxis();
  TH1F* hEtaTemplate = (TH1F*)ftemplate->Get("hEtaTemplate");
  TAxis* templateEtaAxis = (TAxis*)hEtaTemplate->GetXaxis();
  TH1F* hHtTemplate = (TH1F*)ftemplate->Get("hHtTemplate");
  TAxis* templateHtAxis = (TAxis*)hHtTemplate->GetXaxis();

  TH1F* hNull = new TH1F("hNull","hNull",1,-4,-3);
  std::vector<TH1F* >  hNullVec;
  hNullVec.push_back(hNull);
  TGraph* gNull = new TGraph();
  std::vector<TGraph*>  gNullVec;
  gNullVec.push_back(gNull);

  std::cout << "gleaning" << std::endl;
  std::vector<std::vector<TH1F*> > hResTemplates_CC;
  hResTemplates_CC.push_back(hNullVec);
  std::vector<std::vector<TH1F*> > hResTemplatesB_CC;
  hResTemplatesB_CC.push_back(hNullVec);

  std::vector<std::vector<TH1F*> > hRebTemplates_CC;
  hRebTemplates_CC.push_back(hNullVec);
  std::vector<std::vector<TH1F*> > hRebTemplatesB_CC;
  hRebTemplatesB_CC.push_back(hNullVec);

  std::vector<std::vector<TGraph*> > gRebTemplates_CC;
  gRebTemplates_CC.push_back(gNullVec);
  std::vector<std::vector<TGraph*> > gRebTemplatesB_CC;
  gRebTemplatesB_CC.push_back(gNullVec);

  std::cout << "continuing to glean" << std::endl;
  for(int ieta = 1; ieta<templateEtaAxis->GetNbins()+1; ieta++)
    {
      hResTemplates_CC.push_back(hNullVec);
      hRebTemplates_CC.push_back(hNullVec);
      gRebTemplates_CC.push_back(gNullVec);
      hResTemplatesB_CC.push_back(hNullVec);
      hRebTemplatesB_CC.push_back(hNullVec);
      gRebTemplatesB_CC.push_back(gNullVec);
      for (int ipt=1; ipt < templatePtAxis->GetNbins()+1; ipt++)
    	{	
    	  char hname[100];
    	  sprintf (hname, "hRTemplate(gPt%2.1f-%2.1f, gEta%2.1f-%2.1f)", templatePtAxis->GetBinLowEdge(ipt), templatePtAxis->GetBinUpEdge(ipt), templateEtaAxis->GetBinLowEdge(ieta), templateEtaAxis->GetBinUpEdge(ieta));
    	  TH1F* h = (TH1F*)ftemplate->Get(hname);
    	  hResTemplates_CC.back().push_back(h);
    	  hRebTemplates_CC.back().push_back(h);//once upon a time these were rPt"s

    	  sprintf (hname, "hRTemplate(gPt%2.1f-%2.1f, gEta%2.1f-%2.1f)B", templatePtAxis->GetBinLowEdge(ipt), templatePtAxis->GetBinUpEdge(ipt), templateEtaAxis->GetBinLowEdge(ieta), templateEtaAxis->GetBinUpEdge(ieta));
    	  TH1F* hB = (TH1F*)ftemplate->Get(hname);
    	  hResTemplatesB_CC.back().push_back(hB);//trying regular templates
    	  hRebTemplatesB_CC.back().push_back(hB);//was hB

    	  char gname[100];
    	  sprintf (gname, "splines/hRTemplate(gPt%2.1f-%2.1f, gEta%2.1f-%2.1f)_graph", templatePtAxis->GetBinLowEdge(ipt), templatePtAxis->GetBinUpEdge(ipt), templateEtaAxis->GetBinLowEdge(ieta), templateEtaAxis->GetBinUpEdge(ieta));
    	  TGraph* g = (TGraph*)ftemplate->Get(gname);
    	  gRebTemplates_CC.back().push_back(g);

    	  sprintf (gname, "splines/hRTemplate(gPt%2.1f-%2.1f, gEta%2.1f-%2.1f)B_graph", templatePtAxis->GetBinLowEdge(ipt), templatePtAxis->GetBinUpEdge(ipt), templateEtaAxis->GetBinLowEdge(ieta), templateEtaAxis->GetBinUpEdge(ieta));
    	  TGraph* gB = (TGraph*)ftemplate->Get(gname);
    	  gRebTemplatesB_CC.back().push_back(gB);//Instead of gB
    	}
    }

  std::vector<TGraph*> gGenMhtPtTemplatesB0_CC;
  gGenMhtPtTemplatesB0_CC.push_back(gNull);
  std::vector<TGraph*> gGenMhtPtTemplatesB1_CC;
  gGenMhtPtTemplatesB1_CC.push_back(gNull);
  std::vector<TGraph*> gGenMhtPtTemplatesB2_CC;
  gGenMhtPtTemplatesB2_CC.push_back(gNull);
  std::vector<TGraph*> gGenMhtPtTemplatesB3_CC;
  gGenMhtPtTemplatesB3_CC.push_back(gNull);
  std::vector<TGraph*> gGenMhtDPhiTemplatesB0_CC;
  gGenMhtDPhiTemplatesB0_CC.push_back(gNull);
  std::vector<TGraph*> gGenMhtDPhiTemplatesB1_CC;
  gGenMhtDPhiTemplatesB1_CC.push_back(gNull);
  std::vector<TGraph*> gGenMhtDPhiTemplatesB2_CC;
  gGenMhtDPhiTemplatesB2_CC.push_back(gNull);
  std::vector<TGraph*> gGenMhtDPhiTemplatesB3_CC;
  gGenMhtDPhiTemplatesB3_CC.push_back(gNull);

  std::string keyvar = "Mht";//This could be changed later if the MET is to be used

  for(int iht = 1; iht < templateHtAxis->GetNbins()+2; iht++)
    {
      char gname[100];
      sprintf (gname, "splines/hGen%sPtB0(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb0 = (TGraph*)ftemplate->Get(gname);
      gGenMhtPtTemplatesB0_CC.push_back(fb0);
      char gnamePhi[100];
      sprintf (gnamePhi, "splines/hGen%sPhiB0(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb0phi = (TGraph*)ftemplate->Get(gnamePhi);
      gGenMhtDPhiTemplatesB0_CC.push_back(fb0phi);
      sprintf (gname, "splines/hGen%sPtB1(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb1 = (TGraph*)ftemplate->Get(gname);
      gGenMhtPtTemplatesB1_CC.push_back(fb1);
      sprintf (gnamePhi, "splines/hGen%sPhiB1(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb1phi = (TGraph*)ftemplate->Get(gnamePhi);
      gGenMhtDPhiTemplatesB1_CC.push_back(fb1phi);
      sprintf (gname, "splines/hGen%sPtB2(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb2 = (TGraph*)ftemplate->Get(gname);
      gGenMhtPtTemplatesB2_CC.push_back(fb2);
      sprintf (gnamePhi, "splines/hGen%sPhiB2(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb2phi = (TGraph*)ftemplate->Get(gnamePhi);
      gGenMhtDPhiTemplatesB2_CC.push_back(fb2phi);
      sprintf (gname, "splines/hGen%sPtB3(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb3 = (TGraph*)ftemplate->Get(gname);
      gGenMhtPtTemplatesB3_CC.push_back(fb3);
      sprintf (gnamePhi, "splines/hGen%sPhiB3(ght%2.1f-%2.1f)_graph", keyvar.c_str(), templateHtAxis->GetBinLowEdge(iht), templateHtAxis->GetBinUpEdge(iht));
      TGraph* fb3phi = (TGraph*)ftemplate->Get(gnamePhi);
      gGenMhtDPhiTemplatesB3_CC.push_back(fb3phi);
    }

  _Templates_.hEtaTemplate = hEtaTemplate;
  _Templates_.hPtTemplate = hPtTemplate;//_Templates_.hResTemplates
  _Templates_.hHtTemplate = hHtTemplate;
  _Templates_.ResponseFunctions.clear();
  _Templates_.ResponseFunctions.push_back(gRebTemplates_CC);
  _Templates_.ResponseFunctions.push_back(gRebTemplatesB_CC);

  _Templates_.ResponseHistos.clear();
  _Templates_.ResponseHistos.push_back(hResTemplates_CC);
  _Templates_.ResponseHistos.push_back(hResTemplatesB_CC);
  _Templates_.gGenMhtPtTemplatesB0 = gGenMhtPtTemplatesB0_CC;
  _Templates_.gGenMhtPtTemplatesB1 = gGenMhtPtTemplatesB1_CC;
  _Templates_.gGenMhtPtTemplatesB2 = gGenMhtPtTemplatesB2_CC;
  _Templates_.gGenMhtPtTemplatesB3 = gGenMhtPtTemplatesB3_CC;
  _Templates_.gGenMhtDPhiTemplatesB0 = gGenMhtDPhiTemplatesB0_CC;
  _Templates_.gGenMhtDPhiTemplatesB1 = gGenMhtDPhiTemplatesB1_CC;
  _Templates_.gGenMhtDPhiTemplatesB2 = gGenMhtDPhiTemplatesB2_CC;
  _Templates_.gGenMhtDPhiTemplatesB3 = gGenMhtDPhiTemplatesB3_CC;
  _Templates_.lhdMhtThresh = lhdMhtThresh;
  _Templates_.nparams = 0;
}


void fcn(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
  _ActiveLikelihood_ = 1.0;
  _iLeadJet_ = 0; _LeadJetPt_ = 0; _nbjets_ = 0; 
  _ActiveMht_.SetPtEtaPhiE(0,0,0,0); _ActiveHt_ = 0;
  for (int i = 0; i < _Templates_.nparams; i++)
    {
      _Templates_.dynamicJets[i]*=(_par_[i]*1.0/par[i]);//my attempt to scale the jets only once per step.
      _par_[i]=par[i];//my attempt to scale the jets only once per step.
      if(_Templates_.dynamicJets[i].Pt()>_LeadJetPt_)
	{ _iLeadJet_ = i; _LeadJetPt_ = _Templates_.dynamicJets[i].Pt();}
      if(fabs(_Templates_.dynamicJets[i].Eta())<2.4)
	{
	  if (_Templates_.dynamicJets[i].Pt()>JET_PT_THRESH) _ActiveHt_+= _Templates_.dynamicJets[i].Pt(); 
	  if(_Templates_.dynamicJets[i].csv>BTAG_CSV &&
	     _Templates_.dynamicJets[i].Pt()>_Templates_.lhdMhtThresh ) _nbjets_+=1;
	}    
      if(_Templates_.dynamicJets[i].Pt()>_Templates_.lhdMhtThresh)
	_ActiveMht_-=_Templates_.dynamicJets[i].tlv;
      //std::cout << "island 1" << std::endl;
      _ptNew_ = TMath::Min(TMath::Max(11.0, _Templates_.dynamicJets[i].Pt()),1799.0);
      _ietaNew_ = _Templates_.hEtaTemplate->GetXaxis()->FindBin(fabs(_Templates_.dynamicJets[i].Eta()));
      _iptNew_ = _Templates_.hPtTemplate->GetXaxis()->FindBin(_ptNew_);
      _ptBinC_ = _Templates_.hPtTemplate->GetXaxis()->GetBinCenter(_iptNew_);
      _otherbin_ = -1;
      if (_ptNew_-_ptBinC_>0) _otherbin_ = 1;
      _PtBinOther_ = _Templates_.hPtTemplate->GetXaxis()->GetBinCenter(_iptNew_+_otherbin_);
      _a_ = (_PtBinOther_-_ptNew_)/(_PtBinOther_ - _ptBinC_);
      _b_ = (_ptNew_-_ptBinC_)/(_PtBinOther_ - _ptBinC_);
      //std::cout << "island 2" << std::endl;
      try
	{
	  //TString thing; thing.Form("%f, %f, %f", _ptNew_, _Templates_.dynamicJets[i].Eta(), _Templates_.dynamicJets[i].csv);
	  //std::cout << "likelihood alley pt, eta, csv = " << thing << std::endl;
	  _interpolatedFactor_ = 0.5*(_a_*_Templates_.ResponseFunctions[_Templates_.dynamicJets[i].csv>BTAG_CSV][_ietaNew_][_iptNew_]->Eval(par[i],0,"S") +
				      _b_*_Templates_.ResponseFunctions[_Templates_.dynamicJets[i].csv>BTAG_CSV][_ietaNew_][_iptNew_+_otherbin_]->Eval(par[i],0,"S"));
	  //std::cout << "island 3" << std::endl;
	  //std::cout << "thru likelihood alley" << std::endl;
	}
      catch (std::exception& e)
	{
	  _interpolatedFactor_ = _b_*_Templates_.ResponseFunctions[_Templates_.dynamicJets[i].csv>BTAG_CSV][_ietaNew_][_iptNew_+_otherbin_]->Eval(par[i],0,"S");
	}
      _ActiveLikelihood_*=_interpolatedFactor_;
    }
  _ActiveHt_ = TMath::Min(4999.0,_ActiveHt_);
  _iht_ = _Templates_.hHtTemplate->GetXaxis()->FindBin(_ActiveHt_);

  if (_nbjets_ == 0)
    {
      _leadjet_ = _Templates_.dynamicJets[_iLeadJet_];
      _ActiveMhtPrior_ = _Templates_.gGenMhtPtTemplatesB0[_iht_];
      _ActiveMhtPhiPrior_ = _Templates_.gGenMhtDPhiTemplatesB0[_iht_];
    }
  else 
    {
      getLeadingBJet_CC(_Templates_.dynamicJets, _ibjet_, _nbjets_, _leadjet_);
      if(_nbjets_ == 1)
	{
	  _ActiveMhtPrior_ = _Templates_.gGenMhtPtTemplatesB1[_iht_];
	  _ActiveMhtPhiPrior_ = _Templates_.gGenMhtDPhiTemplatesB1[_iht_];
	}
      else if(_nbjets_ == 2)
	{
	  _ActiveMhtPrior_ = _Templates_.gGenMhtPtTemplatesB2[_iht_];
	  _ActiveMhtPhiPrior_ = _Templates_.gGenMhtDPhiTemplatesB2[_iht_];
	}
      else
	{
	  _ActiveMhtPrior_ = _Templates_.gGenMhtPtTemplatesB3[_iht_];
	  _ActiveMhtPhiPrior_ = _Templates_.gGenMhtDPhiTemplatesB3[_iht_];
	}
    }

  _rmht_ = _ActiveMht_.Pt();
  _rdphi_ = _ActiveMht_.DeltaPhi(_leadjet_.tlv);
  _ActiveLikelihood_*=_ActiveMhtPrior_->Eval(_rmht_,0,"S");
  _ActiveLikelihood_*=_ActiveMhtPhiPrior_->Eval(_rdphi_,0,"S");
  f = -fabs(_ActiveLikelihood_);

  return;

}


bool RandSJetsProducer::RebalanceJets_BayesFitter(std::vector<UsefulJet> originalJets){
  _Templates_.dynamicJets.clear();
  _Templates_.nparams = 0;
  for (unsigned int j = 0; j < originalJets.size(); j++)
    {
      if (originalJets[j].Pt()>_Templates_.lhdMhtThresh && j<12) _Templates_.nparams+=1; 
      _Templates_.dynamicJets.push_back(originalJets[j].Clone());
    }

  _par_ = new double[_Templates_.nparams]; for(int i=0; i<_Templates_.nparams; i++) _par_[i] = 1.0;

  int ipin; double cstart;
  findJetToPin(_Templates_.dynamicJets, _Templates_.nparams, ipin, cstart);

  TMinuit * gMinuit = new TMinuit(_Templates_.nparams);
  //gMinuit->SetPrintLevel(0);

  gMinuit->SetFCN(fcn);
  gMinuit->SetPrintLevel(-1);
  Double_t arglist[_Templates_.nparams];
  Int_t ierflg = 0;//Long ierflg = 0;
  if (_Templates_.nparams>0) arglist[0] = 0;

  for(int i = 0; i < _Templates_.nparams; i++)
    {
      char buffer [5];
      sprintf (buffer, "c%d", i);
      //double lowerbound = 0.3; double upperbound = 3.0;
      double lowerbound = 0.01; double upperbound = 5.0;
      //make identical to diss python:
      lowerbound = 0.3; upperbound = 3.5;
      if(i==ipin) gMinuit->mnparm(i, buffer, cstart, 0.05, lowerbound, upperbound, ierflg);
      else  gMinuit->mnparm(i,buffer,1.0,0.05,lowerbound,upperbound,ierflg);
    }
  gMinuit->SetMaxIterations(10000);
  if (_Templates_.nparams>0) arglist[0] = 10000;
  if (_Templates_.nparams>1) arglist[1] = 1;
  //std::cout << "getting ready to MINIMIZE"<< std::endl;
  gMinuit->mnexcm( "MINIMIZE", arglist, 2, ierflg );
  //std::cout << "minimized" << std::endl;
  TLorentzVector rmhtVec = getMHT(_Templates_.dynamicJets, JET_PT_THRESH);
  if (ierflg!=0){
    //std::cout << "failed with attempted MHT of " << rmhtVec.Pt() << std::endl;
    return false;
  }
  if (!(rmhtVec.Pt()<160)){
    //std::cout << "failed with large MHT of " << rmhtVec.Pt() << std::endl;
    return false;
  }


  double currentValue(1), currentError(1);
  for (int i=0; i<_Templates_.nparams; i++){
    gMinuit->GetParameter (i, currentValue, currentError);
    _Templates_.dynamicJets[i]*=(originalJets[i].Pt()/_Templates_.dynamicJets[i].Pt()/currentValue);
  }

  // TLorentzVector omhtVec = getMHT(originalJets, JET_PT_THRESH);
  // double omht = omhtVec.Pt();
  // double odphi = omhtVec.Phi();
  // std::cout << "old mht = " << omht << ", " << odphi << std::endl;

  // double rmht = rmhtVec.Pt();
  // double rdphi = rmhtVec.Phi();
  // std::cout << "new mht = " << rmht << ", " << rdphi << std::endl;
  
  //std::sort(_Templates_.dynamicJets.begin(), _Templates_.dynamicJets.end());//////yikes, killing sort to preserve map validity

  return true;
}


std::vector<UsefulJet> RandSJetsProducer::SmearJets(std::vector<UsefulJet> jetVec, int n2smear, std::vector<double> & smearvector){
  smearvector.clear();
  std::vector<UsefulJet> smearedJets;
  for (unsigned int j=0; j<jetVec.size(); j++){
    smearedJets.push_back(jetVec[j].Clone());
    double pt = smearedJets.back().Pt(); 
    double eta = smearedJets.back().Eta(); 
    if (pt<8 || int(j)>=n2smear) {
      smearvector.push_back(1.0);
      continue;
    }
    int ieta = _Templates_.hEtaTemplate->GetXaxis()->FindBin(fabs(eta));
    int ipt = _Templates_.hPtTemplate->GetXaxis()->FindBin(pt);
    double rando = 1;
    if (!(_Templates_.ResponseHistos.at(smearedJets.back().csv>BTAG_CSV).at(ieta).at(ipt)->Integral()==0))
      {
	//std::cout << "right before smearing, eta, pt, csv="<< pt << ", " << eta << ", " << smearedJets.back().csv <<  std::endl;//
	rando = _Templates_.ResponseHistos.at(smearedJets.back().csv>BTAG_CSV).at(ieta).at(ipt)->GetRandom();
	//std::cout << "right after smearing, eta, pt, csv="<< pt << ", " << eta << ", " << smearedJets.back().csv <<  std::endl;//
	smearedJets.back()*=rando;
      }
    smearvector.push_back(smearedJets.back().tlv.Pt()/smearedJets.back().origPt);//should really debug this very deeply.
    //smearvector.push_back(rando);
  }
  //std::sort(smearedJets.begin(), smearedJets.end());//////yikes, killing sort to preserve csv map validity
  return smearedJets;
}


std::vector<UsefulJet> EndowGenJets(edm::View<pat::Jet> RecoPatJets, edm::View<reco::GenJet> GenJets)
{
  TLorentzVector lviReco_;
  TLorentzVector lviGen_;
  TLorentzVector lviGenBest_;
  std::vector<UsefulJet> endowedGenJets;

  for (edm::View<reco::GenJet>::const_iterator jGen = GenJets.begin(); jGen != GenJets.end(); ++jGen) 
    {
      lviGen_.SetPxPyPzE(jGen->px(), jGen->py(), jGen->pz(), jGen->energy());
      double bestCsv = 0;
      double dRBig = 10;
      int ireco = 0;
      for (edm::View<pat::Jet>::const_iterator jReco = RecoPatJets.begin(); jReco != RecoPatJets.end(); ++jReco) 
	{
	  lviReco_.SetPxPyPzE(jReco->px(), jReco->py(), jReco->pz(), jReco->energy());
	  lviGenBest_ = lviReco_;
	  double dR = lviReco_.DeltaR(lviGen_);
	  if (!(dR<dRBig && dR<1.9)) continue;
	  dRBig = dR;
	  lviGenBest_ = lviGen_;
	  //std::cout << "matched a gen jet/ reco jet pts..." << lviGen_.Pt() << ", " << lviReco_.Pt() <<std::endl;
	  ireco+=1; 
	  bestCsv  =jReco->bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags");
	  if (dR<0.1) break;
	}
    UsefulJet matchedRecoGenJet(lviGenBest_, 
				bestCsv, 
				ireco);
    //std::cout << "ended up with pt" << matchedRecoGenJet.Pt() <<std::endl;
    endowedGenJets.push_back(matchedRecoGenJet);
    }
  return endowedGenJets;
}


// ------------ method called to produce the data  ------------
void RandSJetsProducer::produce(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;

  bool WeightVerbose = false;
  int n2smear = 0;

  edm::Handle<double> weight_treemaker;
  iEvent.getByToken(treemakerWeightToken_, weight_treemaker);
  WeightFromTreemaker_ = (weight_treemaker.isValid() ? (*weight_treemaker) : 1.0);
  if (WeightVerbose) std::cout << "weight from treemaker: " << WeightFromTreemaker_ << std::endl;


  /*
  //---------------------------------
  // get PFCandidate collection
  //---------------------------------
  edm::Handle<edm::View<pat::PackedCandidate> > pfCandidates;
  iEvent.getByToken(pfCandidatesTok_, pfCandidates);
  */

  //---------------------------------------------------------------------\
  // get silly collection to try to re-create result from the other day--->
  //--------------------------------------------------------------------/
  // std::auto_ptr< std::vector<pat::PackedCandidate> > sammysPackedCandidates( new std::vector<pat::PackedCandidate> );
  // for(size_t i=0; i<pfCandidates->size();i++)
  //   {
  //     pat::PackedCandidate pfCand = (*pfCandidates)[i];
  //     pat::Jet::LorentzVector newP4(pfCand.px(),pfCand.py(),
  // 				    pfCand.pz(),pfCand.energy());
  //     pfCand.setP4(newP4);
  //     sammysPackedCandidates->push_back(pfCand);
  //     //std::cout << "virgin peasant " << i << " pt = " << pfCand.pt() << ", eta = " << pfCand.eta() << std::endl;
  //   }


  if ((!weight_treemaker.isValid()) || WeightFromTreemaker_==0)
    {
      if (!weight_treemaker.isValid()) std::cout << "weight not found" << std::endl;
      auto finalRebalancedJets = std::make_unique<std::vector<pat::Jet> >();
      iEvent.put(std::move(finalRebalancedJets), "RebalancedJets");
      auto SmearingWeight = std::make_unique<double>(0);
      iEvent.put(std::move(SmearingWeight),"SmearingWeight");
      auto fitSucceed =  std::make_unique<bool>(true);
      iEvent.put(std::move(fitSucceed),"FitSucceed");
      //iEvent.put(std::move(sammysPackedCandidates), "sammysPackedCandidates");
      for (int itry=0; itry<nTries_; itry++)
	{
	  for (int iextra=0; iextra<nSmearsPerTry_; iextra++)
	    {
	      char buffer [50];
	      sprintf (buffer, "RandSJetsTry%dSmear%d", itry, iextra);
	      auto finalRandSJets = std::make_unique<std::vector<pat::Jet> >();
	      iEvent.put(std::move(finalRandSJets), buffer);

	      /*
	      std::auto_ptr< std::vector<pat::PackedCandidate> > finalPackedCandidates( new std::vector<pat::PackedCandidate> );
	      sprintf (buffer, "sammysPackedCandidatesTry%dSmear%d", itry, iextra);
	      iEvent.put(std::move(finalPackedCandidates), buffer);
	      */
	    }
	}
      if (WeightVerbose) std::cout << "had to zero out some stuff with treemaker weight = " << WeightFromTreemaker_ << std::endl;
      return;
    }


  edm::Handle<edm::View<pat::Jet> > RecoPatJets;
  iEvent.getByToken(jetsToken_, RecoPatJets);
  edm::View<pat::Jet> Jets_rec = *RecoPatJets;


  /*
  //--------------------------------------------------
  // create mapping between daughters and the peasants
  //--------------------------------------------------

  std::vector<std::vector<int> > peasant_of_daughter_of_jet;
  int matchcount = 0;
  int daughtercount = 0;
  for (edm::View<pat::Jet>::const_iterator it = Jets_rec.begin(); it != Jets_rec.end(); ++it) {
    std::vector<int> peasant_of_daughter;
    peasant_of_daughter_of_jet.push_back(peasant_of_daughter);
    for (unsigned int idaughter=0; idaughter<it->numberOfDaughters(); idaughter++)
      {
  	daughtercount+=1;
  	int matched_peasant_idx = -1;
  	for(size_t i=0; i<pfCandidates->size();i++)
  	  {
	    //if(idaughter==-1)std::cout << "candidate pT, eta = " <<it->daughter(idaughter)->pt() << "and mass = " << TMath::Sqrt(std::pow(it->daughter(idaughter)->energy(),2)-std::pow(it->daughter(idaughter)->px(),2)-std::pow(it->daughter(idaughter)->py(),2)-std::pow(it->daughter(idaughter)->pz(),2)) << std::endl;
	    if(it->daughter(idaughter)->px()==(*pfCandidates)[i].px())
	      {
		if(it->daughter(idaughter)->py()==(*pfCandidates)[i].py())
		  {
		    matched_peasant_idx = i;
		    matchcount+=1;
		    break;
		  }
  	      }
  	  }
  	peasant_of_daughter_of_jet.back().push_back(matched_peasant_idx);
      }
  }//std::cout << "daughter count and matched daughter count are: " << daughtercount << " and " << matchcount << std::endl;
  */

  //gen smear snippet pasted below



  std::vector<UsefulJet> UJets_gen;
  edm::Handle<edm::View<reco::GenJet> > GenJets_handle;
  bool gj_present = iEvent.getByToken(genJetsToken_, GenJets_handle);
  std::vector<UsefulJet> UJets_rec;
  std::vector<UsefulJet> UJets_reb;

  //--------------------------------------------------
  // Put pat::jets into a "Useful" format (hehe)
  //--------------------------------------------------
  int patidx = 0;
  if (PrintJets_) std::cout << ":::::::::::ORIGINAL PAT JETS:::::::"<< std::endl;
  for (edm::View<pat::Jet>::const_iterator it = Jets_rec.begin(); it != Jets_rec.end(); ++it) {
    TLorentzVector lvi;
    lvi.SetPxPyPzE(it->px(), it->py(), it->pz(), it->energy());
    double csv = it->bDiscriminator(btagTag_);
    UJets_rec.emplace_back(UsefulJet(lvi,csv, patidx, it->pt()));
    if (PrintJets_) std::cout<<"pt,eta,csv="<<UJets_rec.back().Pt()<<","<<UJets_rec.back().Eta()<<","<<csv<<","<<std::endl;
    patidx +=1;
  }

  //--------------------------------------------------
  // Rebalance the jets
  //--------------------------------------------------
  bool isRebalanced = true;
  if (doGenSmear_ && gj_present)
    {
      UJets_reb = EndowGenJets(Jets_rec, (*GenJets_handle));
    }
  else 
    {
      isRebalanced = RebalanceJets_BayesFitter(UJets_rec);
      if (isRebalanced) 
	{
	  UJets_reb = _Templates_.dynamicJets; 
	  n2smear = _Templates_.nparams;
	}
      else UJets_reb.clear();
    }
  if (isRebalanced && WeightVerbose)
    {
      TLorentzVector mhtOld = getMHT(UJets_rec, JET_PT_THRESH);
      TLorentzVector mht = getMHT(UJets_reb, JET_PT_THRESH);
      std::cout << "this worked! old mht: " << mhtOld.Pt() << std::endl;
      std::cout << "new mht: " << mht.Pt() << std::endl;
    }

  if (PrintJets_){
    if (doGenSmear_) std::cout << "Gen jets::::::::" << std::endl;
    else std::cout << "Rebalanced jets::::::::" << std::endl;
  }

  //-----------------------------------------------------------------
  // Save the rebalanced jets as pat::jets in case you ever need them.
  //-----------------------------------------------------------------
  auto finalRebalancedJets = std::make_unique<std::vector<pat::Jet> >();
  for (unsigned int ijet = 0; ijet<UJets_reb.size() ; ijet++ )
    {
      pat::Jet jet = Jets_rec[UJets_reb[ijet].patIdx];
      pat::Jet::LorentzVector newP4(UJets_reb[ijet].Px(),UJets_reb[ijet].Py(),\
				    UJets_reb[ijet].Pz(),UJets_reb[ijet].E());
      if (PrintJets_){
	double csv = jet.bDiscriminator(btagTag_);
	std::cout<<"pt,eta,csv="<<UJets_reb[ijet].Pt()<<","<<UJets_reb[ijet].Eta()<<","<<csv<<","<<std::endl;
      }
      jet.setP4(newP4);
      finalRebalancedJets->push_back(jet);
    }
  iEvent.put(std::move(finalRebalancedJets), "RebalancedJets");

  //------------------------------------------------------
  // Smear the events many times, save them as pat::jets |
  //------------------------------------------------------
  if (PrintJets_){std::cout << "First smeared collection::::::::" << std::endl; }

  for (int itry=0; itry<nTries_; itry++)
    {
      for (int iextra=0; iextra<nSmearsPerTry_; iextra++)
	{
	  auto finalSmearedJets = std::make_unique<std::vector<pat::Jet> >();
	  /*
	  std::auto_ptr< std::vector<pat::PackedCandidate> > finalPackedCandidates( new std::vector<pat::PackedCandidate> );

	  ///initialize the packed candidates, no scaling
	  for(size_t i=0; i<pfCandidates->size();i++)
	    {
	      pat::PackedCandidate pfCand = *(pfCandidates->at(i).clone());
	      pat::Jet::LorentzVector newP4(pfCand.px(),pfCand.py(),	\
	      				    pfCand.pz(),pfCand.energy());
	      pfCand.setP4(newP4);
	      finalPackedCandidates->push_back(pfCand);

	      //finalPackedCandidates->push_back(sammysPackedCandidates->at(i));

	      if(itry==-1)std::cout << "virgin peasant " << i << " pt = " << pfCand.pt() << ", eta = " << pfCand.eta() << std::endl;
	    }
	  */

	  std::vector<double> smearvector;
	  std::vector<UsefulJet> UJets_rands = SmearJets(UJets_reb, n2smear, smearvector);
	  int totaldaughtersupdated = 0;
	  for (unsigned int ijet = 0; ijet<UJets_rands.size() ; ijet++ )
	    {
	      /*
	      double smearfactor = smearvector[ijet]+0*(smearvector[ijet]-1.0);
	      */
	      pat::Jet jet = Jets_rec[UJets_rands[ijet].patIdx];
	      if (ijet<UJets_rands.size()-1 && UJets_rands[ijet].patIdx!=UJets_rands[ijet+1].patIdx-1)
		std::cout << "not in order!: " << UJets_rands[ijet].patIdx << std::endl;
	      pat::Jet::LorentzVector newP4(UJets_rands[ijet].Px(), UJets_rands[ijet].Py(), UJets_rands[ijet].Pz(), UJets_rands[ijet].E());
	      jet.setP4(newP4);
	      finalSmearedJets->push_back(jet);

	      /*
	      for(unsigned int idaughter = 0; idaughter<peasant_of_daughter_of_jet[ijet].size(); idaughter++)
		{
		  //std::cout << "using smear vector " << smearvector[ijet] << std::endl;
		  pat::Jet::LorentzVector 
		    newCandP4(smearfactor*finalPackedCandidates->at(peasant_of_daughter_of_jet[ijet][idaughter]).px(), \
			      smearfactor*finalPackedCandidates->at(peasant_of_daughter_of_jet[ijet][idaughter]).py(), \
			      smearfactor*finalPackedCandidates->at(peasant_of_daughter_of_jet[ijet][idaughter]).pz(), \
			      smearfactor*finalPackedCandidates->at(peasant_of_daughter_of_jet[ijet][idaughter]).energy());
		  if(itry==-1)std::cout << "under decree of a daughter, setting peasant " << peasant_of_daughter_of_jet[ijet][idaughter] << " from " << finalPackedCandidates->at(peasant_of_daughter_of_jet[ijet][idaughter]).pt() << " to " << std::endl;
		  finalPackedCandidates->at(peasant_of_daughter_of_jet[ijet][idaughter]).setP4(newCandP4);
		  if(itry==-1)std::cout << finalPackedCandidates->at(peasant_of_daughter_of_jet[ijet][idaughter]).pt() << std::endl;
		  if(itry==-1)std::cout << "daughter " << idaughter << " pt = " << Jets_rec[UJets_rands[ijet].patIdx].daughter(idaughter)->pt() << ", eta = " << Jets_rec[UJets_rands[ijet].patIdx].daughter(idaughter)->eta() << std::endl;
		  totaldaughtersupdated+=1;
		}
	      */
	      if (PrintJets_ && itry==0){
		std::cout<<"pt,eta,csv="<<UJets_rands[ijet].Pt()<<","<<UJets_rands[ijet].Eta()<<","<<UJets_rands[ijet].csv<<","<<std::endl;
	      }

	    }

	  // for(size_t i=0; i<finalPackedCandidates->size();i++)
	  // {
	  //   pat::PackedCandidate pfCand = finalPackedCandidates->at(i);
	  //   if(itry==-1)std::cout << "war-torn peasant " << i << " pt = " << pfCand.pt() << ", eta = " << pfCand.eta() << std::endl;
	  // }
	  char buffer [50];
	  sprintf (buffer, "RandSJetsTry%dSmear%d", itry, iextra);
	  iEvent.put(std::move(finalSmearedJets), buffer);
	  /*
	  sprintf (buffer, "sammysPackedCandidatesTry%dSmear%d", itry, iextra);
	  iEvent.put(std::move(finalPackedCandidates), buffer);
	  */
	}
    }

  //iEvent.put(std::move(sammysPackedCandidates),"sammysPackedCandidates");

  auto SmearingWeight = std::make_unique<double>(1.0/nSmearsPerTry_);
  if (WeightVerbose) std::cout << "in producer, nSmearsPerTry_ and weight are " << nSmearsPerTry_ << " and " << 1.0/nSmearsPerTry_ << std::endl;
  iEvent.put(std::move(SmearingWeight),"SmearingWeight");
  auto fitSucceed =  std::make_unique<bool>(isRebalanced);
  iEvent.put(std::move(fitSucceed),"FitSucceed");
  if (WeightVerbose) std::cout  << "fit succeed in the producer is " << isRebalanced << std::endl;


}

// ------------ method called once each job just before starting event loop  ------------
void  RandSJetsProducer::beginJob()
{
  std::cout << "beginning this strange job" << std::endl;
  TemplatesFile= new TFile(smearingfile_.c_str(), "READ", "", 0);
  GleanTemplatesFromFile(TemplatesFile);

  // if (doGenSmear_)
  //   {
  //     edm::Handle<edm::View<reco::GenJet> > GenJets_handle;
  //     edm::View<reco::GenJet> Jets_gen;
  //     std::vector<UsefulJet> UGenJets;
  //     bool gj_present = iEvent.getByToken(genjetsToken_, GenJets_handle);
  //     if (gj_present) {
  // 	Jets_gen = *gj;
  // 	for (edm::View<reco::GenJet>::const_iterator it = Jets_gen.begin(); it != Jets_gen.end(); ++it) {
  // 	  TLorentzVector lvi;
  // 	  lvi.SetPxPyPzE(it->px(), it->py(), it->pz(), it->energy());
  // 	  double csv = 0.;
  // 	  UGenJets.emplace_back(new UsefulJet(lvi,csv));
  // 	}
  //     }
  //   }
  // else

}

// ------------ method called once each job just after ending the event loop  ------------
void  RandSJetsProducer::endJob() {
  TemplatesFile->Close();
}

// ------------ method called when starting to processes a run  ------------
void RandSJetsProducer::beginRun(edm::Run&, edm::EventSetup const&)
{
}

// ------------ method called when ending the processing of a run  ------------
void RandSJetsProducer::endRun(edm::Run&, edm::EventSetup const&)
{
}

// ------------ method called when starting to processes a luminosity block  ------------
void  RandSJetsProducer::beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&)
{
}

// ------------ method called when ending the processing of a luminosity block  ------------
void  RandSJetsProducer::endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&)
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void RandSJetsProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(RandSJetsProducer);
