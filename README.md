# AllHadronicSUSY

## Instructions

```
Works on: cmssw/slc6_amd64_gcc530

cmsrel CMSSW_8_0_25
cd CMSSW_8_0_25/src/
cmsenv
git cms-init
git remote add btv-cmssw https://github.com/cms-btv-pog/cmssw.git
git fetch btv-cmssw BoostedDoubleSVTaggerV4-WithWeightFiles-v1_from-CMSSW_8_0_21
git cms-merge-topic -u cms-btv-pog:BoostedDoubleSVTaggerV4-WithWeightFiles-v1_from-CMSSW_8_0_21
git cms-merge-topic -u kpedro88:METfix8022
git cms-merge-topic -u cms-met:fromCMSSW_8_0_20_postICHEPfilter
git cms-merge-topic -u kpedro88:storeJERFactor8022
git cms-merge-topic -u kpedro88:badMuonFilters_80X_v2_RA2
git clone git@github.com:cms-jet/JetToolbox.git JMEAnalysis/JetToolbox -b jetToolbox_80X_V3
git clone git@github.com:TreeMaker/TreeMaker.git -b Run2

git clone https://gitlab.cern.ch/ra2b/rs.git

cp rs/NoMuonInt.cc TreeMaker/Utils/src/
cp rs/nomuonjet_cfi.py TreeMaker/Utils/python/
cp rs/randsjetsproducer_cfi.py TreeMaker/Utils/python
cp rs/RandSJetsProducer.cc TreeMaker/Utils/src

scram b -j 8

```
